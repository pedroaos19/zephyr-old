@extends('account.layouts.master')

@section('content')
<div class="form-title">
    <h3>Login</h3>
</div>
<form id="form-signin" method="POST" action="/account/signin">
    {{ csrf_token() }}
    <div class="form-element">
        <input class="form-input" type="email" name="email" id="email" placeholder="Email" />
    </div>
    <div class="form-element">
        <input class="form-input" type="password" name="password" id="password" placeholder="Password" />
    </div>
    <button class="form-submit" type="submit">Login</button>
</form>
@endsection