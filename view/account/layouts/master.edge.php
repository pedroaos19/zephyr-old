<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Zephyr</title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans&display=swap" rel="stylesheet">
    <style>
        body {
            background: rgb(178, 48, 222);
            background: linear-gradient(70deg, rgba(178, 48, 222, 1) 0%, rgba(34, 233, 255, 1) 100%);
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            font-family: 'Fira Sans', sans-serif;
        }

        .form-card {
            min-width: 25%;
            border-radius: 4px;
        }

        .form-title {
            display: flex;
            justify-content: center;
            margin-bottom: 32px;
            padding: 16px 0;
            border-bottom: 1px solid rgba(255, 255, 255, 0.15);
            margin-left: 16px;
            margin-right: 16px;
        }

        .form-title h3 {
            margin: 0;
            font-size: 42px;
            color: #f7f7f7;
        }

        .form-element {
            display: flex;
            flex-direction: column;
            margin: 16px 0;
        }

        .form-input {
            height: 42px;
            font-size: 14px;
            padding: 0 12px;
            border-radius: 16px;
            border: none;
            box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.1);
        }

        .form-input:focus {
            outline: none;
        }

        .form-input::placeholder {
            color: rgba(0, 0, 0, 0.25);
        }

        .form-submit {
            margin-top: 16px;
            border-radius: 16px;
            border: none;
            background: #353535;
            height: 42px;
            color: #f7f7f7;
            width: 100%;
            font-size: 14px;
            box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.1);
        }

        .form-submit:focus {
            outline: none;
            background-color: #454545;
        }
    </style>
</head>

<body>
    <div class="form-card">
        @yield('content')
    </div>
</body>

</html>