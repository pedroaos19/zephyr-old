@extends('account.layouts.master')

@section('content')
<div class="form-title">
    <h3>Register</h3>
</div>
<form id="form-signup" method="POST" action="/account/signup">
    {{ csrf_token() }}
    <div class="form-element">
        <input class="form-input" type="email" name="email" id="email" placeholder="Email" />
    </div>
    <div class="form-element">
        <input class="form-input" type="password" name="password" id="password" placeholder="Password" />
    </div>
    <button class="form-submit" type="submit">Sign Up</button>
</form>
@endsection