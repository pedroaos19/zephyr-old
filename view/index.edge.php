<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token_raw()}}">
    <title>Zephyr</title>
</head>

<body>
    <h1>Zephyr</h1>
    @if(auth())
        <h3>Logged in!</h3>
        <form method="POST" action="/account/signout">
            {{csrf_token()}}
            <button type="submit">Logout</button>
        </form>
    @else
        <h3>Not logged in.</h3>
    @endif
</body>

</html>