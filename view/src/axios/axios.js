import axios from 'axios';

const instance = axios.create({
    baseURL: '/'
});

instance.defaults.headers.common["X-CSRF-TOKEN"] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

export default instance;
