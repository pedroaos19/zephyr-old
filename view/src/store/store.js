import Vue from "vue";
import Vuex from "vuex";

// Import modules here.

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        // ...
    }
});
