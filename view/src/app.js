import Vue from "vue";

import App from "./components/App.vue";

import { router } from "./routes/router";
import { store } from "./store/store";
import { mercury } from "./mercury/mercury";

const app = new Vue({
    el: "#app",
    render: h => h(App),
    router,
    store
});
