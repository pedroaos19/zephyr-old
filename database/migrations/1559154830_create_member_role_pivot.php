<?php
use Zephyr\Database\Migration\Migration;
use Zephyr\Database\Schema;
use Zephyr\Database\PivotTable;

class CreateMemberRolePivot implements Migration
{
    public function up()
    {
        $schema = new Schema();

        $schema->primary('id');

        PivotTable::pivot('members', 'roles', $schema);
    }

    public function down()
    {
        PivotTable::drop('member_role');
    }
}
