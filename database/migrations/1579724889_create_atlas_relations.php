<?php
use Zephyr\Database\Migration\Migration;
use Zephyr\Database\Schema;
use Zephyr\Database\Table;

class CreateAtlasRelations implements Migration
{
    public function up()
    {
        $schema = new Schema();

        $schema->primary('id');
        $schema->varchar('table_name');
        $schema->varchar('column_name');
        $schema->varchar('relation_type');
        $schema->text('definition');

        Table::create('atlas_relations', $schema);
    }

    public function down()
    {
        Table::drop('atlas_relations');
    }
}