<?php
use Zephyr\Database\Migration\Migration;
use Zephyr\Database\Schema;
use Zephyr\Database\Table;

class CreateMembersTable implements Migration
{
    public function up()
    {
        $schema = new Schema();

        $schema->primary('id');
		$schema->varchar('email');
        $schema->varchar('password');
        // $schema->timestamps();

        Table::create('members', $schema);
    }

    public function down()
    {
        Table::drop('members');
    }
}
