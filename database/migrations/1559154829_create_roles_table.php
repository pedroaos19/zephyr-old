<?php
use Zephyr\Database\Migration\Migration;
use Zephyr\Database\Schema;
use Zephyr\Database\Table;

class CreateRolesTable implements Migration
{
    public function up()
    {
        $schema = new Schema();

        $schema->primary('id');
		$schema->varchar('name');

        Table::create('roles', $schema);
    }

    public function down()
    {
        Table::drop('roles');
    }
}
