<?php
namespace App\Middleware;

use Zephyr\Http\Request\Request;
use Closure;

class ExampleMiddleware
{
    public $except = [
        "/"
    ];
        
    public function run(Request $request, Closure $next)
    {
        return $next($request);
    }
}
