<?php

namespace App\Controller;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;

class TokenshipController extends Controller
{
    public function signin(Request $request)
    {
        $result = tokenship()->login($request->post("email"), $request->post("password"));

        return json($result)
            ->cookie("jwt_token", $result["jwt_token"], $result["expiration"], true);
    }

    public function signup(Request $request)
    {
        $result = tokenship()->register($request->post("email"), $request->post("password"));
        return json($result);
    }

    public function member()
    {
        $result = tokenship()->member();
        return json($result);
    }
}
