<?php

namespace App\Controller;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;

class AccountController extends Controller
{
    public function login()
    {   
        return view('account/login');
    }

    public function signin(Request $request)
    {
        membership()->login($request->post("email"), $request->post("password"));

        return redirect('/');
    }

    public function register()
    {
        return view('account/register');
    }

    public function signup(Request $request)
    {
        membership()->register($request->post("email"), $request->post("password"));

        return redirect('/');
    }

    public function signout()
    {
        membership()->logout();

        return redirect('/');
    }
}
