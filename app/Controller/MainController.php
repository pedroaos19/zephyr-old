<?php
namespace App\Controller;

use App\Model\Product;
use Zephyr\Controller\Controller;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function app()
    {
        return view('src/app');
    }
}
