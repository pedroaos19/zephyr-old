<?php
namespace App\Mercury;

use Zephyr\Mercury\Mercury;
use Ratchet\ConnectionInterface;

class Hub extends Mercury
{
    public function onOpen(ConnectionInterface $conn)
    {
        parent::onOpen($conn);
    }

    public function onConnected($data)
    {
        
    }
    
    public function onClose(ConnectionInterface $conn)
    {
        parent::onClose($conn);
    }
}
