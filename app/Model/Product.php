<?php
namespace App\Model;

use Zephyr\Model\Model;

class Product extends Model
{

	public function tags()
    {
    	return $this->hasMany('App\Model\Tag', 'product_id');
    }
	public function categories()
    {
    	return $this->hasMany('App\Model\Category', 'product_id');
    }
}
