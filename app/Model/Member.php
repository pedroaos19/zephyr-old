<?php
namespace App\Model;

use Zephyr\Auth\Model\Member as ZephyrModel;

class Member extends ZephyrModel
{
    public function roles()
    {
        return $this->belongsToMany('App\Model\Role');
    }
}
