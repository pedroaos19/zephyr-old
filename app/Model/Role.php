<?php
namespace App\Model;

use Zephyr\Auth\Model\Role as ZephyrRole;

class Role extends ZephyrRole
{
    public function members()
    {
        return $this->belongsToMany('App\Model\Member');
    }
}
