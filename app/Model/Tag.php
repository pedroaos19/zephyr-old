<?php
namespace App\Model;

use Zephyr\Model\Model;

class Tag extends Model
{

	public function product()
    {
    	return $this->belongsTo('App\Model\Product', 'product_id');
    }
}
