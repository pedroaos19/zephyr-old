<?php
namespace Zephyr\Mercury;

use Zephyr\Filesystem\Filesystem;

class Assets
{
    public static function javascript()
    {
        return Filesystem::file('src/Mercury/assets/mercury.js')->path();
    }
}
