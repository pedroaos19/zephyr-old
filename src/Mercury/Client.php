<?php
namespace Zephyr\Mercury;

use Zephyr\Auth\Tokenship;

class Client
{
    private $data;

    public function __construct($connection, $member = null)
    {
        $this->data = [];
        $this->connection = $connection;
        $this->resourceId = $this->connection->resourceId;
    }

    public function send($package)
    {
        $this->connection->send($package);
    }

    public function __get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function data()
    {
        return $this->data;
    }
}
