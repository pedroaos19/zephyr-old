<?php
namespace Zephyr\Mercury;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Mercury implements MessageComponentInterface
{
    protected $interpretor;
    protected $clients;
    protected $currentConnection;
    protected $httpRequest;

    public function __construct()
    {
        $this->interpretor = new Interpretor;
        $this->clients = [];
        $this->currentConnection = null;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients[(int)$conn->resourceId] = new Client($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $this->currentConnection = $this->client($from->resourceId);
        $this->httpRequest = $from->httpRequest;

        $method = $this->interpretor->method($msg);
        $args = $this->interpretor->data($msg);

        $this->call($method, $args);

        $this->currentConnection = null;
    }

    public function onClose(ConnectionInterface $conn)
    {
        unset($this->clients[(int)$conn->resourceId]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }

    // ------------------------------------------------------------------ //

    private function call($method, array $args)
    {
        call_user_func_array([$this, $method], [$args]);
    }

    // *** Access to Client Information

    protected function client($resourceId)
    {
        return isset($this->clients[$resourceId]) ? $this->clients[$resourceId] : null;
    }

    protected function clients()
    {
        return $this->clients;
    }

    // *** Broadcast Options

    protected function direct($client, $function, $data)
    {
        $package = $this->pack($function, $data);
        $client->send($package);
    }

    protected function several($clients, $function, $data)
    {
        $package = $this->pack($function, $data);

        foreach ($clients as $client) 
        {
            $client->send($package);
        }
    }

    protected function broadcast($function, $data)
    {
        $package = $this->pack($function, $data);

        foreach ($this->clients as $client) 
        {
            if ($this->currentConnection->resourceId == $client->resourceId) continue;
            
            $client->send($package);
        }
    }

    protected function broadcastExcept($except, $function, $data)
    {
        $package = $this->pack($function, $data);

        $ids = is_array($except) ? array_column($except, "resourceId") : [$except->resourceId];
        foreach ($this->clients as $client) 
        {
            if ($this->currentConnection->resourceId == $client->resourceId || in_array($client->resourceId, $ids))
                continue;

            $client->send($package);
        }
    }

    protected function all($function, $data)
    {
        $package = $this->pack($function, $data);

        foreach ($this->clients as $client) 
        {
            $client->send($package);
        }
    }

    protected function allExcept($except, $function, $data)
    {
        $package = $this->pack($function, $data);

        $ids = is_array($except) ? array_column($except, "resourceId") : [$except->resourceId];
        foreach ($this->clients as $client) 
        {
            if (in_array($client->resourceId, $ids))
                continue;

            $client->send($package);
        }
    }

    private function pack($function, $data)
    {
        return json_encode(["function" => $function, "data" => $data]);
    }
}
