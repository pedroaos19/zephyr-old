<?php
namespace Zephyr\Mercury;

class Interpretor
{
    public function method($msg)
    {
        return json_decode($msg, true)["method"];
    }

    public function data($msg)
    {
        return isset(json_decode($msg, true)["data"]) ? json_decode($msg, true)["data"] : [];
    }
}
