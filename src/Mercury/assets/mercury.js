function Mercury() {
    var conn = new WebSocket('ws://localhost:5555');

    conn.onopen = function(e) { 
        Mercury.send("onConnected", { csrf: localStorage.getItem("jwt_csrf") });
    };

    conn.onmessage = function(e) {
        package = JSON.parse(e.data);
        functionName = package.function;
        
        window[functionName].apply(this, [package.data]);
    };

    this.conn = conn;

    this.send = function(method, data) {
        this.conn.send(this.encode(method, data));
    }

    this.encode = function(method, data) {
        return JSON.stringify({
            "method": method,
            "data": data
        });
    }

    this.decode = function(package) {
        return JSON.parse(package);
    }
};

window.Mercury = new Mercury();
window.hg = window.mercury;