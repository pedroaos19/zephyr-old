<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$error->type}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="/src/Charon/assets/_charon.css" rel="stylesheet">
</head>

<body>
    <div class="error-banner">
        <div class="banner__header">
            <span>{{$error->type}} - {{$error->file->path()}}</span>
        </div>
        <div class="banner__body">
            <p class="banner__body-title">{{$error->title}}</p>
            <p class="banner__body-message">{{$error->message}}</p>
            <a href="{{$error->url}}" class="banner__body-footer">{{$error->url}}</a>
        </div>
    </div>
</body>

</html>