<?php

namespace Zephyr\Charon\Error;

use Zephyr\Filesystem\Filesystem;
use Zephyr\Http\Request\Request;

class Error
{
    public function __construct($lastError)
    {
        if ($lastError !== null) 
        {
            $this->type = ErrorType::text($lastError["type"]);
            $this->file = Filesystem::file($lastError["file"]);
            $this->line = $lastError["line"];
            $this->title = $this->title($lastError["message"]);

            if($lastError["type"] === E_WARNING)
            {
                $this->message = $lastError["message"];
            }
            else
            {
                $this->message = $this->message($lastError["message"]);
            }

            $this->url = Request::capture()->url;
        }
    }

    protected function title($message)
    {
        $ltrimmed = str_replace("Uncaught ", "", $message);

        return substr(
            $ltrimmed, 0, strpos($ltrimmed, ":")
        );
    }

    protected function message($message)
    {
        $ltrimmed = str_replace("Uncaught ", "", $message);
        $ltrimmed = ltrim($ltrimmed, $this->title . ": ");
        $endPos = strpos(
            $ltrimmed, "in " . $this->file->path() . ":" . $this->line
        );
        return rtrim(substr(
            $ltrimmed, 0, $endPos
        ));
    }
}