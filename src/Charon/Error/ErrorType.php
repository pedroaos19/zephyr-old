<?php

namespace Zephyr\Charon\Error;

class ErrorType
{
    const TYPES = [
        1 => "Fatal Error",
        2 => "Warning",
        4 => "Parse Error",
        8 => "Notice",
        16 => "Core Error",
        32 => "Core Warning",
        64 => "Compilation Error",
        128 => "Compilation Warning",
        256 => "Fatal Error",
        512 => "Warning",
        1024 => "Notice",
    ];

    public static function text(int $type)
    {
        return ErrorType::TYPES[$type];
    }
}