<?php

namespace Zephyr\Charon;

use Zephyr\Charon\Error\Error;
use Zephyr\Charon\Error\ErrorType;
use Zephyr\View\View;

class Charon
{
    public function __construct()
    {
        error_reporting(0);  
        $this->registerShutdownFunction();      
    }

    protected function registerShutdownFunction()
    {
        register_shutdown_function(function () {
            $error = new Error(error_get_last());
            if ($error->type === ErrorType::text(E_WARNING)) 
            {
                $path = $error->file->path();
                echo "
                <div style='
                    position: absolute; 
                    top: 16px;
                    left: 16px;
                    min-width: 25%;
                    border-radius: 4px;
                    border: 1px solid rgba(0, 0, 0, 0.1)';
                '>
                    <p style='
                        margin: 0;
                        padding: 0;
                        background: #e0d058;
                        border-radius: 4px;
                        border-bottom-left-radius: 0px;
                        border-bottom-right-radius: 0px;
                        padding-left: 8px;
                        font-size: 12px;
                        font-weight: bold;
                        color: #333;
                    '>
                        $error->type
                    </p>
                    <p style='
                        margin: 0;
                        padding: 0;
                        background: rgba(0, 5, 25, 0.7); 
                        padding: 16px;
                        font-size: 14px;
                        color: #ddd;
                        border-bottom-left-radius: 4px;
                        border-bottom-right-radius: 4px;
                    >
                        <span style='font-weight: bolder;'>$error->message</span>
                        <br>
                        <span style='color: #fff; font-size: 12px; opacity: 0.5'>$path:$error->line</span>
                    </p>
                ";
            }
            else
            {
                print View::render('_charon', compact('error'));
            }
        });
    }
}