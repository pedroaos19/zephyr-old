<?php

namespace Zephyr\Charon;

class ZephyrException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        $f = self::getFile();
        $l = self::getLine();
        return __CLASS__ . ": {$this->message} in {$f}:{$l}\n";
    }

    public function customFunction()
    {
        echo "A custom function for this type of exception\n";
    }
}
