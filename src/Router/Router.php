<?php
namespace Zephyr\Router;

use Zephyr\Http\Request\Request;
use Zephyr\Http\Session;
use Zephyr\Http\Middleware\MiddlewareHandler;
use Zephyr\Utils\Arr;
use Zephyr\Utils\Str;

class Router
{
    /**
     * @throws \ReflectionException
     */
    public static function listen()
    {
        $request = Request::capture();
        $routes = Map::instance()->routes($request->method);

        $route = static::matchingRoute($routes, $request);
        if($route)
        {
            static::injectGlobalMiddleware($route, $request);
            $response = static::handleMiddleware($route, $request);
            $response->send();

            if(!$response instanceof \Zephyr\Http\Response\RedirectResponse)
                Session::capture()->flushFlash();

            exit();
        }

        $spa = Map::instance()->router_spa();
        if(isset($spa))
        {
            static::injectGlobalMiddleware($spa, $request);
            $response = static::handleMiddleware($spa, $request);
            $response->send();

            exit();
        }

        return send_404();
    }

    private static function matchingRoute($routes, Request $request)
    {
        $result = null;

        foreach ($routes as $route) 
        {
            if(static::urlsMatch($request, $route))
            {
                $result = $route;
                break;
            }
        }

        return $result;
    }

    private static function urlsMatch(Request $request, Route $route)
    {
        $segmentsRequest = $request->segments;
        $segmentsRoute = $route->segments;
        
        if(Arr::sameLength($segmentsRequest, $segmentsRoute))
        {
            $diffSegments = array_diff($segmentsRoute, $segmentsRequest);            
            $notParams = array_filter($diffSegments, function($segment) {
                if(!Str::contains($segment, ":")) 
                {
                    return $segment;
                }
            });

            return empty($notParams) ? true : false;
        }
    }

    private static function ajax(Request $request, $method, $params)
    {
        if (
            !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            foreach ($request->all() as $key => $value) 
            {
                for ($i = 0; $i < count($method->getParameters()); $i++) 
                {
                    if ($method->getParameters()[$i]->getName() === $key) 
                    {
                        $position = $method->getParameters()[$i]->getPosition();

                        array_splice(
                            $params, 
                            $position, 
                            0, 
                            [$request->get($key)]
                        );
                        break;
                    }
                }
            }
        }
    }

    private static function injectGlobalMiddleware($route, $request)
    {
        if (!file_exists("routes/middlewares.php")) return;
        if (!isset($route->middleware)) $route->middleware = [];

        include_once "routes/middlewares.php";
        switch (strtoupper($request->method)) 
        {
            case "GET":
                if(isset($get))
                    $route->middleware = array_merge($route->middleware, $get);
                break;
            case "POST":
                if(isset($post))
                    $route->middleware = array_merge($route->middleware, $post);
                break;
        }

        if(isset($global)) 
            $route->middleware = array_merge($route->middleware, $global);

        if(empty($route->middleware)) $route->middleware = [];
    }

    private static function handleMiddleware($route, $request)
    {
        if (isset($route->middleware)) 
        {
            $response = MiddlewareHandler::handle($route, $request);
        }

        return $response;
    }
}
