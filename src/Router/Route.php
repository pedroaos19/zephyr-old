<?php
namespace Zephyr\Router;

use Zephyr\Utils\Arr;
use Zephyr\Utils\Str;

class Route
{
    private function __construct($method, $path, $mapping, $options)
    {
        $this->method = $method;
        $this->path = $path;

        $mappingExploded = explode('.', $mapping);
        $this->controller = $mappingExploded[0];
        $this->action = $mappingExploded[1];

        $this->segments = $this->getSegments();

        $this->params = $this->getParams();

        $this->middleware = $this->getOptions($options, "middleware");
        $this->namespace = $this->getOptions($options, "namespace");
        $this->alias = $this->getOptions($options, "alias");
    }

    public static function group($path, $options) 
    {
    }

    public static function get($path, $mapping, $options = null)
    {
        return new Route("GET", $path, $mapping, $options);
    }

    public static function post($path, $mapping, $options = null)
    {
        return new Route("POST", $path, $mapping, $options);
    }

    private function getSegments() 
    {
        $segments = explode("/", $this->path);

        if(Arr::first($segments) === "")
        {
            array_splice($segments, 0, 1);
        }

        if(Arr::last($segments) === "")
        {
            array_splice($segments, -1, 1);
        }

        return $segments;
    }

    private function getParams() 
    {
        $params = [];

        foreach($this->segments as $index => $segment) 
        {
            if(Str::contains($segment, ':'))
            {
                $params[$index] = $segment;
            }    
        }

        return $params;
    }

    private function getOptions($options, $name)
    {
        return isset($options[$name]) ? $options[$name] : null;
    }

    public function applyParams(array $params)
    {
        if(empty($params) || $params === null) return $this->path;
        
        $result = $this->path;
        foreach($this->params as $param)
        {
            if(
                in_array(
                    str_replace(":", "", $param), 
                    array_keys($params))
                ) 
            {
                $result = str_replace($param, $params[str_replace(":", "", $param)], $result);
            }
        }

        return $result;
    }
}
