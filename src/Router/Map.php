<?php
namespace Zephyr\Router;

use Zephyr\Http\Request\Request;

class Map
{
    private $get;
    private $post;

    private function __construct()
    {
        $this->get = [];
        $this->post = [];
        $this->spa = null;
    }

    public static function instance()
    {
        static $instance = null;

        if ($instance == null) 
        {
            $instance = new Map();
        }

        return $instance;
    }

    public function get($uri, $mapping, $options = null)
    {
        $this->get[] = Route::get($uri, $mapping, $options);
    }

    public function post($path, $mapping, $options = null)
    {
        $this->post[] = Route::post($path, $mapping, $options);
    }

    public function group($baseUri, array $children, $options = null)
    {
        
    }

    public function spa($mapping, $options = null)
    {
        $this->spa = Route::get(Request::capture()->path, $mapping, $options);
    }

    public function routes($method = null)
    {
        if(isset($method)) 
        {
            switch (strtoupper($method)) 
            {
                case "GET":
                    return $this->get;
                case "POST":
                    return $this->post;
            }
        }
        else
        {
            $mergedRoutes = array_merge($this->get, $this->post);
            usort($mergedRoutes, function($route1, $route2) {
                return strcmp($route1->path, $route2->path);
            });

            return $mergedRoutes;
        }
    }

    public function routesGet()
    {
        return $this->get;
    }

    public function routesPost()
    {
        return $this->post;
    }

    public function router_spa()
    {
        return $this->spa;
    }

    public function findRouteByAlias($alias)
    {
        $all = array_merge($this->get, $this->post);

        $result = null;
        foreach($all as $route)
        {
            if($route->alias === $alias)
            {
                $result = $route;
            }
        }

        return $result; 
    }
}
