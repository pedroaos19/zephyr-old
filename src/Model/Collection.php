<?php
namespace Zephyr\Model;

use JsonSerializable;
use Traversable;
use Zephyr\Charon\ZephyrException;
use Zephyr\Utils\Str;

class Collection implements \ArrayAccess, \IteratorAggregate, \Countable, JsonSerializable
{
    protected $class;
    protected $collection = [];

    public function __construct($class)
    {
        $this->class = $class;
    }

    protected function isFromClass($object)
    {
        return $this->class === get_class($object);
    }

    public function propertyExists($property)
    {
        return count(array_column($this->collection, $property)) > 0;
    }

    public function push($object)
    {
        if(!$this->isFromClass($object)) throw new ZephyrException("Object's class and Collection's class don't match");

        array_push($this->collection, $object);
    }

    public function mass(array $array)
    {
        $this->collection = array_merge($this->collection, $array);
    }

    public function pop()
    {
        $last = $this->last();
        $this->remove($last);
        return $last;
    }

    public function sort(string $property = null, bool $desc = false)
    {
        if($this->empty())
            return $this;
        
        if (!$this->propertyExists($property))
            throw new ZephyrException("Property '{$property}' doesn't exist for this Collection");

        $property = !is_null($property) ? $property : "id";

        usort($this->collection, function($a, $b) use ($property) {
            switch (gettype($a->$property)) 
            {
                case "integer":
                    if ($a->$property === $b->$property) return 0;
                    if ($a->$property > $b->$property) return 1;
                    if ($a->$property < $b->$property) return -1;
                case "string":
                    return strcmp($a->$property, $b->$property);
                case "boolean";
                    return $a->$property - $b->$property;
                case "object":
                    if ($a->$property instanceof \Carbon\Carbon && $b->$property instanceof \Carbon\Carbon) {
                        if ($a->$property->equalTo($b->$property)) return 0;
                        if ($a->$property->greaterThan($b->$property)) return 1;
                        if ($a->$property->lessThan($b->$property)) return -1;
                    }
            }
        });

        $this->collection = $desc ? array_reverse($this->collection) : $this->collection;

        return $this;
    }

    public function find(int $index)
    {
        if($index >= count($this->collection) || $index < 0)
            throw new ZephyrException("Index out of bounds");

        return $this->collection[$index];
    }

    public function get(int $id)
    {
        $result = array_filter($this->collection, function($obj) use ($id) {
            return $obj->id === $id;
        });

        if(count($result) < 1) 
            throw new ZephyrException("Object not found with id {$id}");

        return array_pop($result);
    }

    public function first()
    {
        if(count($this->collection) < 1)
            throw new ZephyrException("This Collection is empty");

        return $this->collection[0];
    }

    public function last()
    {
        if (count($this->collection) < 1)
            throw new ZephyrException("This Collection is empty");
            
        return $this->collection[count($this->collection) - 1];
    }

    public function where2(string $property, $value, $condition = null)
    {
        if (!$this->propertyExists($property))
            throw new ZephyrException("Property '{$property}' doesn't exist for this Collection");

        if (!is_null($condition) && !in_array($condition, ["<", "<=", ">", ">=", "=", "!="]))
            throw new ZephyrException("Invalid condition {$condition}");

        // $collection = new Collection($this->class);
        // $result = array_filter($this->collection, function($obj) {

        // });

        // return $collection->mass($result);

        return $this;
    }

    public function where(string $property, $value, string $condition = null)
    {
        if (!$this->propertyExists($property))
            throw new ZephyrException("Property '{$property}' doesn't exist for this Collection");

        if (!is_null($condition) && !in_array($condition, ["<", "<=", ">", ">=", "=", "!="]))
            throw new ZephyrException("Invalid condition {$condition}");

        $newCollection = new Collection($this->class);
        foreach ($this->collection as $object) {
            if (isset($condition) && $this->evaluate($object->$property, $value, $condition)) {
                $newCollection->push($object);
            } else if (isset($condition) === false && $object->$property === $value) {
                $newCollection->push($object);
            }
        }

        return $newCollection;
    }

    public function whereNot(string $property, $value, string $condition = NULL)
    {
        if (!$this->propertyExists($property))
            throw new ZephyrException("Property '{$property}' doesn't exist for this Collection");

        if (!is_null($condition) && !in_array($condition, ["<", "<=", ">", ">=", "=", "!="]))
            throw new ZephyrException("Invalid condition {$condition}");

        $newCollection = new Collection($this->class);
        foreach ($this->collection as $object) {
            if (isset($condition) && $this->evaluate($object->$property, $value, $condition)) { } else if (isset($condition) === false && $object->$property === $value) { } else {
                $newCollection->push($object);
            }
        }

        return $newCollection;
    }

    protected function evaluate($value, $otherValue, $condition)
    {
        switch ($condition) {
            case "<":
                return ($value < $otherValue) ? true : false;
                break;
            case ">":
                return ($value > $otherValue) ? true : false;
                break;
            case "<=":
                return ($value <= $otherValue) ? true : false;
                break;
            case ">=":
                return ($value >= $otherValue) ? true : false;
                break;
            default:
                return null;
                break;
        }
    }

    public function sum(string $property)
    {
        if (!$this->propertyExists($property))
            throw new ZephyrException("Property '{$property}' doesn't exist for this Collection");

        if(!is_numeric($this->collection[0]->$property))
            throw new ZephyrException("Property '{$property}' isn't an number");

        return array_sum(array_column($this->collection, $property));
    }

    public function average($key, bool $round = null)
    {
        $sum = $this->sum($key);

        return $round ? round($sum / count($this->collection)) : $sum / count($this->collection);
    }
    
    public function has($object)
    {
        if (!$this->isFromClass($object)) 
            throw new ZephyrException("Object's class and Collection's class don't match");


        $result = count(array_filter($this->collection, function($obj) use ($object) {
            return $obj->id === $object->id;
        }));

        return $result > 0;
    }

    public function merge(Collection $other)
    {
        $this->collection = array_merge($this->collection, $other->raw());
        return $this;
    }

    public function partial(int $start, int $length)
    {
        $partialCollection = new Collection($this->class);

        for ($i = $start; $i < $start + $length; $i++) 
        {
            if (isset($this->collection[$i])) 
            {
                $partialCollection->push($this->collection[$i]);
            }
        }

        return $partialCollection;
    }

    public function slice(int $size, int $index = null)
    {
        $collectionSliced = array();

        for ($i = 0; $i < count($this->collection); $i = $i + $size) 
        {
            $slice = new Collection($this->class);

            for ($j = $i; $j < $i + $size; $j++) 
            {
                if (isset($this->collection[$j])) 
                {
                    $slice->push($this->collection[$j]);
                }
            }

            array_push($collectionSliced, $slice);
        }

        if (isset($index) && $index >= 0 && $index < count($collectionSliced) && isset($collectionSliced[$index])) {
            return $collectionSliced[$index];
        } else if (isset($index) === false) {
            return $collectionSliced;
        } else {
            return NULL;
        }
    }

    public function empty()
    {
        return count($this->collection) === 0;
    }

    public function clear()
    {
        $this->collection = [];
    }

    public function max(string $key)
    {
        $max = null;
        foreach ($this->collection as $object) 
        {
            if (isset($object->$key) && gettype($object->$key) == "integer") 
            {
                if (isset($max) === false) 
                {
                    $max = $object->$key;
                } 
                else if ($max < $object->$key) 
                {
                    $max = $object->$key;
                }
            }
        }

        return $max;
    }

    public function min(string $key)
    {
        $min = null;

        foreach ($this->collection as $object) 
        {
            if (isset($object->$key) && gettype($object->$key) == "integer") 
            {
                if (isset($min) === false) 
                {
                    $min = $object->$key;
                } 
                else if ($min > $object->$key) 
                {
                    $min = $object->$key;
                }
            }
        }

        return $min;
    }

    public function remove($object)
    {
        if (!$this->has($object))
            throw new ZephyrException("Object doesn't exist in the Collection");

        array_splice($this->collection, $this->indexOf($object), 1);

        return $this;
    }

    public function indexOf($object)
    {
        if(!$this->has($object))
            throw new ZephyrException("Object doesn't exist in the Collection");

        for ($i = 0; $i < count($this->collection); $i++) 
        {
            if ($object->id === $this->collection[$i]->id) 
            {
                return $i;
            }
        }
    }

    public function random()
    {
        $randomIndex = rand(0, count($this->collection) - 1);
        return $this->collection[$randomIndex];
    }

    public function randomize()
    {
        $collectionCopy = $this->collection;
        $this->collection = array();
        $indexes = array_keys($collectionCopy);

        while (count($indexes) > 0) 
        {
            $randomIndex = rand(0, count($indexes) - 1);
            array_push($this->collection, $collectionCopy[$indexes[$randomIndex]]);

            array_splice($indexes, $randomIndex, 1);
        }

        return $this;
    }

    public function map(\Closure $execution)
    {
        $result = new Collection($this->class);
        $result->mass(array_map($execution, $this->collection));
        
        return $result;
    }

    public function filter(\Closure $execution)
    {
        return array_filter($this->collection, $execution);
    }

    public function pluck($property)
    {
        return array_column($this->collection, $property);
    }

    public function class()
    {
        return $this->class;
    }

    public function toString()
    {
        $toString = "Collection (" . $this->class . "):" . "<br>";
        foreach ($this->collection as $item) 
        {
            $toString .= $this->class . "#" . $this->indexOf($item) . ": " . json_encode($item, JSON_UNESCAPED_UNICODE);
            $toString .= "<br>";
        }

        return rtrim($toString, "<br>");
    }

    // public function toJson()
    // {
    //     $jsonArray = [];
    //     foreach ($this->collection as $item) 
    //     {
    //         $array = (array) $item;
    //         foreach($array as $key => $value)
    //         {
    //             if($value instanceof Collection)
    //             {
    //                 $array[$key] = $value->toJson();
    //             }
    //         }

    //         $jsonArray[] = $array;
    //     }

    //     return $jsonArray;
    // }

    public function raw()
    {
        return $this->collection;
    }

    public function __call(string $name, $arguments)
    {
        if(Str::contains($name, "with"))
        {
            $propName = underscorize(str_replace("with", "", $name));
            foreach ($this->collection as $item) 
            {
                $item->$propName;
            }
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return $this->collection;
    }

    // -----------------------------------------------------------------------------------------------------------

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->collection[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->collection[$offset]) ? $this->collection[$offset] : null;
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->collection[] = $value;
        } else {
            $this->collection[$offset] = $value;
        }
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->collection[$offset]);
    }

    /**
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->collection);
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->collection);
    }
}
