<?php
namespace Zephyr\Model\Paginator;

use Zephyr\Http\Request\Request;
use Zephyr\Database\SqlDialect;
use Zephyr\Database\Table;
use Zephyr\Model\Collection;
use Zephyr\Model\ModelConvertor;
use Zephyr\Database\Connection;
use Traversable;
use Zephyr\View\Generator\Generator;

class Paginator implements \ArrayAccess, \IteratorAggregate, \Countable
{
    private $collection;
    private $perPage;
    private $currentPage;
    private $totalCount;
    private $continues;
    private $eachSide = 3;

    public function __construct($table, $class, $perPage)
    {
        $this->perPage = $perPage;
        $this->totalCount = $this->setTotalCount($class);
        $this->currentPage = $this->resolveCurrentPage();
        $this->collection = $this->setCollection($table, $class);
    }

    public function eachSide(int $value)
    {
        $this->eachSide = $value;
        return $this;
    }

    private function setCollection($table, $class)
    {
        $collection = $this->fetchCollection($table, $class);
        $this->continues = $collection->count() > $this->perPage ? true : false;

        return $collection->partial(0, $this->perPage);
    }

    private function fetchCollection($table, $class)
    {
        $columns = Table::table($table)->schema->columns();

        $sql = SqlDialect::select($table, null, null, null, $this->totalCount, ($this->currentPage - 1) * $this->perPage);
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;

        $collection = new Collection($class);
        foreach ($result as $model) 
        {
            $object = new $class;
            ModelConvertor::convert($model, $object, $columns);
            $collection->push($object);
        }
        
        return $collection;
    }

    private function setTotalCount($class)
    {
        return $class::count();
    }

    private function resolveCurrentPage()
    {
        $request = Request::capture();
        $pageInput = $request->get('page') ? (int)$request->get('page') : 1;
        return $this->pageNoIsValid($pageInput) ? $pageInput : 1;
    }

    private function pageNoIsValid($pageNo)
    {
        return is_int($pageNo) && $pageNo > 0 && $pageNo <= ceil($this->totalCount / $this->perPage) ? true : false;
    }

    public function pages()
    {
        return Generator::render(
            __DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "pages.template.php",
            ["paginator" => $this]
        );
    }

    public function links()
    {
        $totalPages = ceil($this->totalCount / $this->perPage);
        $pages = range(1, $totalPages);

        $links = [];
        if (in_array($this->currentPage, $pages)) 
        {
            $toLeft = range($this->currentPage - $this->eachSide, $this->currentPage - 1);
            $toRight = range($this->currentPage + 1, $this->currentPage + $this->eachSide);

            foreach ($pages as $key => $page) 
            {
                $page = (int)$page;
                // IS FIRST OR LAST
                if (($key == 0 || $key == count($pages) - 1)
                    && $this->pageNoIsValid((int)$page)
                    && !in_array($page, array_column($links, 'page'))
                ) {
                    $links[] = ["page" => (int)$page, 'link' => $this->url($page)];
                    continue;
                }

                // IS CURRENT PAGE
                if (
                    $page == $this->currentPage
                    && $this->pageNoIsValid($page)
                    && !in_array($page, array_column($links, 'page'))
                ) {
                    $links[] = ["page" => (int)$page, 'link' => $this->url($page)];
                    continue;
                }

                // IS ON LEFT SIDE
                if (
                    in_array($page, $toLeft)
                    && $this->pageNoIsValid($page)
                    && !in_array($page, array_column($links, 'page'))
                ) {
                    $links[] = ["page" => $page, 'link' => $this->url($page)];
                    continue;
                }

                // IS ON RIGHT SIDE
                if (
                    in_array($page, $toRight)
                    && $this->pageNoIsValid($page)
                    && !in_array($page, array_column($links, 'page'))
                ) {
                    $links[] = ["page" => $page, 'link' => $this->url($page)];
                    continue;
                }

                // LEFT ...
                if (!in_array($page, $toLeft) && !in_array('...', array_column($links, 'page'))) {
                    $links[] = ["page" => '...', 'link' => null];
                    continue;
                }

                // RIGHT ...
                if (!in_array($page, $toRight) && !in_array('...', array_column($links, 'page'))) {
                    $links[] = ["page" => '...', 'link' => null];
                    continue;
                }
            }
        }

        return $links;
    }

    public function pageCount()
    {
        return $this->collection->count();
    }

    public function currentPage()
    {
        return $this->currentPage;
    }

    public function isCurrentPage($page)
    {
        return $this->currentPage == $page;
    }

    public function isOnFirstPage()
    {
        return $this->currentPage == 1;
    }

    public function first()
    {
        return ($this->currentPage - 1) * $this->perPage + 1;
    }

    public function last()
    {
        return $this->first() + $this->collection->count() - 1;
    }

    public function hasMorePages()
    {
        return $this->continues;
    }

    public function previousPage()
    {
        return $this->pageNoIsValid($this->currentPage - 1) ? $this->currentPage - 1 : null;
    }

    public function previousPageUrl()
    {
        return $this->previousPage() != null ? $this->url($this->previousPage()) : null;
    }

    public function nextPage()
    {
        return $this->pageNoIsValid($this->currentPage + 1) ? $this->currentPage + 1 : null;
    }

    public function nextPageUrl()
    {
        return $this->nextPage() != null ? $this->url($this->nextPage()) : null;
    }

    public function url($page)
    {
        $request = Request::capture();
        $parsedUrl = parse_url($request->fullUrl);

        $path = $parsedUrl['path'];

        $queries = [];
        if (isset($parsedUrl['query'])) {
            $queriesArray = explode('&', $parsedUrl['query']);
            foreach ($queriesArray as $query) {
                $queryKeyValue = explode('=', $query);
                $queries[$queryKeyValue[0]] = $queryKeyValue[1];
            }
        }

        $queries['page'] = (int)$page;
        return base_url() . $path . "?" . http_build_query($queries);
    }

    // ------------------------------------------------------------------------        

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->collection->raw()[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->collection->raw()[$offset]) ? $this->collection->raw()[$offset] : null;
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->collection->raw()[] = $value;
        } else {
            $this->collection->raw()[$offset] = $value;
        }
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->collection->raw()[$offset]);
    }

    /**
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->collection->raw());
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->collection->raw());
    }
}
