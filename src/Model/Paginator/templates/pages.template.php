<?php $paginator = $args['paginator']; ?>

<ul style="list-style: none; padding: 0;">

    <?php if($paginator->isOnFirstPage()): ?>
        <li style="display: inline-block;">&lsaquo;</li>
    <?php else: ?>
        <li style="display: inline-block;"><a href="<?= $paginator->previousPageUrl() ?>">&lsaquo;</a></li>
    <?php endif; ?>

    <?php foreach($paginator->links() as $link): ?> 
        <?php if($link['page'] == '...'): ?>
            <li style="display: inline-block"><?= $link['page'] ?></li>
            <?php continue; ?>
        <?php endif; ?>

        <?php if($link['page'] == $paginator->currentPage()): ?>
            <li style="display: inline-block"><?= $link['page'] ?></li>
        <?php else: ?>    
            <li style="display: inline-block;"><a href="<?= $link['link'] ?>"><?= $link['page'] ?></a></li>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php if($paginator->hasMorePages()): ?>
        <li style="display: inline-block;"><a href="<?= $paginator->nextPageUrl() ?>">&rsaquo;</a></li>
    <?php else: ?>
        <li style="display: inline-block;">&rsaquo;</li>
    <?php endif; ?>
    
</ul>