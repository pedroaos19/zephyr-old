<?php
namespace Zephyr\Model;

use Zephyr\Database\Table;
use Zephyr\Database\SQLGenesis;
use Zephyr\Database\Connection;
use Zephyr\Database\SqlDialect;

class Pivot
{
    private $table;

    public function __construct($tableName)
    {
        $this->table = $tableName;
    }

    public static function bind($first, $second, $table = null, $columns = [])
    {
        if(!empty($columns) && !is_assoc($columns)) die("The columns must be an associative array");

        $pivotTable = $table ? $table : static::resolveTableName($first, $second);
        $pivot = new Pivot($table);

        if (!empty($columns)) 
        {
            foreach($columns as $key => $value)
            {
                $pivot->$key = $value;
            }
        }

        $firstFk = singularize($first->getTable()) . "_id";
        $secondFk = singularize($second->getTable()) . "_id";

        $pivot->$firstFk = $first->id;
        $pivot->$secondFk = $second->id;

        $columns[$firstFk] = $first->id;
        $columns[$secondFk] = $second->id;

        $sql = SqlDialect::insert($pivotTable, array_keys($columns));
        $id = Connection::get()->execute($sql, array_values($columns));

        $pivot->id = $id;
        return $pivot;
    }

    public function save()
    {
        $columns = array_keys($this->variables());
        $values = array_values($this->variables());

        $values[] = $this->id;

        $sql = SqlDialect::update($this->table, $columns);
        Connection::get()->execute($sql, $values);
    }

    public function delete()
    {
        $sql = SqlDialect::delete($this->table);
        Connection::get()->execute($sql, [$this->id]);
    }

    public function variables($withId = false)
    {
        $variables = [];
        $vars = (array)$this;

        foreach ($vars as $name => $value) {
            if ($value !== null && $name != "id") {
                $variables[$name] = $value;
            }
        }

        return $variables;
    }

    public function getTable()
    {
        return $this->table;
    }

    private static function resolveTableName($first, $second)
    {
        $firstTable = singularize($first->getTable());
        $secondTable = singularize($second->getTable());

        $combinedTable = [$firstTable, $secondTable];
        sort($combinedTable);
        $combinedTable = implode("_", $combinedTable);

        if (Table::exists($combinedTable)) 
        {
            return $combinedTable;
        } 
        else 
        {
            die(trigger_error("These objects were not bound yet, no such pivot exists."));
        }
    }
}
