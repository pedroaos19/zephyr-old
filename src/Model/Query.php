<?php

namespace Zephyr\Model;

use Zephyr\Database\Sql;
use Zephyr\Database\Connection;
use Zephyr\Database\Table;
use Zephyr\Model\ModelConvertor;
use Zephyr\Utils\Str;

class Query
{
    const TYPE_WHERE_NORMAL = 1;
    const TYPE_WHERE_BETWEEN = 2;
    const TYPE_WHERE_NOT_BETWEEN = 3;
    const TYPE_WHERE_IN = 4;
    const TYPE_WHERE_NOT_IN = 5;
    const TYPE_WHERE_LIKE = 6;
    const TYPE_WHERE_NOT_LIKE = 7;

    const SIMPLE_EVALS = [
        Sql::EQUALS, Sql::LESS, Sql::LESS_EQUALS, Sql::MORE, Sql::MORE_EQUALS
    ];

    protected $columns;
    protected $table;
    protected $where;
    protected $distinct;
    protected $groupBy;
    protected $having;
    protected $orderBy;
    protected $limit;
    protected $offet;

    protected $forcedClause;

    public function __construct($columns = null, $table)
    {
        $this->columns = empty($columns) ? ["$table.".Sql::ALL_COLS] : $columns;
        $this->table = $table;
        $this->where = [];
        $this->distinct = false;
        $this->groupBy = [];
        $this->having = null;
        $this->orderBy = [];
    
        $this->forcedClause = Sql::WHERE_AND;
    }

    public function where(...$condition)
    {
        switch(count($condition))
        {
            case 1:
                $conditions = $condition[0];
                for($i = 0; $i < count($conditions); $i++)
                {
                    $eval = count($conditions[$i]) < 3 ? "=" : $conditions[$i][1]; 
     
                    $this->where[] = [
                        "type" => Query::TYPE_WHERE_NORMAL,
                        "column" => $conditions[$i][0],
                        "eq" => in_array($eval, Query::SIMPLE_EVALS) ? $eval : "=",
                        "value" => count($conditions[$i]) < 3 ? $conditions[$i][1] : $conditions[$i][2],
                        "clause" => $i === 0 ? $this->forcedClause : Sql::WHERE_AND
                    ];
                }
                break;
            case 2:
                if(is_array($condition[0]) || is_array($condition[1]))
                {
                    die(trigger_error("Please wrap multiple conditions in one single array"));
                }

                $this->where[] = [
                    "type" => Query::TYPE_WHERE_NORMAL,
                    "column" => $condition[0],
                    "eq" => "=",
                    "value" => $condition[1],
                    "clause" => $this->forcedClause
                ];
                break;
            case 3:
                if(is_array($condition[0]) || is_array($condition[1]) || is_array($condition[2]))
                {
                    die(trigger_error("Please wrap multiple conditions in one single array"));
                }

                $this->where[] = [
                    "type" => Query::TYPE_WHERE_NORMAL,
                    "column" => $condition[0],
                    "eq" => $condition[1],
                    "value" => $condition[2],
                    "clause" => $this->forcedClause
                ];
                break;
        }

        return $this;
    }

    public function whereBetween($column, array $array)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_BETWEEN,
            "column" => $column,
            "array" => $array,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function whereNotBetween($column, array $array)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_NOT_BETWEEN,
            "column" => $column,
            "array" => $array,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function whereIn($column, array $array)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_IN,
            "column" => $column,
            "array" => $array,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function whereNotIn($column, array $array)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_NOT_IN,
            "column" => $column,
            "array" => $array,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function whereLike($column, array $pattern)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_LIKE,
            "column" => $column,
            "pattern" => $pattern,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function whereNotLike($column, string $pattern)
    {
        $this->where[] = [
            "type" => Query::TYPE_WHERE_NOT_LIKE,
            "column" => $column,
            "pattern" => $pattern,
            "clause" => $this->forcedClause
        ];

        return $this;
    }

    public function distinct()
    {
        $this->distinct = true;
        return $this;
    }

    public function groupBy(...$groupBy)
    {
        array_merge($this->groupBy, $groupBy);
        return $this;
    }

    public function havingRaw(string $expression)
    {
        $this->having = $expression;
        return $this;
    }

    public function orderBy($column, $desc = false)
    {
        $this->orderBy[] = [
            "column" => $column,
            "order" => $desc ? Sql::DESC : Sql::ASC
        ];

        return $this;
    }

    public function limit(int $limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset(int $offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function toCollection($results)
    {
        $class = Table::table($this->table)->namespace;
        $columns = Table::table($this->table)->schema->columns();
        $results = is_assoc($results) ? [$results] : $results;

        $resultCollection = new Collection($class);

        if(!is_null($results))
        {
            foreach ($results as $result) 
            {
                $object = new $class;
                ModelConvertor::convert($result, $object, $columns);

                $resultCollection->push($object);
            }
        }
        
        return $resultCollection;
    }

    protected function toSql() 
    {
        $values = [];

        $columns = join(", ", $this->columns);
        $distinct = $this->distinct ? Sql::DISTINCT : "";

        $where = "";
        if(!empty($this->where))
        {
            unset($this->where[0]["clause"]);
            foreach ($this->where as $condition) 
            {
                $evaluation = null;
                switch ($condition["type"]) 
                {
                    case Query::TYPE_WHERE_NORMAL:
                        $evaluation = tprintf(Sql::EVAL, $condition["column"], $condition["eq"]);
                        $values[] = $condition["value"];
                        break;
                    case Query::TYPE_WHERE_BETWEEN:
                        $evaluation = tprintf(Sql::BETWEEN, $condition["column"]);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_NOT_BETWEEN:
                        $evaluation = tprintf(Sql::NOT_BETWEEN, $condition["column"]);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_IN:
                        $array = join(", ", array_pad([], count($condition["array"]), "?"));
                        $evaluation = tprintf(Sql::IN, $condition["column"], $array);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_NOT_IN:
                        $array = join(", ", array_pad([], count($condition["array"]), "?"));
                        $evaluation = tprintf(Sql::NOT_IN, $condition["column"], $array);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_LIKE:
                        $evaluation = tprintf(Sql::LIKE, $condition["column"], $condition["pattern"]);
                        $values[] = $condition["value"];
                        break;
                    case Query::TYPE_WHERE_NOT_LIKE:
                        $evaluation = tprintf(Sql::LIKE, $condition["column"], $condition["pattern"]);
                        $values[] = $condition["value"];
                        break;
                }

                if (isset($condition["clause"])) 
                {
                    $evaluation = $condition["clause"] . $evaluation;
                }

                $where .= $evaluation;
            }
        }

        $where = $where === "" ? $where : tprintf(Sql::WHERE, $where);

        $groupBy = !empty($this->groupBy) ? tprintf(Sql::GROUP_BY, join($this->groupBy, ', ')) : "";
        $having = !empty($this->having) ? tprintf(Sql::HAVING, $this->having) : "";

        $orderBy = "";
        if(!empty($this->orderBy))
        {
            $orderByExpressions = array_map(function($oB) {
                return $oB["column"] . " " . $oB["order"];
            }, $this->orderBy);
            $orderBy = join($orderByExpressions, ", ");
        }
        $orderBy = $orderBy === "" ? $orderBy : tprintf(Sql::ORDER_BY, $orderBy);

        $limit = isset($this->limit) ? tprintf(Sql::LIMIT, $this->limit) : "";
        $offset = isset($this->offset) ? tprintf(Sql::OFFSET, $this->offset) : "";

        $sql = tprintf(
            Sql::SELECT,
            $distinct,
            $columns,
            $this->table,
            $where,
            $groupBy,
            $having,
            $orderBy,
            $limit,
            $offset
        ) . ";";
        // if($this->table === "messages")
        //     dd($sql);
        $results = Connection::get()->execute($sql, $values);
        return $results;
    }

    public function get()
    {
        $results = $this->toSql();
        return $this->toCollection($results);
    }

    public function first()
    {
        $this->limit = 1;
        $this->offset = 0;

        $results = $this->toSql();
        $collection = $this->toCollection($results);

        return !$collection->empty() ? $collection->first() : null;
    }

    public function last()
    {
        $this->limit = 1;
        $this->offset = 0;
        
        if(!isset($this->orderBy) || !isset($this->orderBy["column"]))
        {
            $this->orderBy[] = [
                "column" => "id",
                "order" => Sql::DESC
            ];
        }

        $this->orderBy[0]["order"] = Sql::DESC;
        
        $results = $this->toSql();
        return $this->toCollection($results)->first();
    }

    public function chunk(int $length, int $offset = 0)
    {
        $this->limit = $length;
        $this->offset = $offset;

        $results = $this->toSql();
        return $this->toCollection($results);
    }

    public function count()
    {
        $this->columns = [tprintf(Sql::COUNT, Sql::ALL_COLS)];

        $results = $this->toSql();
        return intval($results["count"]);
    }

    public function avg(string $column)
    {
        $this->columns = [tprintf(Sql::AVG, $column)];
        
        $results = $this->toSql();
        return doubleval($results["avg"]);
    }

    public function sum(string $column)
    {
        $this->columns = [tprintf(Sql::COUNT, $column)];

        $results = $this->toSql();
        return doubleval($results["avg"]);
    }

    protected function _($el)
    {
        if(is_string($el))
        {
            return "'$el'";
        }
        if(is_bool($el))
        {
            return $el ? 1 : 0;
        }
        
        return $el;
    }

    public function sql()
    {
        $values = [];

        $columns = join($this->columns, ", ");
        $distinct = $this->distinct ? Sql::DISTINCT : "";

        $where = "";
        if (!empty($this->where)) {
            unset($this->where[0]["clause"]);
            foreach ($this->where as $condition) {
                $evaluation = null;
                switch ($condition["type"]) {
                    case Query::TYPE_WHERE_NORMAL:
                        $evaluation = tprintf(Sql::EVAL, $condition["column"], $condition["eq"]);
                        $values[] = $condition["value"];
                        break;
                    case Query::TYPE_WHERE_BETWEEN:
                        $evaluation = tprintf(Sql::BETWEEN, $condition["column"]);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_NOT_BETWEEN:
                        $evaluation = tprintf(Sql::NOT_BETWEEN, $condition["column"]);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_IN:
                        $array = join(", ", array_pad([], count($condition["array"]), "?"));
                        $evaluation = tprintf(Sql::IN, $condition["column"], $array);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_NOT_IN:
                        $array = join(", ", array_pad([], count($condition["array"]), "?"));
                        $evaluation = tprintf(Sql::NOT_IN, $condition["column"], $array);
                        $values = array_merge($values, $condition["array"]);
                        break;
                    case Query::TYPE_WHERE_LIKE:
                        $evaluation = tprintf(Sql::LIKE, $condition["column"], $condition["pattern"]);
                        $values[] = $condition["value"];
                        break;
                    case Query::TYPE_WHERE_NOT_LIKE:
                        $evaluation = tprintf(Sql::LIKE, $condition["column"], $condition["pattern"]);
                        $values[] = $condition["value"];
                        break;
                }

                if (isset($condition["clause"])) {
                    $evaluation = $condition["clause"] . $evaluation;
                }

                $where .= $evaluation;
            }
        }

        $where = $where === "" ? $where : tprintf(Sql::WHERE, $where);

        $groupBy = !empty($this->groupBy) ? tprintf(Sql::GROUP_BY, join($this->groupBy, ', ')) : "";
        $having = !empty($this->having) ? tprintf(Sql::HAVING, $this->having) : "";

        $orderBy = "";
        if (!empty($this->orderBy)) {
            $orderByExpressions = array_map(function ($oB) {
                return $oB["column"] . " " . $oB["order"];
            }, $this->orderBy);
            $orderBy = join($orderByExpressions, ", ");
        }
        $orderBy = $orderBy === "" ? $orderBy : tprintf(Sql::ORDER_BY, $orderBy);

        $limit = isset($this->limit) ? tprintf(Sql::LIMIT, $this->limit) : "";
        $offset = isset($this->offset) ? tprintf(Sql::OFFSET, $this->offset) : "";

        $sql = tprintf(
            Sql::SELECT,
            $distinct,
            $columns,
            $this->table,
            $where,
            $groupBy,
            $having,
            $orderBy,
            $limit,
            $offset
        ) . ";";

        return $sql;
    }

    public function __call(string $name, array $arguments)
    {
        if(Str::contains($name, "andWhere"))
        {
            $this->forcedClause = Sql::WHERE_AND;
            call_user_func_array([$this, "where"], $arguments);

            return $this;
        }
        else if(Str::contains($name, "orWhere"))
        {
            $this->forcedClause = Sql::WHERE_OR;
            call_user_func_array([$this, "where"], $arguments);
            $this->forcedClause = Sql::WHERE_AND;

            return $this;
        }
        else if(Str::contains($name, "where"))
        {
            $column = underscorize(str_replace("where", "", $name));
            
            array_unshift($arguments, $column);
            call_user_func_array([$this, "where"], $arguments);

            return $this;
        }
        else
        {
            return $this;
        }
    }
}
