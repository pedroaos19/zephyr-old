<?php
namespace Zephyr\Model;

use Carbon\Carbon;
use JsonSerializable;
use Zephyr\Database\Connection;
use Zephyr\Database\SqlDialect;
use Zephyr\Database\Table;
use Zephyr\Model\Paginator\Paginator;

class Model implements JsonSerializable
{
    public function create()
    {
        if (isset($this->id)) 
            die(trigger_error("This model is already created on the database."));

        $table = static::table();

        if (Table::table($table)->hasColumn('created_at')) 
        { 
            $this->created_at = Carbon::now();
        }
        if (Table::table($table)->hasColumn('updated_at')) 
        {
            $this->updated_at = Carbon::now();
        }

        $columns = array_keys($this->variables());
        $values = array_values($this->variables());
                
        $sql = SqlDialect::insert($table, $columns);
        $id = Connection::get()->execute($sql, $values);

        $this->id = $id;
    }

    public function save()
    {
        if (isset($this->id)) 
        {
            $table = static::table();

            if (Table::table($table)->hasColumn('updated_at')) 
            {
                $this->updated_at = Carbon::now();
            }

            $columns = array_keys($this->variables());
            $values = array_values($this->variables());

            $values[] = $this->id;

            $sql = SqlDialect::update($table, $columns);
            Connection::get()->execute($sql, $values);
        } 
        else 
        {
            $this->create();
        }
    }

    public static function get(int $id)
    {
        $table = static::table();
        $columns = Table::table($table)->schema->columns();

        $sql = SqlDialect::select($table, null, ["id"], ["="]);
        $result = Connection::get()->execute($sql, [$id]);

        if(!isset($result)) return null;

        $class = static::fullClass();
        $object = new $class;
        ModelConvertor::convert($result, $object, $columns);

        return $object;
    }

    public static function all()
    {
        $table = self::table();
        $class = self::fullClass();
        $columns = Table::table($table)->schema->columns();

        $sql = SqlDialect::select($table);
        $results = Connection::get()->execute($sql);

        if(!isset($results)) return null;

        $results = is_assoc($results) ? [$results] : $results;
        
        $resultCollection = new Collection($class);
        foreach ($results as $result) 
        {
            $object = new $class;
            ModelConvertor::convert($result, $object, $columns);

            $resultCollection->push($object);
        } 

        return $resultCollection;
    }

    public static function where(array $columns, array $conditions, array $values)
    {
        $table = self::table();
        $class = self::fullClass();
        // dd($table);
        $tableColumns = Table::table($table)->schema->columns();

        $sql = SqlDialect::select($table, null, $columns, $conditions);
        $results = Connection::get()->execute($sql, $values);

        if (!isset($results)) return null;

        $results = is_assoc($results) ? [$results] : $results;

        $resultCollection = new Collection($class);
        foreach ($results as $result) 
        {
            $object = new $class;
            ModelConvertor::convert($result, $object, $tableColumns);

            $resultCollection->push($object);
        } 

        return $resultCollection;
    }

    public static function except($exceptions)
    {
        $query = static::query();

        if($exceptions instanceof Collection || is_array($exceptions))
        {
            foreach($exceptions as $exception)
            {
                if(is_int($exception))
                {
                    $query->where('id', '!=', $exception);
                }
                else
                {
                    $query->where('id', '!=', $exception->id);
                }
            }
        }
        else
        {
            if(is_int($exceptions))
            {
                $query->where('id', '!=', $exceptions);
            }
            else
            {
                $query->where('id', '!=', $exceptions->id);
            }
        }
        
        return $query->get();
    }

    public function delete()
    {
        $table = static::table();

        $sql = SqlDialect::delete($table);
        Connection::get()->execute($sql, [$this->id]);
    }

    public static function paginate($perPage)
    {
        $table = self::table();
        $class = self::fullClass();

        return new Paginator($table, $class, $perPage);        
    }

    public static function count()
    {
        $table = static::table();
        $sql = SqlDialect::count($table);
        $result = Connection::get()->execute($sql);

        return intval($result["count"]);
    }

    public static function query(...$columns)
    {
        return new Query($columns, static::table());
    }

    public function hasMany($class, $columnName = null)
    {
        $fullClassName = explode("\\", $class);
        $className = array_pop($fullClassName);
        $table = tablerize($className);

        $column = $columnName ? $columnName : underscorize(static::class()) . "_id";
        
        return $this->$table = $this->resolveHasMany(
            $this->hasManyTables(), 
            $table, 
            $class, 
            $column
        );
    }

    private function hasManyTables()
    {
        $table = static::table();

        $sql = SqlDialect::hasMany($table);
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;

        return $result;
    }

    private function resolveHasMany($hasMany, $table, $class, $column)
    {
        foreach ($hasMany as $relationship) 
        {
            if ($relationship["table"] === $table && $relationship["fk"] === $column) 
            {
                $columns = Table::table($table)->schema->columns();

                $sql = SqlDialect::select($table, null, [$column], ['=']);
                $result = Connection::get()->execute($sql, [$this->id]);
                $result = is_assoc($result) ? [$result] : $result;

                if($result == null) $result = [];
                
                $collection = new Collection($class);

                foreach ($result as $entry) 
                {
                    $object = new $class;
                    ModelConvertor::convert($entry, $object, $columns);

                    $collection->push($object);
                }
                
                return $collection;
            }
        }

        return null;
    }

    public function belongsTo($class, $columnName = null)
    {
        $fullClassName = explode("\\", $class);
        $className = array_pop($fullClassName);

        $column = $columnName ? $columnName : underscorize($className) . "_id";
        
        $var = underscorize($className);

        $var = underscorize($className);
        return $this->$var = $this->resolveBelongsTo(
            $this->belongsToTables(), 
            $column, 
            $class
        );
    }

    private function belongsToTables()
    {
        $sql = SqlDialect::belongsTo(static::table());
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;

        return $result;
    }

    private function resolveBelongsTo($belongsTo, $column, $class)
    {
        foreach ($belongsTo as $relationship) 
        {
            if ($relationship["COLUMN_NAME"] === $column) 
            {
                $table = $relationship["REFERENCED_TABLE_NAME"];
                $columns = Table::table($table)->schema->columns();

                $sql = SqlDialect::select($table, null, ['id'], ['=']);
                $result = Connection::get()->execute($sql, [$this->$column]);

                $object = new $class;
                ModelConvertor::convert($result, $object, $columns);

                return $object;
            }
        }

        return null;
    }

    public function bind(Model $second, $columns = null, $tableName = null)
    {
        Pivot::bind($this, $second, $tableName, $columns);
    }

    public function belongsToMany($class, $table = null)
    {
        $fullClassName = explode("\\", $class);
        $className = array_pop($fullClassName);

        $firstTable = singularize($this->table());
        $secondTable = singularize(tablerize($className));

        $combinedTable = [$firstTable, $secondTable];
        sort($combinedTable);
        $combinedTable = implode("_", $combinedTable);

        $pivotTable = $table ? $table : $combinedTable;

        $var = tablerize($className);
        return $this->$var = $this->resolveBelongsToMany($pivotTable, $secondTable, $class);
    }

    private function resolveBelongsToMany($table, $endTable, $endClass)
    {
        $hasMany = $this->hasManyTables();
        
        if(!isset($hasMany)) die("No pivot table exists for this objects");

        foreach ($hasMany as $relationship) 
        {
            if ($relationship["table"] === $table) 
            {
                $sql = SqlDialect::select($table, null, [$relationship["fk"]], ['=']);
                $pivots = Connection::get()->execute($sql, [$this->id]);
                $pivots = is_assoc($pivots) ? [$pivots] : $pivots;
                
                $pivotColumns = Table::table($table)->schema->columns();
                
                $endKey = singularize($endTable) . "_id";

                $collection = new Collection($endClass);
                if ($pivots) 
                {
                    foreach ($pivots as $pivot) 
                    {
                        $pivotObject = new Pivot($table);
                        ModelConvertor::convert($pivot, $pivotObject, $pivotColumns);

                        $endObject = $endClass::get($pivotObject->$endKey);
                        $endObject->pivot = $pivotObject;

                        $collection->push($endObject);
                    }
                }

                return $collection;
            }
        }

        return null;
    }

    public function toJson()
    {
        $objectArray = (array)$this;
        
        return json_encode((array)$this, JSON_UNESCAPED_UNICODE);
    }

    public function variables($withId = false)
    {
        $variables = [];
        $vars = (array)$this;

        foreach ($vars as $name => $value) 
        {
            if ($value !== null && $name != "id") 
            {
                $variables[$name] = $value;
            }
        }

        return $variables;
    }

    public static function fullClass()
    {
        return get_called_class();
    }

    public static function class()
    {
        $classNamespace = explode('\\', get_called_class());
        return array_pop($classNamespace);
    }

    public static function table()
    {
        return tablerize(self::class());
    }

    public function getTable()
    {
        return static::table();
    }

    public function toString()
    {
        $result = "[Id: $this->id] (";

        foreach($this->variables() as $key => $value)
        {
            $result .= "$key: $value; ";
        }

        $result = rtrim($result) . ")";

        return $result;
    }

    public function __get($prop_name)
    {
        if(isset($this->prop_name))
            return $this->prop_name;

        return method_exists($this, $prop_name) ? $this->$prop_name() : $this->$prop_name;
    }

    public function jsonSerialize()
    {
        return (array)$this;
    }
}
