<?php
    namespace Zephyr\Model;

    use Carbon\Carbon;

    class ModelConvertor
    {
        public static function convert($data, $object, $columns)
        {
            foreach($columns as $column)
            {                
                $type = $column->type;
                if(isset($data[$column->name])) 
                {
                    $key = $column->name;
                    $object->$key = static::resolve($data[$key], $type);
                }
                else
                {
                    $key = $column->name;
                    $object->$key = null;
                }
            }

        }

        private static function resolve($rawVar, $type)
        {
            if($rawVar == null) return null;

            if(strpos($type, "(") > -1)
            {
                $type = substr($type, 0, strpos($type, "("));
            }
            
            $returnVar = null;
           
            switch ($type)
            {
                case "VARCHAR":case "VARCHAR(250)":case "TEXT":case "CHAR":case "CHAR(1)":case "TINYTEXT":case "MEDIUMTEXT":case "LONGTEXT":
                    $returnVar = strval($rawVar);
                    break;
                case "BIT":
                    $returnVar = boolval($rawVar);
                    break;
                case "INT":case "TINYINT":case "SMALLINT":case "MEDIUMINT":case "BIGINT":
                    $returnVar = intval($rawVar);
                    break;
                case "FLOAT":case "FLOAT(10,2)":
                    $returnVar = floatval($rawVar);
                    break;
                case "DOUBLE":case "DOUBLE(16,4)":
                    $returnVar = doubleval($rawVar);
                    break;
                case "DATE":
                    $returnVar = Carbon::createFromFormat('Y-m-d H:i:s', $rawVar . " 00:00:00");
                    break;
                case "TIME":
                    $returnVar = Carbon::createFromFormat("Y-m-d H:i:s", "1970-01-01 ".$rawVar.":00");
                    break;
                case "DATETIME":case "TIMESTAMP":
                    $returnVar = Carbon::createFromFormat('Y-m-d H:i:s', $rawVar);
                    break;
                default:
                    $returnVar = null;
                    break;
            }

            return $returnVar;
        }
    }