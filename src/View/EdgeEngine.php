<?php
namespace Zephyr\View;

require 'vendor/autoload.php';

use Windwalker\Edge\Edge;
use Windwalker\Edge\Loader\EdgeFileLoader;
use Windwalker\Edge\Cache\EdgeFileCache;
use Zephyr\View\Directive\Bootstrap;
use Zephyr\View\Directive\BootstrapSelect;
use Zephyr\View\Directive\BootstrapTables;
use Zephyr\View\Directive\Jquery;
use Zephyr\View\Directive\FontAwesome;
use Zephyr\View\Directive\Materialize;
use Zephyr\View\Directive\BootstrapColorPicker;
use Zephyr\View\Directive\Mercury;

final class EdgeEngine
{
    private $paths = [
        'View',
        'view',
        'src/Charon/view',
        'src/Atlas/View'
    ];

    private $edge;
    private $edgeFileLoader;

    public static function edge()
    {
        static $instance = null;

        if ($instance == null) 
        {
            $instance = new EdgeEngine();
        }

        return $instance;
    }

    private function __construct()
    {
        $this->edgeFileLoader = new EdgeFileLoader($this->paths);
        $this->edgeFileLoader->addFileExtension('.edge.php');

        $this->edge = new Edge($this->edgeFileLoader, null, new EdgeFileCache('cache'));
        $this->edge->addExtension(new Bootstrap(), null);
        $this->edge->addExtension(new Jquery(), null);
        $this->edge->addExtension(new FontAwesome(), null);
        $this->edge->addExtension(new BootstrapTables(), null);
        $this->edge->addExtension(new BootstrapSelect(), null);
        $this->edge->addExtension(new BootstrapColorPicker(), null);
        $this->edge->addExtension(new Materialize(), null);
        $this->edge->addExtension(new Mercury(), null);
    }

    public function addPath(string $path)
    {
        array_push($this->paths, $path);

        $this->edgeFileLoader->setPaths($this->paths);
        $this->edge->setLoader($this->edgeFileLoader);
    }

    public function getPaths()
    {
        return $this->paths;
    }

    public function render(string $filename, array $data)
    {
        return $this->edge->render($filename, $data);
    }
}
