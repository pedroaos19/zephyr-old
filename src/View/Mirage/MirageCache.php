<?php

namespace Zephyr\View\Mirage;

use Zephyr\Filesystem\Filesystem;

class MirageCache
{
    protected $folder = "cache_two";

    public function isExpired($path)
    {
        $cachePath = $this->getCacheFile($this->getCacheKey($path));

        if(!is_file($cachePath)) return true;

        return filemtime($path) >= filemtime($cachePath);
    }

    public function getCacheFile($key)
    {
        return $this->folder . DIRECTORY_SEPARATOR . "~" . $key;
    }

    public function getCacheKey($path)
    {
        return md5($path);
    }
}
