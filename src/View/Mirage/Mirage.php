<?php

namespace Zephyr\View\Mirage;

class Mirage 
{
    protected $renderCount;
    protected $loader;
    protected $compiler;
    protected $cache;
    protected $extensions;

    // protected $paths

    public function __construct()
    {
        $this->loader = new MirageLoader;
        $this->compiler = new MirageCompiler;
        $this->cache = new MirageCache;
    }

    public function render($layout)
    {
        $this->incrementRender();

        $path = $this->loader->find($layout);
        if($this->cache->isExpired($path))
        {
            $this->prepareExtensions();
            $this->cache->store(
                $path, 
                $this->compiler->compile($this->loader->load($path))
            );
        }

        echo $path;
        echo "<br>finalized";
    }

    protected function incrementRender()
    {
        $this->renderCount++;
    }

    protected function prepareExtensions()
    {
        foreach ($this->extensions as $extension) 
        {
            foreach($extension->getDirectives() as $name => $directive)
            {
                $this->compiler->directive($name, $directive);
            }

            foreach ($extension->getParsers() as $parser) 
            {
                $this->compiler->parser($parser);
            }
        }
    }
}