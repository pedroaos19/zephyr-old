<?php

namespace Zephyr\View\Mirage;

interface MirageExtensionInterface
{
    public function getName();
    public function getDirectives();
    public function getGlobals();
    public function getParsers();
}