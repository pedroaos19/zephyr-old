<?php

namespace Zephyr\View\Mirage;

use Zephyr\Utils\Str;

class MirageCompiler
{
    protected $directives = [];
    protected $parsers = [];

    protected $verbatimBlocks = [];
    protected $verbatimPlaceholder = '@__verbatim__@';

    public function compile($value)
    {
        $result = '';

        if(Str::contains($value, '@verbatim')) {
            $value = $this->storeVerbatimBlocks($value);
        }
    }

    protected function storeVerbatimBlocks($value)
    {
        return preg_replace_callback('/(?<!@)@verbatim(.*?)@endverbatim/s', function ($matches) {
            $this->verbatimBlocks[] = $matches[1];

            return $this->verbatimPlaceholder;
        }, $value);
    }

    public function directive($name, $handler)
    {
        $this->directives[$name] = $handler;
        return $this;
    }

    public function parser($handler)
    {
        $this->directives[] = $handler;
        return $this;
    }
}
