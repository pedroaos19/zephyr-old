<?php

namespace Zephyr\View\Mirage;

use Zephyr\Filesystem\Filesystem;

class MirageLoader
{
    protected $extensions = ['.mirage.php'];
    protected $folders = [
        'view',
        'src/Atlas/View'
    ];

    public function __construct($folders = null)
    {
        // merge paths?
    }

    public function find($path)
    {
        $fullPath = null;

        foreach($this->folders as $folder)
        {
            foreach($this->extensions as $extension)
            {   
                if(Filesystem::exists($folder . DIRECTORY_SEPARATOR . $path . $extension)) 
                {
                    $fullPath = $folder . DIRECTORY_SEPARATOR . $path . $extension;
                    break 2;
                } 
            }
        }

        if(is_null($fullPath))
        {
            throw new \UnexpectedValueException('View file not found: ' . $path);
        }

        return $fullPath;
    }

    public function load($path)
    {
        return Filesystem::file($path)->contents();
    }

    protected function normalize($path)
    {
        return str_replace('.', '/', $path);
    }
}
