<?php
namespace Zephyr\View;

use Zephyr\Filesystem\Filesystem;

class View
{
    public static function render(string $filename, array $data = null)
    {
        if (self::viewExists($filename)) 
        {
            $dataArray = isset($data) ? $data : [];
            return EdgeEngine::edge()->render($filename, $dataArray);
        } 
        else 
        {
            die("View file doesn't exist in the current View paths!");
        }
    }

    private static function viewExists(string $filename)
    {
        foreach (EdgeEngine::edge()->getPaths() as $path) 
        {
            if (Filesystem::exists("$path/$filename.edge.php")) 
            {
                return true;
            }
        }

        return false;
    }

    public static function addPath(string $path)
    {
        EdgeEngine::edge()->addPath($path);
    }
}
