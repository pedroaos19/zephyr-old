<?php

namespace Zephyr\View\Generator;

class Generator
{
    public static function render($template, $args = null)
    {
        ob_start();
        include($template);
        $var = ob_get_contents();
        ob_end_clean();
        echo $var;
    }
}
