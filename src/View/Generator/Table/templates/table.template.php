<table <?= $args["id"] ?> class="atlas-table table table-striped table-bordered <?= $args["class"] ?>" style="width:100%">
    <thead>
        <?php foreach($args["headers"] as $header):?>
            <th><?= $header ?></th>
        <?php endforeach ?>
    </thead>
    <tbody>
        <?php foreach($args["data"] as $dataRow):?>
            <tr>
                <?php foreach($dataRow as $data):?>
                    <th><?= $data ?></th>
                <?php endforeach ?>
            </tr>
        <?php endforeach ?>
    </tbody>            
</table>