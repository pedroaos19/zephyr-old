<?php
    namespace Zephyr\View\Generator\Table;

    class Table
    {
        public static function render($collection, $options)
        {
            $headers = $collection["th"];
            $data = $collection["data"];
            $id = isset($options["id"]) ? "id=\"" . $options["id"] . "\"" : "";
            $class = isset($options["class"]) ? $options["class"] : "";
            $args = array('headers' => $headers, 'data' => $data, "id" => $id, "class" => $class);
            return self::resolve($args);
        }

        public static function resolve($args)
        {
            ob_start();
            include(__DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "table.template.php");
            $var=ob_get_contents(); 
            ob_end_clean();
            return $var;
        }
    }