<?php

namespace Zephyr\View\Directive;

class BootstrapColorPicker implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'bootstrap_color_picker';
	}

	public function getDirectives()
	{
		return array(
			'bootstrap_color_picker_css' => array($this, 'bootstrap_color_picker_css'),
			'bootstrap_color_picker_js' => array($this, 'bootstrap_color_picker_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function bootstrap_css()
	{
        $css = "<link rel='stylesheet' href='" . asset("css/bootstrap-select.min.css") . "'/> ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function bootstrap_js()
	{
        $js = "<script src='" . asset("js/bootstrap-select.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}