<?php

namespace Zephyr\View\Directive;

class Bootstrap implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'bootstrap';
	}

	public function getDirectives()
	{
		return array(
			'bootstrap_css' => array($this, 'bootstrap_css'),
			'bootstrap_js' => array($this, 'bootstrap_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function bootstrap_css()
	{
        $css = "<link rel='stylesheet' href='" . asset("css/bootstrap.min.css") . "'/> ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function bootstrap_js()
	{
        $js = "<script src='" . asset("js/bootstrap.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}