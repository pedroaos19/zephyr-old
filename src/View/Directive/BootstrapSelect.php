<?php

namespace Zephyr\View\Directive;

class BootstrapSelect implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'bootstrap_select';
	}

	public function getDirectives()
	{
		return array(
			'bootstrap_select_css' => array($this, 'bootstrap_select_css'),
			'bootstrap_select_js' => array($this, 'bootstrap_select_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function bootstrap_select_css()
	{
        $css = "<link rel='stylesheet' href='" . asset("css/bootstrap-select.min.css") . "'/> ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function bootstrap_select_js()
	{
        $js = "<script src='" . asset("js/bootstrap-select.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}