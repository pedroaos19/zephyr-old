<?php

namespace Zephyr\View\Directive;

class FontAwesome implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'fontawesome';
	}

	public function getDirectives()
	{
		return array(
			'fontawesome_css' => array($this, 'fontawesome_css'),
			'fontawesome_js' => array($this, 'fontawesome_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function fontawesome_css()
	{
        $css = "<link rel='stylesheet' href='" . asset("css/fontawesome.min.css") . "'/> ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function fontawesome_js()
	{
        $js = "<script src='" . asset("js/fontawesome.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}