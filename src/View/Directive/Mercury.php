<?php

namespace Zephyr\View\Directive;

use Zephyr\Mercury\Assets;

class Mercury implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
    public function getName()
    {
        return 'mercury';
    }

    public function getDirectives()
    {
        return array(
            'mercury' => array($this, 'mercury'),
        );
    }

    public function getGlobals()
    {
        return array(
            'global_test' => 'global_test'
        );
    }

    public function getParsers()
    {
        return array();
    }

    public function mercury()
    {
        $js = "<script src='" . Assets::javascript() . "'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
    }
}
