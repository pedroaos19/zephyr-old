<?php

namespace Zephyr\View\Directive;

class Materialize implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'materialize';
	}

	public function getDirectives()
	{
		return array(
			'materialize_css' => array($this, 'materialize_css'),
			'materialize_js' => array($this, 'materialize_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function bootstrap_css()
	{
        $css = "<link rel='stylesheet' href='" . asset("css/materialize.min.css") . "'/> ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function bootstrap_js()
	{
        $js = "<script src='" . asset("js/materialize.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}