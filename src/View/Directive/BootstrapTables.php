<?php

namespace Zephyr\View\Directive;

class BootstrapTables implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'bootstrap_tables';
	}

	public function getDirectives()
	{
		return array(
			'bootstrap_tables_css' => array($this, 'bootstrap_tables_css'),
			'bootstrap_tables_js' => array($this, 'bootstrap_tables_js'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function bootstrap_tables_css()
	{
        $css = "
            <link rel='stylesheet' href='" . asset("css/dataTables.bootstrap4.min.css") . "'/> 
            <link rel='stylesheet' href='" . asset("css/responsive.bootstrap4.min.css") . "'/> 
        ";
        return sprintf("<?php echo \"%s\"; ?>", $css);
	}

	public function bootstrap_tables_js()
	{
        $js = "
            <script src='" . asset("js/jquery.dataTables.min.js") ."'></script>
            <script src='" . asset("js/dataTables.bootstrap4.min.js") ."'></script>
            <script src='" . asset("js/dataTables.responsive.min.js") ."'></script>
            <script src='" . asset("js/responsive.bootstrap4.min.js") ."'></script>
        ";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}