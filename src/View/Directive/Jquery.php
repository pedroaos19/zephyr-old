<?php

namespace Zephyr\View\Directive;

class Jquery implements \Windwalker\Edge\Extension\EdgeExtensionInterface
{
	public function getName()
	{
		return 'jquery';
	}

	public function getDirectives()
	{
		return array(
			'jquery' => array($this, 'jquery'),
		);
	}

	public function getGlobals()
	{
		return array(
			'global_test' => 'global_test'
		);
	}

	public function getParsers()
	{
		return array();
	}

	public function jquery()
	{
        $js = "<script src='" . asset("js/jquery.min.js") ."'></script>";
        return sprintf("<?php echo \"%s\"; ?>", $js);
	}
}