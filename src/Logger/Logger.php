<?php
    namespace Zephyr\Logger;

    class Logger
    {
        private static $instance = null;
        private $folderPath;

        private function __construct() 
        {
            $this->folderPath = "log" . DIRECTORY_SEPARATOR;
        }

        public static function get()
        {
            if(static::$instance == null)
            {
                static::$instance = new Logger();
            }

            return static::$instance;
        }

        public function info($message, $tag = null)
        {
            $this->write("info", $message, $tag);
        }

        public function debug($message, $tag = null)
        {
            $this->write("debug", $message, $tag);
        }

        public function error($message, $tag = null)
        {
            $this->write("error", $message, $tag);
        }

        private function write($file, $message, $tag = null)
        {
            $message = $tag ? "[" . $tag .  "] " . $message : $message;
            $finalMessage = strtoupper($file) . ": " . $message . PHP_EOL;
            
            if(!file_exists($this->folderPath . $file . ".log"))
            {
                $fileTemp = fopen($this->folderPath . $file . ".log", "w");
                fclose($fileTemp);
            }

            $file = fopen($this->folderPath . $file . ".log", "a");
            fwrite($file, $finalMessage);

            fclose($file);
        }
    }