<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Zephyr\Filesystem\Filesystem;

class CreateMigrationCommand extends Command
{
    protected $commandName = 'make:migration';
    protected $commandDescription = "Creates a new migration";

    protected $commandArgumentName = "name";
    protected $commandArgumentNameDescription = "Name of the new migration";

    protected $commandArgumentTable = "table";
    protected $commandArgumentTableDescription = "Name of the table";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentNameDescription
            )
            ->addArgument(
                $this->commandArgumentTable,
                InputArgument::REQUIRED,
                $this->commandArgumentTableDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = Filesystem::file('src/Boreas/templates/migration.template')->contents();

        $name = $input->getArgument($this->commandArgumentName);
        $className = camelize($name);

        $tableName = $input->getArgument($this->commandArgumentTable);

        $template = str_replace("@className", $className, $template);
        $template = str_replace("@tableName", $tableName, $template);

        if (!Filesystem::exists('database/migrations'))
            Filesystem::createDirectory('database/migrations');

        Filesystem::createFile("database/migrations/" . time() . "_$name.php")->putContents($template);
        $output->writeln("Migration created successfully.");
    }
}
