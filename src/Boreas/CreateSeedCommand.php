<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Zephyr\Filesystem\Filesystem;

class CreateSeedCommand extends Command
{
    protected $commandName = 'make:seed';
    protected $commandDescription = "Creates a new seed";

    protected $commandArgumentName = "name";
    protected $commandArgumentNameDescription = "Name of the new seed";

    protected $commandArgumentTable = "table";
    protected $commandArgumentTableDescription = "Name of the table";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentNameDescription
            )
            ->addArgument(
                $this->commandArgumentTable,
                InputArgument::REQUIRED,
                $this->commandArgumentTableDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = Filesystem::file('src/Boreas/templates/seed.template')->contents();

        $className = $input->getArgument($this->commandArgumentName);

        $tableName = $input->getArgument($this->commandArgumentTable);
        $modelName = classerize($tableName, true);

        $template = str_replace("@className", $className, $template);
        $template = str_replace("@modelName", $modelName, $template);

        if (!Filesystem::exists('database/seeds'))
            Filesystem::createDirectory('database/seeds');

        Filesystem::createFile("database/seeds/$className.php")->putContents($template);
        $output->writeln("Seed created successfully.");
    }
}
