<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Zephyr\Database\Migration\Migrator;

class MigrationRollbackCommand extends Command
{
    protected $commandName = 'migration:rollback';
    protected $commandDescription = "Rolls back the last Migration batch";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrator = new Migrator();
        $migrator->runRollback();
        $output->writeln("Rolled back successfully.");
    }
}
