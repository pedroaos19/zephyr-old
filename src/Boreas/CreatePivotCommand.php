<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Zephyr\Filesystem\Filesystem;

class CreatePivotCommand extends Command
{
    protected $commandName = 'make:pivot';
    protected $commandDescription = "Creates a new pivot migration";

    protected $commandArgumentName = "name";
    protected $commandArgumentNameDescription = "Name of the new pivot migration";

    protected $commandArgumentFirstTable = "firstTable";
    protected $commandArgumentFirstTableDescription = "Name of the first table";

    protected $commandArgumentSecondTable = "secondTable";
    protected $commandArgumentSecondTableDescription = "Name of the second table";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentNameDescription
            )
            ->addArgument(
                $this->commandArgumentFirstTable,
                InputArgument::REQUIRED,
                $this->commandArgumentFirstTableDescription
            )
            ->addArgument(
                $this->commandArgumentSecondTable,
                InputArgument::REQUIRED,
                $this->commandArgumentSecondTableDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = Filesystem::file('src/Boreas/templates/pivot.template')->contents();

        $name = $input->getArgument($this->commandArgumentName);
        $className = camelize($name);

        $firstTable = $input->getArgument($this->commandArgumentFirstTable);
        $secondTable = $input->getArgument($this->commandArgumentSecondTable);
        $tablesArray = [singularize($firstTable), singularize($secondTable)];
        sort($tablesArray);
        $tableName = implode("_", $tablesArray);

        $template = str_replace("@className", $className, $template);
        $template = str_replace("@tableName", $tableName, $template);
        $template = str_replace("@firstTable", $tablesArray[0], $template);
        $template = str_replace("@secondTable", $tablesArray[1], $template);

        if (!Filesystem::exists('database/migrations'))
            Filesystem::createDirectory('database/migrations');

        Filesystem::createFile("database/migrations/" . time() . "_$name.php")->putContents($template);
        $output->writeln("Pivot created successfully.");
    }
}
