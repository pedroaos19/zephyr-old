<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use Zephyr\Database\Seed\Seeder;

class SeedUpCommand extends Command
{
    protected $commandName = 'seed:up';
    protected $commandDescription = "Rolls Migration batch up";

    protected $commandArgumentClass = "name";
    protected $commandArgumentClassDescription = "Seed's class";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentClass,
                InputArgument::REQUIRED,
                $this->commandArgumentClassDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument($this->commandArgumentClass);

        $seeder = new Seeder();
        $seeder->seedUp($class);
        $output->writeln($class . " planted in table.");
    }
}
