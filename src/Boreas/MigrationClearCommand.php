<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Zephyr\Database\Migration\Migrator;

class MigrationClearCommand extends Command
{
    protected $commandName = 'migration:clear';
    protected $commandDescription = "Rolls back all migrations";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrator = new Migrator();
        $migrator->rollbackAll();
        $output->writeln("Migrations cleared successfully.");
    }
}
