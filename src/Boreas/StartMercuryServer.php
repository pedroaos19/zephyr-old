<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Zephyr\Mercury\Mercury;

class StartMercuryServer extends Command
{
    protected $commandName = 'mercury:run';
    protected $commandDescription = "Starts the Mercury websocket server.";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('WebSocket server started on port 5555!');

        $hubClass = conf()->get('mercury.hub');
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new $hubClass()
                )
            ),
            5555
        );
        $server->run();
    }
}
