<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Zephyr\Filesystem\Filesystem;

class CreateAuthCommand extends Command
{
    protected $commandName = 'make:auth';
    protected $commandDescription = "Creates a new authentication system";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->model();
        $this->migration();
        $this->controller();
        $this->layoutView();
        $this->loginView();
        $this->registerView();
        $this->routes();

        $output->writeln("Authentication built successfully.");
    }

    protected function model()
    {
        $template = Filesystem::file('src/Boreas/templates/auth/role.model.template')->contents();
        Filesystem::createFile('app/Model/Role.php')->putContents($template);

        $template = Filesystem::file('src/Boreas/templates/auth/member.model.template')->contents();
        Filesystem::createFile('app/Model/Member.php')->putContents($template);
    }

    protected function migration()
    {
        $template = Filesystem::file('src/Boreas/templates/auth/role.migration.template')->contents();
        Filesystem::createFile('database/migrations/' . time() . "_create_roles_table.php")
            ->putContents($template);

        usleep(500000);

        $template = Filesystem::file('src/Boreas/templates/auth/member.migration.template')->contents();
        Filesystem::createFile('database/migrations/' . time() . "_create_members_table.php")
            ->putContents($template);

        usleep(500000);

        $template = Filesystem::file('src/Boreas/templates/auth/member-role.pivot.template')->contents();
        Filesystem::createFile('database/migrations/' . time() . "_create_member_role_pivot.php")
            ->putContents($template);
    }

    protected function controller()
    {
        $template = Filesystem::file('src/Boreas/templates/auth/account.controller.template')->contents();
        Filesystem::createFile('app/Controller/AccountController.php')
            ->putContents($template);

        $template = Filesystem::file('src/Boreas/templates/auth/account-api.controller.template')->contents();
        Filesystem::createFile('app/Controller/AccountApiController.php')
            ->putContents($template);
    }

    protected function layoutView()
    {
        Filesystem::createDirectory('view/account/layouts');

        $template = Filesystem::file('src/Boreas/templates/auth/layout.view.template')->contents();
        Filesystem::createFile('view/account/layouts/master.edge.php')
            ->putContents($template);
    }

    protected function loginView()
    {
        $template = Filesystem::file('src/Boreas/templates/auth/login.view.template')->contents();
        Filesystem::createFile('view/account/login.edge.php')
            ->putContents($template);
    }

    protected function registerView()
    {
        $template = Filesystem::file('src/Boreas/templates/auth/register.view.template')->contents();
        Filesystem::createFile('view/account/register.edge.php')
            ->putContents($template);
    }

    protected function routes()
    {
        $routes = Filesystem::file('routes/routes.php')->contents();
        $routes .= PHP_EOL . "membership_routes();" . PHP_EOL;

        Filesystem::file('routes/routes.php')->putContents($routes);
    }
}
