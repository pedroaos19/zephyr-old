<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Zephyr\Database\Migration\Migrator;

class MigrationRefreshCommand extends Command
{
    protected $commandName = 'migration:refresh';
    protected $commandDescription = "Rolls back all migrations and then runs all of them again";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrator = new Migrator();
        $migrator->refresh();
        $output->writeln("Migrations refreshed successfully.");
    }
}
