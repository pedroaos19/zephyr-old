<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Zephyr\Filesystem\Filesystem;

class CreateControllerCommand extends Command
{
    protected $commandName = 'make:controller';
    protected $commandDescription = "Creates a new controller";

    protected $commandArgumentName = "name";
    protected $commandArgumentNameDescription = "Name of the new controller";

    protected $commandCrudOptionName = "crud";
    protected $commandCrudOptionDescription = 'If set, creates the typical CRUD methods';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentNameDescription
            )
            ->addOption(
                $this->commandCrudOptionName,
                "t",
                InputOption::VALUE_NONE,
                $this->commandCrudOptionDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument($this->commandArgumentName);

        if ($input->getOption($this->commandCrudOptionName) != null) 
        {
            $template = Filesystem::file('src/Boreas/templates/controller.crud.template')->contents();
            $template = str_replace("@name", $name, $template);

            Filesystem::file("app/Controller/$name.php")->putContents($template);
        } 
        else 
        {
            $template = Filesystem::file('src/Boreas/templates/controller.template')->contents();
            $template = str_replace("@name", $name, $template);

            Filesystem::createFile("app/Controller/$name.php")->putContents($template);
        }

        $output->writeln("Controller created successfully.");
    }
}
