<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\PhpExecutableFinder;

class ServeCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('serve')
            ->setDescription('Starts the development server')
            ->addOption(
                'host',
                'ht',
                InputOption::VALUE_OPTIONAL,
                'Host used to serve the application',
                '127.0.0.1'
            )
            ->addOption(
                'port',
                'pt',
                InputOption::VALUE_OPTIONAL,
                'Port used to serve the application',
                '8000'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $php = (new PhpExecutableFinder)->find(false);
        $host = $input->getOption('host');
        $port = $input->getOption('port');

        $output->writeln("Serving on " . $host . ":" . $port);
        passthru($php . ' -S ' . $host . ":" . $port);
    }
}
