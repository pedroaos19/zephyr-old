<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Zephyr\Filesystem\Filesystem;

class CreateModelCommand extends Command
{
    protected $commandName = 'make:model';
    protected $commandDescription = "Creates a new Model";

    protected $commandArgumentName = "name";
    protected $commandArgumentNameDescription = "Name of the new model";

    protected $commandTableOptionName = "table";
    protected $commandTableOptionDescription = 'If set, creates a migration for the model';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentNameDescription
            )
            ->addOption(
                $this->commandTableOptionName,
                "t",
                InputOption::VALUE_NONE,
                $this->commandTableOptionDescription
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = Filesystem::file('src/Boreas/templates/model.template')->contents();

        $name = $input->getArgument($this->commandArgumentName);

        $template = str_replace("@className", $name, $template);
        Filesystem::createFile("app/Model/$name.php")->putContents($template);

        if ($input->getOption($this->commandTableOptionName) != null) 
        {
            try 
            {
                $tableName = tablerize($name);

                $migrationCommand = $this->getApplication()->find('make:migration');
                $arguments = array(
                    'command' => 'make:migration',
                    'name' => 'create_' . $tableName . "_table",
                    'table' => $tableName,
                );

                $argumentsArray = new ArrayInput($arguments);
                $migrationCommand->run($argumentsArray, $output);
            } 
            catch (Exception $e) 
            {
                die(trigger_error($e->getMessage()));
            }
        }

        $output->writeln("Model created successfully.");
    }
}
