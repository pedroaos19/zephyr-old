<?php
namespace Zephyr\Boreas;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Zephyr\Database\Migration\Migrator;

class MigrationUpCommand extends Command
{
    protected $commandName = 'migration:up';
    protected $commandDescription = "Rolls Migration batch up";

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrator = new Migrator();
        $migrator->runUp();
        $output->writeln("Migrated successfully.");
    }
}
