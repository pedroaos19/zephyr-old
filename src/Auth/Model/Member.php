<?php
    namespace Zephyr\Auth\Model;

    use Zephyr\Model\Model;
    use Zephyr\Auth\Contracts\RoleTrait;

    class Member extends Model
    {
        use RoleTrait;
    }
