<?php
    namespace Zephyr\Auth\Model;

    use Zephyr\Model\Model;

    class Role extends Model
    {
        public static function called($name)
        {
            $result = static::where(['name'], ['='], [$name]);
            return $result->count() === 1 ? $result->first() : null;
        }
    }