<?php
namespace Zephyr\Auth\Contracts;

use Zephyr\Auth\Model\Member;
use Zephyr\Auth\Model\Role;

trait RoleTrait
{
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function assignRole($role)
    {
        if (get_class($role) == "Zephyr\\Model\\Collection" || is_array($role)) 
        {
            foreach ($role as $singleRole) 
            {
                $this->bind($singleRole);
            }
        } 
        else if (get_class($role) == "Zephyr\\Auth\\Model\\Role") 
        {
            $this->bind($role);
        } 
        else if (is_string($role)) 
        {
            $roleObj = Role::called($role);
            if ($roleObj) $this->bind($roleObj);
        }
    }

    public function has($role)
    {
        $searchRole = $role;
        if (is_string($role)) $searchRole = Role::called($role);

        if ($searchRole) 
        {
            foreach ($this->roles as $roleObj) 
            {
                if ($roleObj->id === $searchRole->id)
                    return true;
            }
        } 
        else 
        {
            die(trigger_error("Role not found!"));
        }

        return false;
    }

    public function hasNot($role)
    {
        return !$this->has($role);
    }

    public function removeRole(Role $role)
    {
        if (isset($role->pivot)) 
        {
            $role->pivot->delete();
        } 
        else 
        {
            $roleObj = $this->roles->get($role->id);
            $roleObj->pivot->delete();
        }
    }

    public function clearRoles()
    {
        foreach ($this->roles as $role) 
        {
            $role->pivot->delete();
        }
    }
}
