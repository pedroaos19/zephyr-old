<?php

namespace Zephyr\Auth\Middleware;

use Zephyr\Http\Request\Request;
use Closure;

class CheckAuthenticated
{
    public function run(Request $request, Closure $next)
    {
        if (auth()) {
            return redirect('/');
        }

        return $next($request);
    }
}
