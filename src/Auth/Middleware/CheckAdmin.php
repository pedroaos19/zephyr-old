<?php
namespace Zephyr\Auth\Middleware;

use Zephyr\Http\Request\Request;
use Closure;

class CheckAdmin
{
    public function run(Request $request, Closure $next)
    {
        if (!membership()->member()->has('admin')) {
            return redirect('/');
        }

        return $next($request);
    }
}
