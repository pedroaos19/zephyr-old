<?php

namespace Zephyr\Auth\Middleware;

use Zephyr\Http\Request\Request;
use Closure;

class CheckNotAuthenticated
{
    public function run(Request $request, Closure $next)
    {
        if (!auth()) {
            return redirect('/');
        }

        return $next($request);
    }
}
