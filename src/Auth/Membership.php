<?php
namespace Zephyr\Auth;

use Zephyr\Http\Request\Request;

class Membership
{
    private static $instance = null;

    private function __construct()
    {
        $this->model = conf()->get('auth.model');
        $this->column = conf()->get('auth.column');
        $this->session = &Request::capture()->session;

        if (in_array($this->column, ['id', 'password', 'created_at', 'updated_at'])) {
            die(trigger_error("Cannot use this column for a member registration."));
        }
    }

    public static function use()
    {
        if (is_null(self::$instance)) 
        {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function identifier()
    {
        return "login_membership_" . sha1(static::class);
    }

    public function register($column, $password, $data = null)
    {
        $match = $this->model::query()->where($this->column, $column)->first();
        if (!is_null($match)) 
        {
            die(trigger_error("This member is already registered."));
        }

        $member = new $this->model;

        $colName = $this->column;
        $member->$colName = $column;
        $member->password = encrypt($password);

        if ($data) 
        {
            foreach ($data as $key => $value) 
            {
                $member->$key = $value;
            }
        }

        $member->create();
        static::login($member->$colName, $password);
    }

    public function login($column, $password)
    {
        $this->session->regenerate();

        if($this->validate($column, $password))
        {
            $member = $this->model::query()->where($this->column, $column)->first();

            $this->session->set($this->identifier(), $member->id);
            $this->member = $member;
        }
    }

    protected function validate($column, $password)
    {
        $match = $this->model::query()->where($this->column, $column)->first();

        if (is_null($match)) return false;

        return password_verify($password, $match->password);
    }

    public function member()
    {
        if(!$this->session->has($this->identifier())) return null;

        $id = $this->session->get($this->identifier());

        return $this->model::get($id);
    }

    public function logout()
    {
        $this->session->regenerate();

        $this->session->delete($this->identifier());
        $this->member = null;
    }

    public function authenticated()
    {
        $this->session->regenerate();

        if(!$this->session->has($this->identifier())) return false;

        return !is_null($this->member());
    }    
}
