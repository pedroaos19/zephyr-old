<?php
namespace Zephyr\Auth;

use Zephyr\Http\Request\Request;
use Firebase\JWT\JWT;
use Zephyr\Auth\Model\Member;
use Zephyr\Http\Cookie;
use Zephyr\Http\Security\Csrf;

class Tokenship
{
    private static $instance;

    private function __construct()
    {
        $this->model = conf()->get('auth.model');
        $this->column = conf()->get('auth.column');

        if (in_array($this->column, ['id', 'password', 'created_at', 'updated_at'])) {
            die(trigger_error("Cannot use this column for a member registration."));
        }
    }

    public static function use()
    {
        if(self::$instance === null)
        {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function register($column, $password, $data = null)
    {
        $match = $this->model::query()->where($this->column, $column)->first();
        if (!is_null($match)) 
        {
            return [
                "result" => false,
                "member" => null,
                "message" => "Member already exists successfully"
            ];
        }

        $member = new $this->model;

        $colName = $this->column;
        $member->$colName = $column;
        $member->password = encrypt($password);

        if ($data) 
        {
            foreach ($data as $key => $value) 
            {
                $member->$key = $value;
            }
        }
        
        $member->create();
        return [
            "result" => true,
            "member" => $member,
            "message" => "Member registered successfully"
        ];
    }

    public function login($column, $password)
    {
        if ($this->validate($column, $password)) 
        {
            $member = $this->model::query()->where($this->column, $column)->first();
            $tokenData = $this->token($member);

            unset($member->password);

            return [
                "result" => true,
                "member" => $member,
                "jwt_token" => $tokenData["jwt_token"],
                "expiration" => $tokenData["expiration"],
                "csrf" => $tokenData["csrf"],
                "message" => "Login successful"
            ];
        }

        return [
            "result" => false,
            "member" => null,
            "message" => "Login failed"
        ];
    }

    protected function validate($column, $password)
    {
        $matches = $this->model::query()->where($this->column, $column);
        if (is_null($matches)) return false;
        
        $match = $matches->first();
        if (is_null($match)) return false;

        return password_verify($password, $match->password);
    }

    protected function token($member)
    {
        if (getenv('JWT_KEY')) 
        {
            $issuedAt = time();
            $csrf = Csrf::onlyToken();
            $token = array(
                "iat" => $issuedAt,
                "exp" => $issuedAt + 60 * 120,
                "data" => [
                    'memberId' => $member->id,
                    'memberEmail' => $member->email,
                    'csrf' => $this->hashToken($csrf)
                ],
            );

            $jwt = JWT::encode($token, getenv('JWT_KEY'));
            return [
                "jwt_token" => $jwt,
                "csrf" => $csrf,
                "expiration" => $issuedAt + 60 * 120, // 2 hours
            ];
        }
    }

    protected function hashToken($token)
    {
        return hash_hmac('sha256', getenv('APP_KEY'), $token);
    }

    public function member()
    {
        $csrf = Request::capture()->get('csrf');
        if(is_null($csrf)) 
        {
            return [
                "result" => false, 
                "message" => "CSRF token was not provided in the request"
            ];
        };
        
        $jwt = Cookie::value('jwt_token');
        try 
        {
            $jwtDecoded = JWT::decode($jwt, getenv('JWT_KEY'), array('HS256'));
            
            if (!hash_equals($this->hashToken($csrf), $jwtDecoded->data->csrf))
            {
                return [
                    "result" => false, 
                    "message" => "CSRF token doesn't match"
                ];
            }

            $member = Member::get($jwtDecoded->data->memberId);
            unset($member->password);

            return [
                "result" => true, 
                "member" => $member
            ];
        } 
        catch (\Exception $e)
        {
            return [
                "success" => false, 
                "message" => "JWT has expired"
            ];            
        }
    }
}