<?php
namespace Zephyr\Controller;

use Zephyr\Http\Request\Request;
use Zephyr\Http\Request\Validator;

class Controller
{
    public function validate(Request $request, $validation)
    {
        foreach (array_keys($validation) as $key) 
        {
            if (in_array($key, array_keys($request->all()))) 
            {
                $validator = new Validator($request->post($key), $validation[$key]);
                                    
                if (!$validator->isValid()) redirect($request->back)->send();
            } 
            else 
            {
                die(trigger_error($key . " doesn't exist in this request"));
            }
        }
    }
}
