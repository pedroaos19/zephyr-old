<?php
    namespace Zephyr\Filesystem;

    class Directory
    {
        private $path;
        private $fullPath;

        public function __construct(string $path)
        {
            $this->path = str_replace("/", DIRECTORY_SEPARATOR, $path);
            $this->fullPath = realpath($this->path);
        }

        public function newChild($name)
        {
            $newChildPath = $this->path . DIRECTORY_SEPARATOR . $name;

            if(
                is_string($name) &&
                !file_exists($newChildPath) &&
                $name != ""
            ) {
                mkdir($newChildPath);
            }

            return new Directory($newChildPath);
        }

        public function name()
        {
            $nameParts = explode(DIRECTORY_SEPARATOR, $this->path);
            return $nameParts[count($nameParts) - 1];
        }

        public function path()
        {
            return $this->path;
        }

        public function fullPath()
        {
            return $this->fullPath;
        }

        public function files($extension = null)
        {
            if(isset($this->files))
            {
                return $this->files;
            }

            $files = glob($this->path . DIRECTORY_SEPARATOR . "*.*");

            $this->files = [];
            foreach($files as $file)
            {
                $this->files[] = new File($file);
            }

            return $this->files;
        }

        public function fileNames()
        {
            if(!isset($this->files))
            {
                $this->files();
            }

            $fileNames = [];
            foreach($this->files as $file)
            {
                $fileNames[] = $file->name();
            }

            return $fileNames;
        }

        public function children()
        {
            if(isset($this->directories))
            {
                return $this->directories;
            }

            $files = glob($this->path . DIRECTORY_SEPARATOR . "*.*");
            $all = glob($this->path . DIRECTORY_SEPARATOR . "*");
            $children = array_diff($all, $files);

            $this->children = [];
            foreach($children as $child)
            {
                $this->children[] = new Directory($child);
            }

            return $this->children;
        }

        public function childrenNames()
        {
            if(!isset($this->children))
            {
                $this->children();
            }

            $childrenNames = [];
            foreach($this->children as $child)
            {
                $childrenNames[] = $child->name();
            }

            return $childrenNames;
        }

        public function parent()
        {
            if(isset($this->parent))
            {
                return $this->parent;
            }

            $this->parent = new Directory(pathinfo($this->path)['dirname']);
            return $this->parent;
        }

        public function siblings()
        {
            if(isset($this->siblings))
            {
                return $this->siblings;
            }

            $parent = $this->parent();
            $children = $parent->children();
            
            $siblings = [];
            foreach($children as $child)
            {
                if($child->name() != $this->name())
                {
                    $siblings[] = $child;
                }
            }

            $this->siblings = $siblings;
            return $this->siblings;
        }
    }