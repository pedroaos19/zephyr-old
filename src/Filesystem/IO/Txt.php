<?php
    namespace Zephyr\Filesystem\IO;

    use Zephyr\Filesystem\Filesystem;

    class Txt extends FileIO
    {
        public function __construct()
        {
            $this->content = "";
        }

        public function read($file, $lineBreak = false)
        {
            $this->file = $file;

            if(!Filesystem::exists($file))
            {
                return null;
            }

            $handle = fopen($file, 'r');
            if($handle)
            {
                while(($line = fgets($handle)) !== false) 
                {
                    if($lineBreak)
                    {
                        if(!is_array($this->content))
                        {
                            $this->content = [];
                        }
                        
                        $this->content[] = $this->trimLineBreak($line);
                    }
                    else
                    {
                        if(!is_string($this->content))
                        {
                            $this->content = "";
                        }

                        $this->content .= $this->trimLineBreak($line) . '\n';
                    }
                }

                fclose($handle);
            }

            return $this;
        }

        public function lines()
        {
            if(is_array($this->content))
            {
                return $this->content;
            }
            else if(is_string($this->content))
            {
                return explode('\n', $this->content);
            }
        }

        public function write($file, $content)
        {
            $this->content = $content;
            $lines = $this->lines();

            $handle = fopen($file, 'w');
            foreach($lines as $line)
            {
                fwrite($handle, $line . PHP_EOL);
            }
        }

        private function trimLineBreak($string)
        {
            return substr($string, 0, -1);
        }
    }