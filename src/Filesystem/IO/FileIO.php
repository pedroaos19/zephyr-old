<?php
    namespace Zephyr\Filesystem\IO;

    abstract class FileIO
    {
        protected $content;

        abstract function write($file, $content);
        abstract function read($file);
        
        public function content()
        {
            return $this->content;
        }
    }