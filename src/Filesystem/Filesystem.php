<?php
    namespace Zephyr\Filesystem;

    class Filesystem
    {
        public static function directory($path)
        {
            if(static::exists($path))
            {
                if(isset(pathinfo($path)['extension']))
                {
                    die(trigger_error("The path specified is a file, not a directory."));
                }

                return new Directory($path);
            }
        }

        public static function file($path)
        {
            if(static::exists($path))
            {
                if(!isset(pathinfo($path)['extension']))
                {
                    die(trigger_error("The path specified is a directory, not a file."));
                }

                return new File($path);
            }
        }

        public static function createFile($path, $contents = null)
        {
            if(!static::exists($path))
            {
                if($contents)
                {
                    file_put_contents($path, $contents);
                }
                else
                {
                    touch($path);
                }

            }

            return new File($path);
        }

        public static function createDirectory($path)
        {
            $correctedPath = str_replace("/", DIRECTORY_SEPARATOR, $path);
            if(!static::exists($correctedPath))
            {
                mkdir($correctedPath, 0777, true);
                return new Directory($path);
            }
        }

        public static function exists($path)
        {
            return file_exists(str_replace("/", DIRECTORY_SEPARATOR, $path));
        }
    }