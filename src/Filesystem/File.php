<?php
namespace Zephyr\Filesystem;

class File
{
    private $path;
    private $fullPath;

    public function __construct(string $path)
    {
        $this->path = str_replace("/", DIRECTORY_SEPARATOR, $path);
        $this->fullPath = realpath($this->path);
    }

    public function path()
    {
        return $this->path;
    }

    public function fullPath()
    {
        return $this->fullPath;
    }

    public function folder()
    {
        if (isset($this->folder)) return $this->folder;

        $this->folder = new Directory(pathinfo($this->path)['dirname']);
        return $this->folder;
    }

    public function siblings()
    {
        $folder = $this->folder();
        $folderFiles = $folder->files();

        $siblings = [];
        foreach ($folderFiles as $file) 
        {
            if ($file->name() != $this->name()) 
            {
                $siblings[] = $file;
            }
        }

        $this->siblings = $siblings;
        return $this->siblings;
    }

    public function extension()
    {
        if (isset($this->extension)) 
        {
            return $this->extension;
        }

        $this->extension = pathinfo($this->path)['extension'];
        return $this->extension;
    }

    public function name($withExtenstion = true)
    {
        return $withExtenstion ?
            basename($this->path) : pathinfo($this->path)['filename'];
    }

    public function size()
    {
        // Bytes
        if (isset($this->size)) 
        {
            return $this->size;
        }

        $this->size = filesize($this->path);
        return $this->size;
    }

    public function delete()
    {
        unlink($this->path);
    }

    public function rename($name)
    {
        rename($this->path(), $this->folder()->path() . DIRECTORY_SEPARATOR . $name);
    }

    public function copy(Directory $to)
    {
        if (!copy($this->path, $to->path() . DIRECTORY_SEPARATOR . $this->name())) 
        {
            die(trigger_error("Failed to copy the file! Check permissions or if the file exists!"));
        }
    }

    public function move(Directory $to)
    {
        if (!rename($this->path, $to->path() . DIRECTORY_SEPARATOR . $this->name())) {
            die(trigger_error("Failed to move the file! Check permissions or if the file exists!"));
        }
    }

    public function contents()
    {
        return file_get_contents($this->fullPath);
    }

    public function putContents($content)
    {
        return file_put_contents($this->fullPath, $content);
    }
}
