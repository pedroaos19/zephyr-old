<?php

namespace Zephyr\Utils;

class Str
{
    public static function contains($str, $substr)
    {
        return strpos($str, $substr) === false ? false : true;
    }
}