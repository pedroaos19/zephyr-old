<?php

namespace Zephyr\Utils;

class Arr
{
    public static function first($array)
    {
        return isset($array) && isset($array[0]) ? $array[0] : null;
    }

    public static function last($array)
    {
        $reverse = array_reverse($array);
        return static::first($reverse);
    }

    public static function sameLength($first, $second)
    {
        return count(array_diff($first, $second)) === count(array_diff($second, $first));
    }
}