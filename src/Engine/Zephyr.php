<?php

namespace Zephyr\Engine;

use Zephyr\Router\Router;
use Dotenv\Dotenv;
use Zephyr\Charon\Charon;

class Zephyr
{
    public function __construct()
    {
        $this->env(); 
        if(conf()["zephyr"]["charon"]) $this->charon();
    }

    public function listen($debug = false)
    {
        if (!$debug) Router::listen();
    }

    protected function env()
    {
        $dotenv = Dotenv::create(app_dir());
        $dotenv->load();
    }

    protected function charon()
    {
        $charon = new Charon;
    }
}
