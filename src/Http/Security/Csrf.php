<?php
namespace Zephyr\Http\Security;

use Zephyr\Http\Request\Request;
use Zephyr\Http\Session;

class Csrf
{
    private static $tokenHtml = "<input type=\"hidden\" name=\"_csrf\" value=\"@_token_@\">";

    public static function token()
    {
        $token = hash_hmac('sha256', bin2hex(random_bytes(32)), getenv('APP_KEY'));
        Session::capture()->set('_csrf', $token);

        return str_replace('@_token_@', $token, self::$tokenHtml);
    }

    public static function rawToken()
    {
        $token = hash_hmac('sha256', bin2hex(random_bytes(32)), getenv('APP_KEY'));
        Session::capture()->set('_csrf', $token);

        return $token;
    }

    public static function onlyToken()
    {
        return hash_hmac('sha256', bin2hex(random_bytes(32)), getenv('APP_KEY'));
    }

    public static function tokensMatch($request)
    {
        if(Session::capture()->has('_csrf') && array_key_exists('X-CSRF-TOKEN', $request->headers))
        {
            return hash_equals(Request::capture()->headers['X-CSRF-TOKEN'], Session::capture()->get('_csrf'));
        }
        else if (Session::capture()->has('_csrf')) 
        {
            return hash_equals($request->post('_csrf'), Session::capture()->get('_csrf'));
        }
        
        die(trigger_error("CSRF token not set!"));
    }

    public static function tokensMismatch($request)
    {
        return !(self::tokensMatch($request));
    }
}
