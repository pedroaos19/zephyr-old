<?php
namespace Zephyr\Http\Security\Middleware;

use Zephyr\Http\Request\Request;
use Closure;
use Zephyr\Http\Security\Csrf;

class VerifyCsrfToken
{
    public function run(Request $request, Closure $next)
    {
        if (Csrf::tokensMismatch($request)) 
        {
            die(trigger_error("CSRF Tokens mismatch!"));
        }

        return $next($request);
    }
}
