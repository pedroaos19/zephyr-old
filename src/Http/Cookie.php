<?php
namespace Zephyr\Http;

use Carbon\Carbon;

class Cookie
{
    const DEFAULT_DURATION = 3600;

    private function __construct($name, $value, $expiration = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expiration = $expiration;
    }

    public static function create($name, $value, $duration = null)
    {
        $timestamp = time() + (isset($duration) ? $duration : Cookie::DEFAULT_DURATION);
        $expiration = Carbon::createFromTimestamp($timestamp);

        return new Cookie($name, $value, $expiration);
    }

    public static function createAndSend($name, $value, $duration = null)
    {
        $cookie = static::create($name, $value, $duration);
        $cookie->send();

        return $cookie;
    }

    public static function all()
    {
        $cookies = [];

        foreach ($_COOKIE as $name => $value) 
        {
            $cookies[] = static::get($name);
        }

        return $cookies;
    }

    public static function get($name)
    {
        $result = null;

        if (isset($_COOKIE[$name])) 
        {
            $cookieValue = $_COOKIE[$name];
            $cookieExpiration = null;

            if (is_json($cookieValue)) 
            {
                $cookieDecoded = json_decode($cookieValue, true);

                $cookieValue = switchval($cookieDecoded["value"]);
                $cookieExpiration = $cookieDecoded["expiration"];
            }

            $result = new Cookie(
                $name, 
                $cookieValue, 
                Carbon::createFromTimestamp($cookieExpiration)
            );
        }

        return $result;
    }

    public static function value($name)
    {
        $result = null;

        if (isset($_COOKIE[$name])) 
        {
            $result = is_json($_COOKIE[$name]) ?
            json_decode($_COOKIE[$name], true)["value"] : $_COOKIE[$name];
        }

        return switchval($result);
    }

    public static function expiration($name)
    {
        $result = null;

        if (isset($_COOKIE[$name])) 
        {
            $result = is_json($_COOKIE[$name]) ?
            json_decode($_COOKIE[$name], true)["expiration"] : null;
        }

        return Carbon::createFromTimestamp($result);
    }

    public static function secondsToExpire($name)
    {
        return Carbon::now()->diffInSeconds(static::expiration($name));
    }

    public static function delete($name)
    {
        $cookie = static::get($name);

        if ($cookie) setcookie($cookie->name, "", -1, "/");
    }

    public function send()
    {
        $jsonValue = json_encode([
            "value" => $this->value,
            "expiration" => $this->expiration->timestamp
        ]);

        setcookie($this->name, $jsonValue, $this->expiration->timestamp, "/");
    }

    public function secondsToExpiration()
    {
        return Carbon::now()->diffInSeconds($this->expiration);
    }

    public function remove()
    {
        setcookie($this->name, "", -1, "/");
    }
}
