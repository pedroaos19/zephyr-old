<?php
namespace Zephyr\Http\Response;

use Carbon\Carbon;
use DateTimeZone;
use Zephyr\Http\Cookie;

class Response
{
    private $content;
    private $headers;
    private $status;
    private $cookies;

    public function __construct($content, $status = 200)
    {
        $this->content = $content;
        //$this->content = htmlspecialchars($content);
        $this->status = $status;
        $this->cookies = [];
        $this->headers = [];
    }

    public function send()
    {
        $this->sendCookies();
        $this->sendHeaders();
        $this->sendStatus();
        $this->sendContent();
    }

    private function sendContent()
    {
        if (isset($this->content)) echo $this->content;
    }

    public function header(string $key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    private function sendHeaders()
    {
        foreach ($this->headers as $key => $value) 
        {
            header($key . ":" . $value, true, $this->status);
        }
    }

    public function cookie(string $name, $value, int $expiresTimestamp = 0, bool $httpOnly = false)
    {
        // $this->cookies[] = Cookie::create($name, $value, $seconds);
        $this->cookies[$name] = ["value" => $value, "expiresTimestamp" => $expiresTimestamp, "httpOnly" => $httpOnly];
        return $this;
    }

    private function sendCookies()
    {
        $template = "Set-Cookie: %s=%s; Expires=%s; Path=/; %s";
        
        if(isset($this->cookies))
        {
            foreach ($this->cookies as $name => $cookie) 
            {
                $dt = new \DateTime("now", new \DateTimeZone('Europe/London'));
                $dt->setTimestamp($cookie["expiresTimestamp"]);
                $exp = $dt->format("D, d M Y H:i:s");

                header(
                    sprintf(
                        $template,
                        $name,
                        $cookie["value"],
                        $exp,
                        $cookie["httpOnly"] ? "httpOnly" : ""
                    )
                );

                // setcookie(
                //     $name, 
                //     $cookie["value"], 
                //     $cookie["expiresTimestamp"], 
                //     "/",
                //     "",
                //     false,
                //     $cookie["httpOnly"]
                // );                
            }
        }
    }

    public function withStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    private function sendStatus()
    {
        http_response_code($this->status);
    }
}
