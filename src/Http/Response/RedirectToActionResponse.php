<?php
    namespace Zephyr\Http\Response;

    use Zephyr\Router\Map;
    use Zephyr\Router\Route;

    /**
     * Class RedirectToActionResponse
     * @package Zephyr\Http\Response
     */
    class RedirectToActionResponse extends Response
    {
        public function __construct($controller, $action, $params = NULL)
        {
            $controllerName = $controller;
            if(is_object($controller))
            {
                $controllerNameArray = explode('\\', get_class($controller));
                $controllerName = $controllerNameArray[count($controllerNameArray) - 1];
            }

            $routes = Map::instance()->routes();

            $foundRoute = NULL;
            /** @var Route $route */
            foreach ($routes as $route)
            {
                if($route->controller == $controllerName && $route->action == $action)
                {
                    $foundRoute = $route;
                    break;
                }
            }

            if($foundRoute)
            {
                $redirectionUrl = [];

                $routeParts = explode("/", $route->uri);
                foreach ($routeParts as $routePart)
                {
                    $urlPart = $routePart;
                    if(strpos($routePart, ":") !== false)
                    {
                        $replacement = $params[str_replace(":", "", $routePart)];
                        $urlPart = $replacement;
                    }

                    $redirectionUrl[] = $urlPart;
                }

                $finalUrl = join("/", $redirectionUrl);

                $this->header("Cache-Control", "max-age=3600, public");
                $this->header("Location", $finalUrl);
            }
            else
            {
                die("No action for that redirection");
            }
        }
    }
