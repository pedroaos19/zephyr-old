<?php
    namespace Zephyr\Http\Response;

    /**
     * Class FileResponse
     * @package Zephyr\Http\Response
     */
    class FileResponse extends Response
    {
        public function __construct($content)
        {
            parent::__construct($content);

            $this->header("Cache-Control", "max-age=3600, public");
            $this->header("Content-Type", "text/html");
        }
    }
