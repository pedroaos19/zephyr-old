<?php
    namespace Zephyr\Http\Response;

    /**
     * Class RedirectResponse
     * @package Zephyr\Http\Response
     */
    class RedirectResponse extends Response
    {
        public function __construct($url)
        {
            $this->header("Cache-Control", "max-age=3600, public");
            $this->header("Location", $url);
        }
    }
