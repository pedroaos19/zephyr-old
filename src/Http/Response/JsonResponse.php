<?php

namespace Zephyr\Http\Response;

use Zephyr\Model\Collection;

class JsonResponse extends Response
{
    public function __construct($content)
    {
        $jsonContent = json_encode($content, JSON_UNESCAPED_UNICODE);
        parent::__construct($jsonContent);

        $this->header("Cache-Control", "max-age=3600, public");
        $this->header("Content-Type", "application/json");
    }
}
