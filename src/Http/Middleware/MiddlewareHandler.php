<?php
    namespace Zephyr\Http\Middleware;

    use Zephyr\Http\Request\Request;
    use Zephyr\Http\Response\Response;
    use Zephyr\Http\Request\RequestExecutor;
    use Zephyr\Utils\Arr;

    /**
     * Class Middleware
     * @package Zephyr\Http\Middleware
     */
    class MiddlewareHandler
    {
        public static function handle($route, $request)
        {
            // dd($stack);
            // $top = function($request) use ($response) {
            //     return $response;
            // };

            $top = function($request) use ($route) {
                return RequestExecutor::execute($route, $request);
            };

            $middlewares = [$top];
            foreach($route->middleware as $middleware)
            {
                $middlewareInst = new $middleware;
                
                if(isset($middlewareInst->except) &&
                    in_array($request->path, $middlewareInst->except))
                    continue;
                
                $next = Arr::last($middlewares);
                $middlewares[] = function(Request $request) use ($middlewareInst, $next) {
                    $result = $middlewareInst->run($request, $next);
                    
                    if(!is_object($result) || (
                        get_class($result) != \Zephyr\Http\Response\Response::class &&
                        get_class($result) != \Zephyr\Http\Response\ViewResponse::class &&
                        get_class($result) != \Zephyr\Http\Response\JsonResponse::class &&
                        get_class($result) != \Zephyr\Http\Response\FileResponse::class &&
                        get_class($result) != \Zephyr\Http\Response\DownloadResponse::class &&
                        get_class($result) != \Zephyr\Http\Response\RedirectResponse::class &&
                        get_class($result) != \Zephyr\Http\Response\RedirectToActionResponse::class)
                    ) {
                        die(trigger_error("The Middleware " . get_class($middlewareInst) . " doesn't return a Response"));
                    }
                    
                    return $result;
                };
            }
            
            return $middlewares[count($middlewares) - 1]($request);
        }
    }
