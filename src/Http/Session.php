<?php
namespace Zephyr\Http;

class Session
{
    private function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) session_start();

        $this->id = session_id();
        $this->storage = $_SESSION;
    }

    public static function capture()
    {
        return new Session;
    }

    public function regenerate()
    {
        session_regenerate_id();
        return $this;
    }

    public function destroy()
    {
        unset($this->storage);
        session_destroy();
        session_unset();
    }

    public function get($key)
    {
        return isset($this->storage[$key]) ? $this->storage[$key] : null;
    }

    public function has($key)
    {
        return isset($this->storage[$key]);
    }

    public function set($key, $value)
    {
        $this->storage[$key] = $_SESSION[$key] = $value; 
        return $this;
    }

    public function flash($key, $content = null)
    {
        if (!isset($this->storage['_flash'])) 
        {
            $this->storage['_flash'] = $_SESSION['_flash'] = [];
        }

        if (isset($content)) 
        {
            if (!isset($this->storage['_flash'][$key])) 
            {
                $this->storage['_flash'][$key] = $_SESSION['_flash'][$key] = [];
            }

            if($key === "form_errors")
            {
                $this->storage['_flash'][$key][] = $_SESSION['_flash'][$key][] = $content;
            }
            else
            {
                $this->storage['_flash'][$key] = $_SESSION['_flash'][$key] = $content;
            }
            return $this;
        }

        return isset($this->storage['_flash'][$key]) ? $this->storage['_flash'][$key] : null;
    }

    public function flushFlash()
    {
        if (isset($this->storage['_flash'])) 
        {
            $this->storage['_flash'] = $_SESSION['_flash'] = [];
        }
    }

    public function delete($key)
    {
        if (isset($this->storage[$key])) 
        {
            unset($this->storage[$key]);
            unset($_SESSION[$key]);
        }
    }
}
