<?php
namespace Zephyr\Http\Request;

use Carbon\Carbon;

class Collector
{
    public static function collect(array $post)
    {
        $data = [];
        foreach($post as $key => $entry) 
        {
            $dataEntry = null;
            if(is_array($entry)) 
            {
                $dataEntry = [];
                foreach($entry as $subEntry) 
                {
                    $dataEntry[] = static::resolve($subEntry);
                }
            }
            else
            {
                $dataEntry = static::resolve($entry);
            }

            $data[$key] = $dataEntry;
        }

        return $data;
    }

    public static function files($files)
    {
        $data = [];
        foreach($files as $key => $file)
        {
            if($file["name"] === '')
            {
                $data[$key] = null;
            }
            else
            {
                $data[$key] = $file;
            }
            
        }

        return $data;
    }

    protected static function resolve($entry) 
    {
        if (\DateTime::createFromFormat('Y-m-d', $entry) !== false) {
            return Carbon::createFromFormat('Y-m-d', $entry);
        }

        if (\DateTime::createFromFormat('Y-m-d H:i:s', $entry) !== false) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $entry);
        }

        if (\DateTime::createFromFormat('H:i:s', $entry) !== false) {
            return Carbon::createFromFormat('H:i:s', $entry);
        } else if ($entry === "1_on") {
            return true;
        } else if ($entry === "0_off") {
            return false;
        } else if (ctype_digit($entry)) {
            return (int) $entry;
        } else if (is_numeric($entry)) {
            return (float) $entry;
        } else if (is_string($entry) && $entry === '') {
            return null;
        } else {
            return $entry;
        }
    }
}
