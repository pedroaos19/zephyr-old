<?php

namespace Zephyr\Http\Request;

use Zephyr\Router\Route;

class RequestExecutor
{
    public static function execute(Route $route, Request $request)
    {
        $params = static::params($request, $route);

        $controller = isset($route->namespace) ? 
            $route->namespace . "\\" . $route->controller :
            "App\\Controller\\" . $route->controller;
        
        $method = new \ReflectionMethod($controller, $route->action);

        $requestPosition = static::requestParamPosition($method);
        if(isset($requestPosition))
        {
            array_splice($params, $requestPosition, 0, [$request]);
        }

        return call_user_func_array([new $controller, $route->action], $params);
    }

    private static function params(Request $request, Route $route)
    {
        $params = [];

        if(!empty($route->params))
        {
            $paramIndexes = array_keys($route->params);
            foreach($paramIndexes as $paramIndex)
            {
                $params[] = $request->segments[$paramIndex];
            }
        }

        return $params;
    }

    private static function requestParamPosition($method)
    {
        $requestPosition = null;

        for ($i = 0; $i < count($method->getParameters()); $i++) 
        {
            if (
                strpos($method->getParameters()[$i]->getClass(), "Request")
                && $method->getParameters()[$i]->getName() === "request"
            ) {
                $requestPosition = $method->getParameters()[$i]->getPosition();
                break;
            }
        }

        return $requestPosition;
    }
}