<?php
namespace Zephyr\Http\Request;

class Uploader
{
	const DESTINATION = "public" . DIRECTORY_SEPARATOR . "files";

	public static function save($key, $subFolder = null)
	{
		$request = Request::capture();
		if(is_null($request->files[$key])) return null;
		
		$name = $_FILES[$key]['name'];
		$data = $_FILES[$key]['tmp_name'];
		$errorCode = ($_FILES[$key]['error']);
		$extension = pathinfo($name, PATHINFO_EXTENSION);

		$subFolderPath = "";
		if (isset($subFolder)) 
		{
			if (!file_exists(Uploader::DESTINATION . DIRECTORY_SEPARATOR . $subFolder)) 
			{
				mkdir(Uploader::DESTINATION . DIRECTORY_SEPARATOR . $subFolder);
			}

			$subFolderPath = $subFolder;
		}

		$fileDestination = Uploader::DESTINATION . DIRECTORY_SEPARATOR .
			$subFolderPath . DIRECTORY_SEPARATOR .
			static::randomName() . "." . $extension;

		if (static::canUpload($errorCode)) 
		{
			move_uploaded_file($data, $fileDestination);
		} 
		else 
		{
			throw new Exception("Error uploading file. Check if folder is writable or the file's size.");
		}

		return ltrim($fileDestination, "public" . DIRECTORY_SEPARATOR);
	}

	private static function canUpload($errorCode)
	{
		$writable = is_writable(Uploader::DESTINATION);

		if ($writable === false) 
		{
			$canUpload = false;	
		} 
		else if ($errorCode === 1) 
		{
			$canUpload = false;
		} 
		else if ($errorCode > 1) 
		{
			$canUpload = false;
		} 
		else 
		{
			$canUpload = true;
		}

		return $canUpload;
	}

	private static function randomName()
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < 32; $i++) 
		{
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}
}
