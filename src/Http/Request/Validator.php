<?php
namespace Zephyr\Http\Request;

class Validator
{
    private $conditions = [
        "required", "max", "min", "eq",
        "email", "len-max", "len-min", "len-eq"
    ];

    private $condition;
    private $value;

    public function __construct($value, string $condition, string $errorMessage = null)
    {
        $this->value = $value;
        $this->condition = $condition;
    }

    private function conditionArray()
    {
        $conditionArray = explode("|", $this->condition);

        for ($i = 0; $i < count($conditionArray); $i++) 
        {
            if (strpos($conditionArray[$i], ":") !== false) 
            {
                $subCondition = explode(":", $conditionArray[$i]);

                if (in_array($subCondition[0], $this->conditions)) 
                {
                    $subCondition[1] = intval($subCondition[1]);
                    $conditionArray[$i] = $subCondition;
                } 
                else
                {
                    die(trigger_error("The condition " . $subCondition[0] . " doesn't exist"));
                }
            } 
            else if (!in_array($conditionArray[$i], $this->conditions)) 
            {
                die(trigger_error("The condition " . $conditionArray[$i] . " doesn't exist"));
            }
        }

        return $conditionArray;
    }

    public function isValid()
    {
        $conditionArray = $this->conditionArray();

        $isValid = true;
        foreach ($conditionArray as $condition) 
        {
            if (!$this->passes($condition, $this->value)) 
            {
                $isValid = false;
                break;
            }
        }

        return $isValid;
    }

    private function passes($condition, $value)
    {
        if (is_array($condition)) 
        {
            return $this->resolve($condition[0], $value, $condition[1]);
        } 
        else if (is_string($condition)) 
        {
            return $this->resolve($condition, $value);
        }
    }

    private function resolve($condition, $value, $subCondition = NULL)
    {
        $solved = true;

        switch ($condition) 
        {
            case "required":
                $solved = $this->required($value);
                break;
            case "max":
                $solved = $this->max($value, $subCondition);
                break;
            case "min":
                $solved = $this->min($value, $subCondition);
                break;
            case "eq":
                $solved = $this->eq($value, $subCondition);
                break;
            case "email":
                $solved = $this->email($value, $subCondition);
                break;
            case "len-max":
                $solved = $this->lenMax($value, $subCondition);
                break;
            case "len-min":
                $solved = $this->lenMin($value, $subCondition);
                break;
            case "len-min":
                $solved = $this->lenEq($value, $subCondition);
                break;
        }

        return $solved;
    }

    private function required($value)
    {
        return $value !== NULL || !empty($value) ? true : false;
    }

    private function max($value, $condition)
    {
        return $value <= $condition ? true : false;
    }

    private function min($value, $condition)
    {
        return $value >= $condition ? true : false;
    }

    private function eq($value, $condition)
    {
        return $value === $condition ? true : false;
    }

    private function email($value)
    { }

    private function lenMax($value, $condition)
    {
        return strlen($value) <= $condition ? true : false;
    }

    private function lenMin($value, $condition)
    {
        return strlen($value) >= $condition ? true : false;
    }

    private function lenEq($value, $condition)
    {
        return strlen($value) === $condition ? true : false;
    }
}
