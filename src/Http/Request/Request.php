<?php
namespace Zephyr\Http\Request;

use Zephyr\Utils\Arr;
use Zephyr\Http\Session;

/**
 * Class Request
 * @package Zephyr\Http\Request
 */
class Request
{
    private function __construct()
    { }

    public static function capture()
    {
        $request = new Request;

        $request->method = static::getMethod();
        $request->url = static::getUrl();
        $request->fullUrl = static::getFullUrl();
        $request->path = static::getPath();
        $request->query = static::getQuery();
        $request->referer = static::getReferer();
        $request->data = static::getData();
        $request->files = static::getFiles();
        $request->headers = static::getHeaders();
        $request->cookies = static::getCookies();
        $request->segments = static::getSegments();
        $request->session = static::getSession();
        $request->back = static::getBack();

        return $request;
    }

    private static function getSegments()
    {
        $segments = explode("/", static::getPath());
        
        if (Arr::first($segments) === "") 
        {
            array_splice($segments, 0, 1);
        }

        if (Arr::last($segments) === "") 
        {
            array_splice($segments, -1, 1);
        }

        return $segments;
    }

    private static function getMethod()
    {
        return $_SERVER["REQUEST_METHOD"];
    }

    private static function getUrl()
    {
        return base_url() . static::getPath();
    }

    private static function getFullUrl()
    {
        return base_url() . $_SERVER["REQUEST_URI"];
    }

    private static function getPath()
    {
        return strtok($_SERVER["REQUEST_URI"], '?');
    }

    private static function getQuery()
    {
        $query = null;

        if (isset($_SERVER["QUERY_STRING"])) 
        {
            parse_str($_SERVER["QUERY_STRING"], $query);
        }

        return $query;
    }

    private static function getReferer()
    {
        return null;
    }

    private static function getData()
    {
        $data = null;
        if(!empty($_POST)) 
        {
            $data = Collector::collect($_POST);;
        }
        else
        {
            $content = trim(file_get_contents("php://input"));
            $data = json_decode($content, true);
        }

        return $data;
    }

    private static function getFiles()
    {
        return empty($_FILES) ? null : Collector::files($_FILES);
    }

    private static function getHeaders()
    {
        $headers = [];

        foreach (\getallheaders() as $key => $value) 
        {
            $headers[$key] = $value;
        }

        return $headers;
    }

    private static function getCookies()
    {
        $cookies = [];

        foreach ($_COOKIE as $name => $value) 
        {
            $cookies[$name] = $value;
        }

        return $cookies;
    }

    private static function getSession()
    {
        return Session::capture();
    }

    private static function getBack()
    {
        return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
    }

    public function all()
    {
        return $this->data;
    }

    public function get($key)
    {
        return isset($this->query) && isset($this->query[$key]) ? $this->query[$key] : null;
    }

    public function post($key)
    {
        return isset($this->data) && isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function has($key, $get = false)
    {
        if($get) isset($this->query) && isset($this->query[$key]);

        return isset($this->data) && isset($this->data[$key]);
    }

    public function header($key)
    {
        return isset($this->headers[$key]) ? $this->headers[$key] : null;
    }

    public function cookie($name)
    {
        return isset($this->cookie[$name]) ? $this->cookie[$name] : null;
    }

    public function validate(array $validation)
    {
        foreach (array_keys($validation) as $key) 
        {
            if (in_array($key, array_keys($this->all()))) 
            {
                $validator = new Validator($this->post($key), $validation[$key]);

                if (!$validator->isValid()) redirect($this->back)->send();
            } 
            else 
            {
                die(trigger_error($key . " doesn't exist in this request"));
            }
        }
    }

    // ********************

    public function save($name, $subFolder = null)
    {
        if (in_array($name, array_keys($this->files))) 
        {
            return Uploader::save($name, $subFolder);
        } 
        else 
        {
            die(trigger_error("Key doesn't exist in this request"));
        }
    }
}
