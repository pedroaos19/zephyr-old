<?php

namespace Zephyr\Database;

use Dotenv\Dotenv;

class SqlDialect
{
    const CREATE_TABLE = "CREATE TABLE %s (%s) DEFAULT CHARACTER SET=utf8mb4;";
    const DROP_TABLE = "DROP TABLE %s";

    //const PRIMARY_KEY_COL = "%s BIGINT %s PRIMARY KEY AUTO_INCREMENT";
    //const FOREIGN_KEY_COL = "%s BIGINT %s, CONSTRAINT %s_%s_foreign FOREIGN KEY (`%s`) REFERENCES `%s` (`%s`)";

    const BELONGS_TO = "SELECT `TABLE_NAME`, `COLUMN_NAME`, `REFERENCED_TABLE_NAME`, `REFERENCED_COLUMN_NAME`
                        FROM `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`
                        WHERE `TABLE_SCHEMA` = SCHEMA() AND `REFERENCED_TABLE_NAME` IS NOT NULL AND `TABLE_NAME`='%s'";
    const HAS_MANY = "SELECT column_name as 'fk', table_name AS 'table'
                      FROM information_schema.key_column_usage
                      WHERE `TABLE_SCHEMA` = SCHEMA() AND REFERENCED_TABLE_NAME = '%s'";

    const COLUMN = "%s %s %s %s %s %s";

    const ALTER_TABLE = "ALTER TABLE %s"; 
    const ADD_COLUMN = "ADD COLUMN %s";
    const MODIFY_COLUMN = "MODIFY COLUMN %s";
    const RENAME_COLUMN = "CHANGE `%s` `%s` %s";
    const DROP_COLUMN = "DROP COLUMN %s";

    const CONSTRAINT_PK = "CONSTRAINT %s_%s_primary PRIMARY KEY (`%s`)";
    const CONSTRAINT_PK_NAME = "SELECT * 
                                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                                WHERE CONSTRAINT_NAME LIKE '%%%s%%' 
                                AND CONSTRAINT_NAME LIKE '%%primary%%' 
                                AND COLUMN_NAME = '%s'";
    const ALTER_ADD_CONSTRAINT_PK = "ADD CONSTRAINT %s_%s_primary PRIMARY KEY (`%s`)";
    const ALTER_DROP_CONSTRAINT_PK = "DROP PRIMARY KEY %s";

    const CONSTRAINT_FK = "CONSTRAINT %s_%s_foreign FOREIGN KEY (`%s`) REFERENCES `%s` (`%s`)";
    const CONSTRAINT_FK_NAME = "SELECT * 
                                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                                WHERE CONSTRAINT_NAME LIKE '%%%s%%' 
                                AND CONSTRAINT_NAME LIKE '%%foreign%%' 
                                AND COLUMN_NAME = '%s' 
                                AND TABLE_SCHEMA = '%s'";
    const ALTER_ADD_CONSTRAINT_FK = "ADD CONSTRAINT %s_%s_foreign FOREIGN KEY (`%s`) REFERENCES `%s` (`%s`)";
    const ALTER_DROP_CONSTRAINT_FK = "DROP FOREIGN KEY %s";
    
    const ALTER_SET_CONSTRAINT_DEFAULT = "ALTER `%s` SET DEFAULT %s";
    const ALTER_DROP_CONSTRAINT_DEFAULT = "ALTER `%s` DROP DEFAULT";
    
    const CONSTRAINT_UNIQUE = "CONSTRAINT %s_%s_unique UNIQUE (`%s`)";
    const CONSTRAINT_UNIQUE_NAME = "SELECT * 
                                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                                WHERE CONSTRAINT_NAME LIKE '%%%s%%' 
                                AND CONSTRAINT_NAME LIKE '%%unique%%' 
                                AND COLUMN_NAME = '%s'";
    const ALTER_ADD_CONSTRAINT_UNIQUE= "ADD CONSTRAINT %s_%s_unique UNIQUE (`%s`)";
    const ALTER_DROP_CONSTRAINT_UNIQUE= "DROP INDEX %s";

    const NULLABLE = "DEFAULT NULL";
    const NOT_NULL = "NOT NULL";
    const AFTER = "AFTER `%s`";
    const DEFAULT = "DEFAULT %s";
    const AUTO_INCREMENT = "AUTO_INCREMENT";

    const ALL_COLUMNS = "SELECT * 
                        FROM information_schema.columns
    					WHERE table_schema='%s' AND TABLE_NAME = '%s'";
    const ALL_TABLES = "SELECT table_name 
                        FROM information_schema.tables 
                        WHERE table_schema='%s';";

    const SELECT = "SELECT %s FROM %s %s %s %s";
    const WHERE = "WHERE %s";
    const WHERE_AND = " AND ";
    const WHERE_OR = " OR ";
    const LIMIT = "LIMIT %s";
    const LIMIT_MAX = "18446744073709551615";
    const OFFSET = "OFFSET %s";

    const COUNT = "SELECT COUNT(*) AS count FROM %s";

    const INSERT = "INSERT INTO `%s` (%s) VALUES (%s)";
    const UPDATE = "UPDATE `%s` SET %s %s";
    const DELETE = "DELETE FROM %s %s";

    const COL_BIT = "BIT";
    const COL_INT = "INT";
    const COL_TINYINT = "TINYINT";
    const COL_SMALLINT = "SMALLINT";
    const COL_MEDIUMINT = "MEDIUMINT";
    const COL_BIGINT = "BIGINT";
    const COL_FLOAT = "FLOAT(%s,%s)";
    const COL_DOUBLE = "DOUBLE(%s,%s)";
    const COL_DATE = "DATE";
    const COL_DATETIME = "DATETIME";
    const COL_TIMESTAMP = "TIMESTAMP";
    const COL_TIME = "TIME";
    const COL_CHAR = "CHAR(%s)";
    const COL_VARCHAR = "VARCHAR(%s)";
    const COL_FILE = "VARCHAR(510)";
    const COL_IMAGE = "VARCHAR(512)";
    const COL_TEXT = "TEXT";
    const COL_TEXT_CHARSET = "CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci";
    const COL_TINYTEXT = "TINYTEXT";
    const COL_MEDIUMTEXT = "MEDIUMTEXT";
    const COL_LONGTEXT = "LONGTEXT";

    public static function createTable($name, $columns)
    {
        return tprintf(
            SqlDialect::CREATE_TABLE,
            $name,
            $columns
        );
    }

    public static function dropTable($table)
    {
        return tprintf(
            SqlDialect::DROP_TABLE,
            $table
        );
    }

    public static function primaryKeyColumn($name, $nullable = false)
    {
        return tprintf(
            SqlDialect::PRIMARY_KEY_COL,
            $name,
            $nullable ? SqlDialect::NULLABLE : SqlDialect::NOT_NULL
        );
    }

    public static function foreignKeyColumn($name, $table, $foreignTable, $nullable = false, $foreignId = null)
    {
        return tprintf(
            SqlDialect::FOREIGN_KEY_COL,
            $name,
            $nullable ? SqlDialect::NULLABLE : SqlDialect::NOT_NULL,
            $table,
            $name,
            $name,
            $foreignTable,
            isset($foreignId) ? $foreignId : "id"
        );
    }

    public static function column(
        $name, 
        $type, 
        $nullable = false,
        $default = null, 
        $autoIncrement = false,
        $after = null
    ) {
        $colNullable = "";
        $colDefault = "";

        if($nullable && isset($default)) {
            $colDefault = tprintf(SqlDialect::DEFAULT, _sqltype($default));
        } else if($nullable && !isset($default)) {
            $colNullable = SqlDialect::NULLABLE;
        } else if(!$nullable && isset($default)) {
            $colNullable = SqlDialect::NOT_NULL;
            $colDefault = tprintf(SqlDialect::DEFAULT, _sqltype($default));
        } else if(!$nullable && !isset($default)) {
            $colNullable = SqlDialect::NOT_NULL;
        }
        
        return tprintf(
            SqlDialect::COLUMN,
            $name,
            $type,
            $colNullable,
            $colDefault,
            $autoIncrement ? SqlDialect::AUTO_INCREMENT : " ", 
            isset($after) ? sprintf(SqlDialect::AFTER, $after) : " "
        );
    }

    public static function addColumns($table, Schema $schema)
    {
        $columns = join(", ", array_map(function($column) use ($table) {
            return tprintf(SqlDialect::ADD_COLUMN, $column->sql($table));
        }, $schema->columns()));

        return tprintf(
            "%s %s",
            tprintf(SqlDialect::ALTER_TABLE, $table),
            $columns
        );
    }

    public static function modifyColumns($table, Schema $oldSchema, Schema $schema)
    {
        $columns = join(", ", array_map(function ($column) use ($table, $oldSchema) {
            $oldColumn = $oldSchema->column($column->name);

            if($oldColumn)
            {
                $sql = Column::compareToSql($table, $oldColumn, $column);
                return tprintf(SqlDialect::MODIFY_COLUMN, $sql);
            }
        }, $schema->columns()));

        return tprintf(
            "%s %s",
            tprintf(SqlDialect::ALTER_TABLE, $table),
            $columns
        );
    }

    public static function renameColumn($from, $to, Column $column)
    {
        $columnSql = SqlDialect::column(
            null,
            $column->type,
            $column->nullable,
            $column->default,
            $column->autoIncrement,
            $column->after
        );

        return tprintf(
            SqlDialect::RENAME_COLUMN,
            $from,
            $to,
            $columnSql
        );
    }

    public static function dropColumn($name)
    {
        return tprintf(
            SqlDialect::DROP_COLUMN,
            $name
        );
    }

    public static function belongsTo($name)
    {
        return tprintf(
            SqlDialect::BELONGS_TO,
            $name
        );
    }

    public static function hasMany($name)
    {
        return tprintf(
            SqlDialect::HAS_MANY,
            $name
        );
    }

    public static function allColumns($table)
    {
        return tprintf(
            SqlDialect::ALL_COLUMNS,
            getenv('DB_NAME'),
            $table
        );
    }

    public static function allTables()
    {
        return tprintf(
            SqlDialect::ALL_TABLES,
            getenv('DB_NAME')
        );
    }

    public static function select(
        $table, 
        array $columns = null, 
        array $whereColumns = null, 
        array $whereConditions = null, 
        $limit = null, 
        $offset = null
    ) {
        $selection = isset($columns) ? join(", ", $columns) : '*';

        if(isset($whereColumns) && isset($whereConditions))
        {
            $conditions = array_map(function ($column, $condition) {
                return $column . $condition . "?";
            }, $whereColumns, $whereConditions);

            $where = tprintf(
                SqlDialect::WHERE,
                join(SqlDialect::WHERE_AND, $conditions)
            );
        }
        
        return tprintf(
            SqlDialect::SELECT,
            $selection,
            $table,
            isset($where) ? $where : "",
            isset($limit) ? tprintf(SqlDialect::LIMIT, $limit) : "",
            isset($offset) ? tprintf(SqlDialect::OFFSET, $offset) : ""
        );
    }

    public static function count($table)
    {
        return tprintf(
            SqlDialect::COUNT,
            $table
        );
    }

    public static function insert($table, $columns)
    {
        $insertion = join(", ", $columns);
        $values = join(", ", array_pad([], count($columns), "?"));

        return tprintf(
            SqlDialect::INSERT,
            $table,
            $insertion,
            $values
        );
    }

    public static function update($table, $columns, $whereColumns = null, $whereConditions = null)
    {
        $update = join(", ", array_map(function($column) {
            return "`$column` = ?";
        }, $columns));

        $conditions = "WHERE `id` = ?";
        if(isset($whereColumns) && isset($whereConditions))
        {
            $conditions = join(SqlDialect::WHERE_AND, array_map(function ($column, $condition) {
                return tprintf(SqlDialect::WHERE, $column . $condition . "?");
            }, $whereColumns, $whereConditions), $conditions);
        }
        
        return tprintf(
            SqlDialect::UPDATE,
            $table,
            $update,
            $conditions
        );
    }

    public static function delete($table, $whereColumns = null, $whereConditions = null)
    {
        $conditions = "`id` = ?";
        if (isset($whereColumns) && isset($whereConditions)) 
        {
            $conditions = join(SqlDialect::WHERE_AND, array_map(function ($column, $condition) {
                return $column . $condition . "?";
            }, $whereColumns, $whereConditions));
        }

        return tprintf(
            SqlDialect::DELETE,
            $table,
            tprintf(SqlDialect::WHERE, $conditions)
        );
    }

    public static function constraintPk($table, $column)
    {
        return tprintf(
            SqlDialect::CONSTRAINT_PK,
            $table,
            $column,
            $column
        );
    }

    public static function constraintPkName($table, $column)
    {
        return tprintf(
            SqlDialect::CONSTRAINT_PK_NAME,
            $table,
            $column
        );
    }

    public static function addConstraintPk($table, $column)
    {
        return tprintf(
            SQLDialect::ALTER_ADD_CONSTRAINT_PK,
            $table,
            $column,
            $column
        );
    }

    public static function dropConstraintPk($constraint)
    {
        return tprintf(
            SQLDialect::ALTER_DROP_CONSTRAINT_PK,
            $constraint
        );
    }

    public static function constraintFk($table, $column, $foreignTable, $foreignColumn = null)
    {
        return tprintf(
            SqlDialect::CONSTRAINT_FK,
            $table,
            $column,
            $column,
            $foreignTable,
            isset($foreignColumn) ? $foreignColumn : "id"
        );
    }

    public static function constraintFkName($table, $column)
    {
        return tprintf(
            SqlDialect::CONSTRAINT_FK_NAME,
            $table,
            $column,
            getenv('DB_NAME')
        );
    }

    public static function addConstraintFk($table, $column, $foreignTable, $foreignColumn = null)
    {
        return tprintf(
            SqlDialect::ALTER_ADD_CONSTRAINT_FK,
            $table,
            $column,
            $column,
            $foreignTable,
            isset($foreignColumn) ? $foreignColumn : "id"
        );
    }

    public static function dropConstraintFk($constraint)
    {
        return tprintf(
            SqlDialect::ALTER_DROP_CONSTRAINT_FK,
            $constraint
        );
    }

    public static function setConstraintDefault($column, $value)
    {
        return tprintf(
            SqlDialect::ALTER_SET_CONSTRAINT_DEFAULT,
            $column,
            _sqltype($value)
        );
    }

    public static function dropConstraintDefault($column)
    {
        return tprintf(
            SqlDialect::ALTER_DROP_CONSTRAINT_DEFAULT,
            $column
        ); 
    }

    public static function constraintUnique($table, $column)
    {
        return tprintf(
            SqlDialect::CONSTRAINT_UNIQUE,
            $table,
            $column,
            $column
        );
    }

    public static function constraintUniqueName($table, $column)
    {
        return Connection::get()->execute(tprintf(
            SqlDialect::CONSTRAINT_UNIQUE_NAME,
            $table,
            $column
        ))["CONSTRAINT_NAME"];

        return tprintf(
            SqlDialect::CONSTRAINT_UNIQUE_NAME,
            $table,
            $column
        );
    }

    public static function addConstraintUnique($table, $column)
    {
        return tprintf(
            SQLDialect::ALTER_ADD_CONSTRAINT_UNIQUE,
            $table,
            $column,
            $column
        );
    }

    public static function dropConstraintUnique($constraint)
    {
        return tprintf(
            SQLDialect::ALTER_DROP_CONSTRAINT_UNIQUE,
            $constraint
        );
    }
}