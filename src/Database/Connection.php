<?php
namespace Zephyr\Database;

use PDO;
use PDOException;
use PDOStatement;
use Exception;
use Zephyr\Utils\Str;

final class Connection
{
    const AS_OBJECT = PDO::FETCH_OBJ;
    const AS_ARRAY = PDO::FETCH_ASSOC;
    // const MYSQL = "mysql:host={1}:{2};dbname={3};charset=utf8mb4";
    const MYSQL = "mysql:host={1}:{2};dbname={3}";
    private $pdo;

    private function __construct()
    {
        $this->setPdo($this->getConnectionString());
    }

    private function getConnectionString()
    {
        $placeholders = ["{1}", "{2}", "{3}"];

        $connection = str_replace($placeholders, [getenv('DB_HOST'), getenv('DB_PORT'), getenv('DB_NAME')], Connection::MYSQL);
        return $connection;
    }

    private function setPdo(string $connection)
    {
        try 
        {
            $this->pdo = new PDO($connection, getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
        } 
        catch (PDOException $e) 
        {
            trigger_error($e->getMessage());
        }
    }

    public static function get()
    {
        static $instance = null;

        if ($instance == null) 
        {
            $instance = new Connection();
        }

        return $instance;
    }

    public function execute(string $sql, array $data = null)
    {
        $sqlStatement = $this->pdo->prepare($sql);
        
        if (isset($data)) 
        {
            for ($i = 0; $i < count($data); $i++) 
            {
                switch (gettype($data[$i])) 
                {
                    case "integer":
                        $sqlStatement->bindParam($i + 1, $data[$i], PDO::PARAM_INT);
                        break;
                    case "string":
                        $sqlStatement->bindParam($i + 1, $data[$i], PDO::PARAM_STR);
                        break;
                    case "boolean";
                        $sqlStatement->bindParam($i + 1, $data[$i], PDO::PARAM_BOOL);
                        break;
                    case "object":
                        if ($data[$i] instanceof \Carbon\Carbon) 
                        {
                            $dateParam = $data[$i]->format("Y-m-d H:i:s");
                            $sqlStatement->bindParam($i + 1, $dateParam, PDO::PARAM_STR);
                        }
                        break;
                    default:
                        $sqlStatement->bindParam($i + 1, $data[$i], PDO::PARAM_STR);
                        break;
                }
            }
        }

        $sqlStatement->execute();
        return $this->getReturn($sqlStatement);
    }

    private function getReturn(PDOStatement $statement)
    {
        if (
            Str::contains($statement->queryString, "UPDATE")
            || Str::contains($statement->queryString, "DELETE")
            || Str::contains($statement->queryString, "ALTER TABLE")
            || Str::contains($statement->queryString, "DROP TABLE")
            || Str::contains($statement->queryString, "CREATE TABLE")
        ) {
            return true;
        } 
        else if (Str::contains($statement->queryString, "INSERT INTO")) 
        {
            return (int)$this->pdo->lastInsertId();
        }

        if ($statement->rowCount() > 1) 
        {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } 
        else if ($statement->rowCount() === 1) 
        {
            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function disableForeignKeyChecks()
    {
        $sql = "SET FOREIGN_KEY_CHECKS=0;";
        $sqlStatement = $this->pdo->prepare($sql);

        try 
        {
            $sqlStatement->execute();
        } 
        catch (Exception $e) 
        {
            die("<p>You tried to run this sql: $sql</p><p>Exception: $e</p>");
        }
    }

    public function enableForeignKeyChecks()
    {
        $sql = "SET FOREIGN_KEY_CHECKS=1;";
        $sqlStatement = $this->pdo->prepare($sql);

        try {
            $sqlStatement->execute();
        } catch (Exception $e) {
            die("<p>You tried to run this sql: $sql</p><p>Exception: $e</p>");
        }
    }
}
