<?php

namespace Zephyr\Database\Seed;

use Zephyr\Filesystem\Filesystem;

class Seeder
{
    private function getInstanceFromFileName($filename)
    {
        $folder = Filesystem::directory("database/seeds")->path();
        if (file_exists($folder. DIRECTORY_SEPARATOR . $filename . ".php")) 
        {
            require_once $folder . DIRECTORY_SEPARATOR . $filename . ".php";
            return new $filename;
        } 
        else 
        {
            die("Seed file " . $filename . " doesn't exist in the 'database/seeds' folder.");
        }
    }

    public function seedUp($className)
    {
        $instance = $this->getInstanceFromFileName($className);
        $instance->seed();
    }
}
