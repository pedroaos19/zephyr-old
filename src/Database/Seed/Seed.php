<?php
    namespace Zephyr\Database\Seed;

    interface Seed
    {
        public function seed();
    }
