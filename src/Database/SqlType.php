<?php

namespace Zephyr\Database;

use Zephyr\Utils\Str;

class SqlType
{
    public static function bit()
    {
        return SqlDialect::COL_BIT;
    }

    public static function integer()
    {
        return SqlDialect::COL_INT;
    }

    public static function tinyint()
    {
        return SqlDialect::COL_TINYINT;
    }

    public static function smallint()
    {
        return SqlDialect::COL_SMALLINT;
    }

    public static function mediumint()
    {
        return SqlDialect::COL_MEDIUMINT;
    }

    public static function bigint()
    {
        return SqlDialect::COL_BIGINT;
    }

    public static function float($length = "10", $decimals = "2")
    {
        return tprintf(
            SqlDialect::COL_FLOAT,
            strval($length),
            strval($decimals)
        );
    }

    public static function double($length = "16", $decimals = "4")
    {
        return tprintf(
            SqlDialect::COL_DOUBLE,
            strval($length),
            strval($decimals)
        );
    }

    public static function date()
    {
        return SqlDialect::COL_DATE;
    }

    public static function datetime()
    {
        return SqlDialect::COL_DATETIME;
    }

    public static function timestamp()
    {
        return SqlDialect::COL_TIMESTAMP;
    }

    public static function time()
    {
        return SqlDialect::COL_TIME;
    }

    public static function char($length = "1")
    {
        return tprintf(
            SqlDialect::COL_CHAR,
            strval($length)
        );
    }

    public static function varchar($length = "250")
    {
        return tprintf(
            SqlDialect::COL_VARCHAR,
            strval($length)
        );
    }

    public static function file()
    {
        return tprintf(
            SqlDialect::COL_FILE
        );
    }

    public static function image()
    {
        return tprintf(
            SqlDialect::COL_IMAGE
        );
    }

    public static function text()
    {
        return SqlDialect::COL_TEXT;
    }

    public static function tinytext()
    {
        return SqlDialect::COL_TINYTEXT;
    }

    public static function mediumtext()
    {
        return SqlDialect::COL_MEDIUMTEXT;
    }

    public static function longtext()
    {
        return SqlDialect::COL_LONGTEXT;
    }
}
