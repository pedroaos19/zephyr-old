<?php

namespace Zephyr\Database;

class Table
{
    const ALTER_DROP_NON_PRESENT = 1;

    private function __construct(string $name)
    {
        $this->name = $name;
        $this->model = classerize($name);
        $this->namespace = 'App\\Model\\' . classerize($name);
        $this->schema = Schema::fillByName($name);
    }

    public static function create(string $name, Schema $schema)
    {
        if (!Table::exists($name)) 
        {
            $sql = SqlDialect::createTable($name, $schema->toSql($name));
            Connection::get()->execute($sql);
        } 

        return;
    }

    public static function drop(string $name)
    {
        if (Table::exists($name)) 
        {
            $sql = SqlDialect::dropTable($name);
            Connection::get()->execute($sql);
        } 
        else 
        {
            die("Table " . $name . " doesn't exist.");
        }
    }

    public static function table(string $name)
    {
        return Table::exists($name) ? 
            new Table($name) : 
            null;
    }

    public static function exists(string $name)
    {
        $sql = SqlDialect::allTables();
        $result = Connection::get()->execute($sql);
        if (isset($result))
        {
            $result = is_assoc($result) ? [$result] : $result;
            $names = array_column($result, 'table_name');
            
            return in_array($name, $names);
        }

        return false;
    }

    public static function all($pivots = true, $except = [])
    {
        $sql = SqlDialect::allTables();
        $tables = Connection::get()->execute($sql);

        $tables = is_array($tables) ? $tables : [$tables];

        $tables = array_filter($tables, function($table) use ($except) {
            return !in_array($table["table_name"], $except);
        });
        
        return array_filter(array_map(function ($table) use ($pivots) {
            $tableObj = new Table($table["table_name"]);
            
            if(!$pivots && $tableObj->isPivot()) {} else {
                return $tableObj;
            };
        }, $tables));
    }

    public function addColumns(Schema $schema, $fkCheckDisable = false)
    {
        if($fkCheckDisable) Connection::get()->disableForeignKeyChecks();
        
        $sql = SqlDialect::addColumns($this->name, $schema);
        Connection::get()->execute($sql);

        if($fkCheckDisable) Connection::get()->enableForeignKeyChecks();
    }

    public function renameColumn(string $from, string $to)
    {
        if (!$this->hasColumn($from))
            die("The column $from doesn't exist in the table $this->name");

        $column = $this->schema->column($from);

        $sql = tprintf(
            "%s %s",
            tprintf(SqlDialect::ALTER_TABLE, $this->name),
            SqlDialect::renameColumn($from, $to, $column)
        );

        Connection::get()->execute($sql);
    }

    public function alterColumns(Schema $schema)
    {
        foreach($schema->columns() as $column)
        {
            if(!$this->hasColumn($column->name)) 
                die("The column $column->name doesn't exist in the table $this->name");
        }

        $sql = SqlDialect::modifyColumns($this->name, $this->schema, $schema);
        Connection::get()->execute($sql);
    }

    public function dropColumn(string $columnName)
    {
        if($this->hasColumn($columnName))
        {
            $column = $this->schema->column($columnName);
            
            $contraintDrops = [];
            if($column->unique) 
            {
                $uniqueNameSql = SqlDialect::constraintUniqueName($this->name, $column->name);
                $uniqueName = Connection::get()->execute($uniqueNameSql)["CONSTRAINT_NAME"];

                $sql = SqlDialect::dropConstraintUnique($uniqueName);
                $contraintDrops[] = tprintf(
                    "%s %s",
                    tprintf(SqlDialect::ALTER_TABLE, $this->name),
                    $sql
                );
            }

            if ($column->foreignKey) 
            {
                $fkNameSql = SqlDialect::constraintFkName($this->name, $column->name);
                $fkName = Connection::get()->execute($fkNameSql)["CONSTRAINT_NAME"];
                
                $sql = SqlDialect::dropConstraintFk($fkName);
                
                $contraintDrops[] = tprintf(
                    "%s %s",
                    tprintf(SqlDialect::ALTER_TABLE, $this->name),
                    $sql
                );
            }

            if ($column->primaryKey) 
            {
                $pkNameSql = SqlDialect::constraintFkName($this->name, $column->name);
                $pkName = Connection::get()->execute($pkNameSql)["CONSTRAINT_NAME"];

                $sql = SqlDialect::dropConstraintFk($pkName);

                $contraintDrops[] = tprintf(
                    "%s %s",
                    tprintf(SqlDialect::ALTER_TABLE, $this->name),
                    $sql
                );
            }

            $sqlTransactions = array_merge($contraintDrops, [
                tprintf(
                    "%s %s",
                    tprintf(SqlDialect::ALTER_TABLE, $this->name),
                    SqlDialect::dropColumn($column->name)
                )
            ]);
            
            $sql = join("; ", $sqlTransactions);
            Connection::get()->execute($sql);
        }
        else
        {
            die("The column $columnName doesn't exist in the table $this->name");
        }
    }

    public function hasColumn($column)
    {
        $currentColumnNames = array_map(function ($column) {
            return $column->name;
        }, $this->schema->columns());

        return !in_array($column, $currentColumnNames) ? false : true;
    }

    public function isPivot()
    {
        $sql = SqlDialect::belongsTo($this->name);
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;
        
        if(is_null($result)) return false;

        $names = explode("_", $this->name);
        
        if(count(array_column($result, "TABLE_NAME")) !== 2) return false;
        if(!in_array(pluralize($names[0]), array_column($result, "REFERENCED_TABLE_NAME"))) return false;
        if(!in_array(pluralize($names[1]), array_column($result, "REFERENCED_TABLE_NAME"))) return false;
   
        return true;
    }

    public function belongsToTables()
    {
        $sql = SqlDialect::belongsTo($this->name);
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;

        return !is_null($result) ? $result : [];
    }

    public function hasManyTables(bool $withPivots = false)
    {
        $sql = SqlDialect::hasMany($this->name);
        $result = Connection::get()->execute($sql);
        $result = is_assoc($result) ? [$result] : $result;

        if(is_null($result)) return [];
        if($withPivots) return $result;

        return array_filter($result, function ($table) {
            return !(Table::table($table["table"])->isPivot());
        });
    }

    public function belongsToManyTables()
    {
        $hasManyTables = $this->hasManyTables(true);
        $pivots = array_filter($hasManyTables, function($table) {
            return Table::table($table["table"])->isPivot();
        });

        $pivotsBelongsTo = array_map(function($pivot) {
            $belongsTo = Table::table($pivot["table"])->belongsToTables();
            $pivotedTable = array_filter($belongsTo, function($table) {
                return $table["REFERENCED_TABLE_NAME"] !== $this->name;
            });
            $pivotedTable = array_pop($pivotedTable);

            return ["table" => $pivotedTable["REFERENCED_TABLE_NAME"], "pivot" => $pivot["table"]];
        }, $pivots);

        return $pivotsBelongsTo;
    }
}