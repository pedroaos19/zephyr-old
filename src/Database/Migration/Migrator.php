<?php

namespace Zephyr\Database\Migration;

use Zephyr\Database\Connection;
use Zephyr\Database\SqlDialect;
use Zephyr\Database\Table;
use Zephyr\Database\Schema;
use Zephyr\Filesystem\Filesystem;

class Migrator
{
    private $files = [];

    public function __construct()
    {
        $this->getFiles();
    }

    private function getFiles()
    {
        $pattern = Filesystem::directory('database/migrations')->path() . DIRECTORY_SEPARATOR . "*.php";
        foreach (glob($pattern) as $filename) 
        {
            $filename = str_replace(
                Filesystem::directory('database/migrations')->path() . DIRECTORY_SEPARATOR, 
                "", 
                $filename
            );
            array_push($this->files, $filename);
        }
    }

    public function createMigrationsTableIfNotExists()
    {
        if (!Table::exists('migrations')) 
        {
            $schema = new Schema;
            $schema->primary('id');
            $schema->varchar('file_name');
            $schema->integer('batch');

            Table::create('migrations', $schema);
        }
    }

    private function getInstanceFromFileName($filename)
    {
        $folder = Filesystem::directory('database/migrations')->path();
        if (file_exists($folder . DIRECTORY_SEPARATOR . $filename)) 
        {
            require_once $folder . DIRECTORY_SEPARATOR . $filename;

            $filenameArray = explode("_", $filename);
            array_shift($filenameArray);
            $filenameNoTimestamp = implode("_", $filenameArray);

            $className = camelize(str_replace(".php", "", $filenameNoTimestamp));
            return new $className;
        } 
        else 
        {
            die("Migration file " . $filename . "doesn't exist in the 'database/migrations' folder.");
        }
    }

    private function getAllMigrations()
    {
        $sql = SqlDialect::select('migrations');
        $migrations = Connection::get()->execute($sql);

        return isset($migrations) ? $migrations : null;
    }

    public function getLastBatchNumber()
    {
        $sql = SqlDialect::select('migrations', ["MAX(batch)"]);
        $migrations = Connection::get()->execute($sql);
        
        return isset($migrations) && isset($migrations["MAX(batch)"]) ? intval($migrations["MAX(batch)"]) : 0;
    }

    private function getNextBatchNumber()
    {
        return $this->getLastBatchNumber() + 1;
    }

    private function getLastBatch()
    {
        $sql = SqlDialect::select('migrations', null, ["batch"], ["="]);
        $migrations = Connection::get()->execute($sql, [$this->getLastBatchNumber()]);

        return $migrations;
    }

    public function getNextBatch()
    {
        $lastBatch = $this->getLastBatch();

        if (isset($lastBatch)) 
        {
            $lastBatch = is_assoc($lastBatch) ? [$lastBatch] : $lastBatch;

            $lastMigration = $lastBatch[count($lastBatch) - 1];
            $lastMigrationIndex = array_search($lastMigration["file_name"], $this->files);
            return array_slice($this->files, $lastMigrationIndex + 1);
        }
        else 
        {
            return $this->files;
        }
    }

    public function runUp()
    {
        $this->createMigrationsTableIfNotExists();
        $nextBatch = $this->getNextBatch();
        $nextBatchNumber = $this->getNextBatchNumber();
        
        $migrationsColumns = Table::table('migrations')->schema->names();
        if (isset($nextBatch) && count($nextBatch) > 0) 
        {
            foreach ($nextBatch as $migrationFile) 
            {
                echo "Migrating file '" . $migrationFile . "'" . PHP_EOL;
                $instance = $this->getInstanceFromFileName($migrationFile);
                $instance->up();

                $sql = SqlDialect::insert('migrations', $migrationsColumns);
                Connection::get()->execute($sql, [$migrationFile, $nextBatchNumber]);
            }
        } 
        else 
        {
            die("No migrations to perform.");
        }
    }

    public function runRollback()
    {
        $this->createMigrationsTableIfNotExists();
        $lastBatchNumber = $this->getLastBatchNumber();
        $lastBatch = $this->getLastBatch();

        if (isset($lastBatch)) 
        {
            $lastBatch = is_assoc($lastBatch) ? [$lastBatch] : $lastBatch;
            $lastBatch = array_reverse($lastBatch);

            foreach ($lastBatch as $migrationFile) 
            {
                echo "Rolling back Migration file '" . $migrationFile["file_name"] . "'" . PHP_EOL;
                $instance = $this->getInstanceFromFileName($migrationFile["file_name"]);
                $instance->down();

                $sql = SqlDialect::delete('migrations', ['batch'], ['=']);
                Connection::get()->execute($sql, [$lastBatchNumber]);
            }
        } 
        else 
        {
            die("No migrations to rollback.");
        }
    }

    public function rollbackAll()
    {
        $this->createMigrationsTableIfNotExists();
        $migrations = $this->getAllMigrations();

        if (isset($migrations)) 
        {
            $migrations = is_assoc( $migrations) ? [ $migrations] : $migrations;
            $migrations = array_reverse($migrations);

            foreach ($migrations as $migration) 
            {
                echo "Rolling back migration file '" . $migration["file_name"] . "'" . PHP_EOL;
                $instance = $this->getInstanceFromFileName($migration["file_name"]);
                $instance->down();

                $sql = SqlDialect::delete('migrations', ["file_name"], ["="]);
                Connection::get()->execute($sql, [$migration["file_name"]]);
            }
        }
        else
        {
            die("No migrations to rollback.");
        }
    }

    /**
     *
     */
    public function refresh()
    {
        $this->rollbackAll();
        $this->runUp();
    }
}
