<?php
namespace Zephyr\Database\Migration;

/**
 * Class Migration
 * @package Zephyr\Database\Migration
 */
interface Migration
{
    public function up();

    public function down();
}
