<?php

namespace Zephyr\Database;

use Zephyr\Utils\Str;
use Zephyr\Database\SqlType;

class Schema
{
    private $columns = [];

    public static function fillByName($name)
    {
        $sql = SqlDialect::allColumns($name);
        $result = Connection::get()->execute($sql);

        $result = !is_assoc($result) ? $result : [$result];

        $schema = new Schema;
        foreach($result as $column)
        {
            $type = null;
            if($column["COLUMN_TYPE"] === "varchar(510)")
            {
                $type = "file";
            }
            else if($column["COLUMN_TYPE"] === "varchar(512)")
            {
                $type = "image";
            }
            else if(Str::contains($column["COLUMN_TYPE"], "("))
            {
                $type = explode("(", $column["COLUMN_TYPE"])[0];
            } 
            else
            {
                $type = $column["COLUMN_TYPE"];
            }

            $type = $type === "int" ? "integer" : $type;

            $newColumn = new Column(
                $column["COLUMN_NAME"],
                // forward_static_call([SqlType::class, $column["DATA_TYPE"]]),
                forward_static_call([SqlType::class, $type]),
                $column["IS_NULLABLE"] === "YES" ? true : false,
                $column["COLUMN_DEFAULT"],
                Str::contains($column["EXTRA"], "auto_increment") ? true : false
            );
            
            if(Str::contains($column["COLUMN_KEY"], "PRI"))
            {
                $newColumn->asPrimaryKey();
            }

            if (Str::contains($column["COLUMN_KEY"], "MUL")) 
            {
                $fkSql = SqlDialect::constraintFkName($name, $column["COLUMN_NAME"]);
                $fkSqlResult = Connection::get()->execute($fkSql);
                
                $newColumn->asForeignKey(
                    $fkSqlResult["REFERENCED_TABLE_NAME"],
                    $fkSqlResult["REFERENCED_COLUMN_NAME"]
                );
            }

            if (Str::contains($column["COLUMN_KEY"], "UNI")) 
            {
                $newColumn->unique();
            }
            
            $schema->add($newColumn);
        }

        return $schema;
    }

    public function add(Column $column)
    {
        $this->columns[] = $column;
        return $column;
    }

    public function primary()
    {
        return $this->add(
            (new Column("id", SqlDialect::COL_BIGINT, false, null, true))->asPrimaryKey()
        );
    }

    public function foreign(string $name, $foreignTable, $foreignColumn = 'id')
    {
        return $this->add(
            (new Column($name, SqlDialect::COL_BIGINT))->asForeignKey($foreignTable, $foreignColumn)
        );
    }

    public function bit(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_BIT)
        );
    }

    public function integer(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_INT)
        );
    }

    public function tinyint(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_TINYINT)
        );
    }

    public function smallint(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_SMALLINT)
        );
    }

    public function mediumint(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_MEDIUMINT)
        );
    }

    public function bigint(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_BIGINT)
        );
    }

    public function increments($name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_BIGINT, false, null, true)
        );
    }

    public function float(string $name, $length = "10", $decimals = "2")
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_FLOAT,
                strval($length),
                strval($decimals)
            ))
        );
    }

    public function double(string $name, $length = "16", $decimals = "4")
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_DOUBLE,
                strval($length),
                strval($decimals)
            ))
        );
    }

    public function date(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_DATE)
        );
    }

    public function datetime(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_DATETIME)
        );
    }

    public function timestamp(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_TIMESTAMP)
        );
    }

    public function timestamps()
    {
        $this->add(
            new Column('created_at', SqlDialect::COL_TIMESTAMP)
        );

        $this->add(
            new Column('updated_at', SqlDialect::COL_TIMESTAMP)
        );
    }

    public function time(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_TIME)
        );
    }

    public function char(string $name, $length = "1")
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_CHAR,
                strval($length)
            ))
        );
    }

    public function varchar(string $name, $length = "250")
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_VARCHAR,
                strval($length)
            ))
        );
    }

    public function file(string $name)
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_FILE
            ))
        );
    }

    public function image(string $name)
    {
        return $this->add(
            new Column($name, tprintf(
                SqlDialect::COL_IMAGE
            ))
        );
    }

    public function text(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_TEXT)
        );
    }

    public function tinytext(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_TINYTEXT)
        );
    }

    public function mediumtext(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_MEDIUMTEXT)
        );
    }

    public function longtext(string $name)
    {
        return $this->add(
            new Column($name, SqlDialect::COL_LONGTEXT)
        );
    }

    public function columns($withId = true)
    {
        $columns = [];
        foreach ($this->columns as $column) {
            if ($column->name === "id" && !$withId) {
                continue;
            }

            $columns[] = $column;
        }

        return $columns;
    }

    public function names($withId = false)
    {
        $names = [];
        foreach($this->columns as $column)
        {
            if($column->name === "id" && !$withId)
            {
                continue;
            }

            $names[] = $column->name;
        }
        
        return $names;
    }

    public function column($name)
    {
        $result = null;

        foreach($this->columns as $column)
        {
            if($column->name === $name)
            {
                $result = $column;
            }
        }

        return $result;
    }

    public function size()
    {
        return count($this->columns);
    }

    public function toSql($table)
    {
        return join(", ", array_map(function($column) use ($table) {
            return $column->sql($table);
        }, $this->columns));
    }
}