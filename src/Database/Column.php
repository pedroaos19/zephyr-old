<?php

namespace Zephyr\Database;

use Zephyr\Utils\Str;

class Column
{
    public function __construct($name, $type, $nullable = false, $default = null, $autoIncrement = false, $after = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->nullable = $nullable;
        $this->default = isset($default) && is_string($default) ? rtrim(ltrim($default, "'"), "'") : $default;
        $this->autoIncrement = $autoIncrement;
        $this->after = $after;

        $this->primaryKey = false;
        $this->foreignKey = false;
        $this->unique = false;

        if($this->name === "updated_at")
            dd($this->sql("test"));
    }

    public function humanizedType()
    {
        $type = null;

        if ($this->type === "VARCHAR(510)") {
            $type = "file";
        } else if ($this->type === "VARCHAR(512)") {
            $type = "image";
        } else if (Str::contains($this->type, "(")) {
            $type = explode("(", $this->type)[0];
        } else {
            $type = $this->type;
        }

        return humanize($type);
    }

    public function nullable(bool $nullable = true)
    {
        $this->nullable = $nullable;
        return $this;
    }

    public function default($value)
    {
        $this->default = $value;
        return $this;
    }

    public function unique(bool $unique = true)
    {
        $this->unique = $unique;
        return $this;
    }

    public function after($after)
    {
        $this->after = $after;
        return $this;
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function autoIncrement()
    {
        $this->autoIncrement = true;
        return $this;
    }

    public function asPrimaryKey()
    {
        $this->primaryKey = true;
        return $this;
    }

    public function asForeignKey($foreignTable, $foreignColumn = 'id')
    {
        $this->foreignKey = [
            "table" => $foreignTable,
            "column" => $foreignColumn
        ];
        return $this;
    }

    public function sql($table)
    {
        $sql = SqlDialect::column(
            $this->name,
            $this->type,
            $this->nullable,
            $this->default,
            $this->autoIncrement,
            $this->after
        );

        if($this->primaryKey)
        {
            $sql = tprintf(
                "%s, %s", 
                $sql, 
                !Table::exists($table) ? 
                    SqlDialect::constraintPk($table, $this->name) : 
                    SqlDialect::addConstraintPk($table, $this->name)
            );
        }

        if($this->foreignKey)
        {
            $sql = tprintf(
                "%s, %s",
                $sql,
                !Table::exists($table) ? 
                    SqlDialect::constraintFk(
                        $table,
                        $this->name,
                        $this->foreignKey["table"],
                        $this->foreignKey["column"]
                    ) :
                    SqlDialect::addConstraintFk(
                        $table,
                        $this->name,
                        $this->foreignKey["table"],
                        $this->foreignKey["column"]
                    )
            );
        }

        if($this->unique)
        {
            $sql = tprintf(
                "%s, %s",
                $sql,
                !Table::exists($table) ?
                    SqlDialect::constraintUnique($table, $this->name) :
                    SqlDialect::addConstraintUnique($table, $this->name)
            );
        }

        return $sql;
    }

    public static function compareToSql($table, $oldColumn, $newColumn)
    {
        $sql = SqlDialect::column(
            $newColumn->name,
            $newColumn->type,
            $newColumn->nullable,
            $newColumn->default,
            $newColumn->autoIncrement,
            $newColumn->after
        );

        if ($oldColumn->primaryKey && !$newColumn->primaryKey) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::dropConstraintPk(SqlDialect::constraintPkName($table, $newColumn->name))
            );  
        } else if (!$oldColumn->primaryKey && $newColumn->primaryKey) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::addConstraintPk($table, $newColumn->name)
            ); 
        }

        if ($oldColumn->unique && !$newColumn->unique) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::dropConstraintUnique(SqlDialect::constraintUniqueName($table, $newColumn->name))
            );  
        } else if (!$oldColumn->unique && $newColumn->unique) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::addConstraintUnique($table, $newColumn->name)
            ); 
        }

        if ($oldColumn->foreignKey && !$newColumn->foreignKey) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::dropConstraintFk(SqlDialect::constraintFkName($table, $newColumn->name))
            );
        } else if (!$oldColumn->foreignKey && $newColumn->foreignKey) {
            $sql = tprintf(
                "%s, %s",
                $sql,
                SqlDialect::addConstraintFk(
                    $table, 
                    $newColumn->name, 
                    $newColumn->foreignKey["table"],
                    $newColumn->foreignKey["column"])
            ); 
        }

        return $sql;
    }
}
