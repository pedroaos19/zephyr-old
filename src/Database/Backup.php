<?php
    namespace Zephyr\Database;

    use Zephyr\Filesystem\Filesystem;

    class Backup
    {
        public static function dump()
        {
            $dir = Filesystem::directory('database/dumps');
            $fileName = time() . "_" . getenv("DB_NAME") . "_dump";
            $dumpName = $dir . DIRECTORY_SEPARATOR . $fileName;
            
            $script = "mysqldump --host=" . getenv("DB_HOST") . " --port=" . getenv("DB_PORT") .  " --user=" . getenv("DB_USERNAME") . " --password=" . getenv("DB_PASSWORD") . " --opt " . getenv("DB_NAME") . " > $dumpName.sql";
            system($script, $output);
        }
    }