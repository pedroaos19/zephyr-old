<?php

namespace Zephyr\Database;

class PivotTable extends Table
{
    public static function pivot(string $firstTable, string $secondTable, Schema $schema, string $as = null)
    {
        if (Table::exists($firstTable) && Table::exists($secondTable)) 
        {
            $firstColumn = singularize($firstTable);
            $secondColumn = singularize($secondTable);

            $tablesArray = [$firstColumn, $secondColumn];
            sort($tablesArray);

            $tableName = isset($as) ? $as : implode("_", $tablesArray);
            if (!Table::exists($tableName)) 
            {
                $schema->foreign($firstColumn . "_id", $firstTable);
                $schema->foreign($secondColumn . "_id", $secondTable);

                $sql = SqlDialect::createTable($tableName, $schema->toSql($tableName));
                Connection::get()->execute($sql);
            } 
            else 
            {
                die("Table " . $tableName . " already exists.");
            }
        } 
        else 
        {
            die("One of the tables you want to pivot doesn't exist");
        }
    }
}
