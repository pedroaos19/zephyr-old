<?php

namespace Zephyr\Database;

class Sql
{
    /**
     * SELECT (1)DISTINCT (2)[COLUMN] FROM (3)[TABLE] (4)WHERE (5)GROUP BY (6)HAVING (7)ORDER BY (8)LIMIT (9)OFFSET
     */
    const SELECT = "SELECT %s %s FROM %s %s %s %s %s %s %s";
    const DISTINCT = "DISTINCT";
    const WHERE = "WHERE %s";
    const WHERE_AND = " AND ";
    const WHERE_OR = " OR ";

    const INNER_JOIN = "INNER JOIN %s ON %s";
    const LEFT_JOIN = "LEFT JOIN %s ON %s";
    const RIGHT_JOIN = "RIGHT JOIN %s ON %s";
    const OUTER_JOIN = "OUTER JOIN %s ON %s";

    const EQUALS = "%s = %s";
    const LESS = "%s < %s";
    const MORE = "%s > %s";
    const LESS_EQUALS = "%s <= %s";
    const MORE_EQUALS = "%s >= %s";
    const EVAL = "%s %s ?";

    const BETWEEN = "%s BETWEEN ? AND ?";
    const NOT_BETWEEN = "%s NOT BETWEEN ? AND ?";
    const IN = "%s IN (%s)";
    const NOT_IN = "%s NOT IN (%s)";
    const LIKE = "%s LIKE ?";
    const NOT_LIKE = "%s NOT LIKE ?";

    const GROUP_BY = "GROUP BY %s";
    const HAVING = "HAVING %s";

    const ORDER_BY = "ORDER BY %s";
    const ASC = "ASC";
    const DESC = "DESC";

    const LIMIT = "LIMIT %s";
    const LIMIT_MAX = "18446744073709551615";
    const OFFSET = "OFFSET %s";

    const ALL_COLS = "*";

    const COUNT = "COUNT(%s) as count";
    const AVG = "AVG(%s) as avg";
    const SUM = "SUM(%s) as sum";
}