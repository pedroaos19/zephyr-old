<?php 
    namespace Zephyr\Pinpoint;

    use GeoIp2\Database\Reader;

    class Pinpointer
    {
        private static $cityReader;
        private static $countryReader;

        public static function country($publicIp)
        {
            if(static::$countryReader == null)
            {
                static::$countryReader = new Reader('src/Pinpoint/resources/GeoLite2-Country.mmdb');
            }

            if(static::$cityReader == null)
            {
                static::$cityReader = new Reader('src/Pinpoint/resources/GeoLite2-City.mmdb');
            }

            $point = static::$countryReader->country($publicIp);
            $cityPoint = static::$cityReader->city($publicIp);
            
            return new CountryPoint(
                $point->country->names['en'],
                $point->country->names['pt-BR'],
                $point->country->isoCode,
                $point->country->isInEuropeanUnion,
                $cityPoint->location->latitude,
                $cityPoint->location->longitude
            );
        }

        public static function own()
        {
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            $publicIp = $m[1];

            return static::country($publicIp);
        }
    }
