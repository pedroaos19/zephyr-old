<?php
namespace Zephyr\Pinpoint;

class CountryPoint
{
    public function __construct($name, $namePortuguese, $iso, $eu, $latitude, $longitude)
    {
        $this->name = $name;
        $this->namePt = $namePortuguese;
        $this->iso = $iso;
        $this->eu = $eu;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function name($pt = false)
    {
        return $pt ? $this->namePt : $this->name;
    }

    public function iso()
    {
        return $this->iso;
    }

    public function isInEU()
    {
        return $this->eu;
    }

    public function latitude()
    {
        return $this->latitude;
    }

    public function longitude()
    {
        return $this->longitude;
    }

    public function coordinates()
    {
        return [
            "latitude" => $this->latitude,
            "longitude" => $this->longitude
        ];
    }
}
