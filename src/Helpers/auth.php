<?php

use Zephyr\Router\Map;
use Zephyr\Auth\Membership;
use Zephyr\Auth\Tokenship;
use Zephyr\Auth\Middleware\CheckAuthenticated;

if (!function_exists('membership_routes')) 
{
    function membership_routes()
    {
        Map::instance()->get("/account/login", "AccountController.login", [
            "middleware" => [CheckAuthenticated::class]
        ]);
        Map::instance()->post("/account/signin", "AccountController.signin");
        Map::instance()->get("/account/register", "AccountController.register", [
            "middleware" => [CheckAuthenticated::class]
        ]);
        Map::instance()->post("/account/signup", "AccountController.signup");
        Map::instance()->post("/account/signout", "AccountController.signout");
    }
}

if (!function_exists('tokenship_routes')) 
{
    function tokenship_routes()
    {
        Map::instance()->post("/api/account/signin", "TokenshipController.signin");
        Map::instance()->post("/api/account/signup", "TokenshipController.signup");
        Map::instance()->get("/api/account/member", "TokenshipController.member");
    }
}

if (!function_exists('membership')) 
{
    function membership()
    {
        return Membership::use();
    }
}

if (!function_exists('auth')) 
{
    function auth()
    {
        return Membership::use()->authenticated();
    }
}

if (!function_exists('member')) 
{
    function member()
    {
        return Membership::use()->member();
    }
}

if (!function_exists('tokenship')) 
{
    function tokenship()
    {
        return Tokenship::use();
    }
}