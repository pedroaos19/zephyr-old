<?php

use Zephyr\Atlas\AtlasRouter;
use Zephyr\Database\Table;
use Zephyr\Model\Query;

include "auth.php";
include "http.php";
include "logger.php";
include "misc.php";
include "public.php";
include "router.php";
include "session.php";
include "str.php";

if (!function_exists('atlas_routes')) 
{
    function atlas_routes()
    {
        AtlasRouter::routes();
    }
}

if (!function_exists('atlas_foreign_key')) 
{
    function atlas_foreign_key($table, $relations, $column)
    {
        foreach($relations as $relation)
        {
            if($relation->table_name === $table->name && $relation->column_name === $column)
            {
                $definition = json_decode($relation->definition, true);
                return $definition["display_as"];
            }
        }
    }
}

if (!function_exists('atlas_foreign_value')) 
{
    function atlas_foreign_value($table, $relations, $column, $record)
    {
        foreach($relations as $relation)
        {
            if($relation->table_name === $table->name && $relation->column_name === $column)
            {
                $definition = json_decode($relation->definition, true);

                $results = (new Query(null, $definition["referenced"]))->where('id', $record->$column)->get();
                $ref_column = $definition["display_column"];

                return $results->first()->$ref_column;
            }
        }
    }
}



