<?php

use Zephyr\Logger\Logger;

if (!function_exists('logger')) 
{
    function logger()
    {
        return Logger::get();
    }
}