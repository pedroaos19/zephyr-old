<?php

use ICanBoogie\Inflector;

if (!function_exists('singularize')) 
{
    function singularize($word)
    {
        return Inflector::get('en')->singularize($word);
    }
}

if (!function_exists('pluralize')) 
{
    function pluralize($word)
    {
        return Inflector::get('en')->pluralize($word);
    }
}

if (!function_exists('camelize')) 
{
    function camelize($word, $tableToClass = false)
    {
        $word = $tableToClass ? singularize($word) : $word;
        return Inflector::get('en')->camelize($word);
    }
}

if (!function_exists('humanize')) 
{
    function humanize($word)
    {
        return Inflector::get('en')->humanize($word);
    }
}

if (!function_exists('classerize')) 
{
    function classerize($word)
    {
        return Inflector::get('en')->camelize(singularize($word));
    }
}

if (!function_exists('underscorize')) 
{
    function underscorize($word, $classToTable = false)
    {
        $word = $classToTable ? pluralize($word) : $word;
        return Inflector::get('en')->underscore($word);
    }
}

if (!function_exists('tablerize')) 
{
    function tablerize($word)
    {
        return Inflector::get('en')->underscore(pluralize($word));
    }
}