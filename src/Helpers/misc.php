<?php

use Noodlehaus\Config;
use Carbon\Carbon;

if (!function_exists('conf')) 
{
    function conf()
    {
        $conf = new Config('config/default.php');
        return $conf;
    }
}

if (!function_exists('env')) 
{
    function env($name, $value)
    {
        if (!defined($name)) define($name, $value);
    }
}

if (!function_exists('dd')) 
{
    function dd($var)
    {
        die(var_dump($var));
    }
}

if (!function_exists('app_dir')) 
{
    function app_dir()
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }
}

if (!function_exists('project_dir')) 
{
    function project_dir()
    {
        return getcwd();
    }
}

if (!function_exists('encrypt')) 
{
    function encrypt($value)
    {
        return password_hash($value, PASSWORD_BCRYPT);
    }
}

if (!function_exists('tprintf')) 
{
    function tprintf($string, ...$args)
    {
        return str_replace("  ", " ", trim(vsprintf($string, $args)));
    }
}

if (!function_exists('is_assoc')) 
{
    function is_assoc($arr)
    {
        if (is_null($arr)) return false;
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}

if (!function_exists('now')) 
{
    function now()
    {
        return Carbon::now();
    }
}

if (!function_exists('_sqltype')) 
{
    function _sqltype($var)
    {
        if (is_string($var)) {
            return "'$var'";
        } else if ($var instanceof \Carbon\Carbon) {
            $dateText = $var->format("Y-m-d H:i:s");
            return "'$dateText'";
        } else {
            return $var;
        }
    }
}

if (!function_exists('is_json')) 
{
    function is_json(string $string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (!function_exists('switchval')) 
{
    function switchval($var)
    {
        $result = null;

        if (is_string($var)) {
            $result = strval($var);
        } else if (is_integer($var)) {
            $result = intval($var);
        } else if (is_double($var)) {
            $result = doubleval($var);
        } else if (is_bool($var)) {
            $result = boolval($var);
        } else if (is_array($var)) {
            $result = (array) $var;
        }

        return $result;
    }
}