<?php

use Zephyr\View\View;
use Zephyr\Http\Response\Response;
use Zephyr\Http\Response\JsonResponse;
use Zephyr\Http\Response\ViewResponse;
use Zephyr\Http\Response\RedirectResponse;
use Zephyr\Http\Response\RedirectToActionResponse;

if (!function_exists('getallheaders')) 
{
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

if (!function_exists('current_url')) 
{
    function current_url()
    {
        return sprintf(
            "%s://%s%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['SERVER_PORT'] == "80" ? "" : ":" . $_SERVER['SERVER_PORT'],
            $_SERVER['REQUEST_URI']
        );
    }
}

if (!function_exists('base_url')) 
{
    function base_url()
    {
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['SERVER_PORT'] === "80" ? "" : ":" . $_SERVER['SERVER_PORT']
        );
    }
}

if (!function_exists('response')) 
{
    function response($content)
    {
        return new Response($content);
    }
}

if (!function_exists('view')) 
{
    function view(string $filename, array $data = null)
    {
        return new ViewResponse(View::render($filename, $data));
    }
}

if (!function_exists('json')) 
{
    function json($data)
    {
        return new JsonResponse($data);
    }
}

if (!function_exists('redirect')) 
{
    function redirect($route)
    {
        return new RedirectResponse($route);
    }
}

if (!function_exists('action')) 
{
    function action($controller, $action, array $params)
    {
        return new RedirectToActionResponse($controller, $action, $params);
    }
}

if (!function_exists('send_404')) 
{
    function send_404()
    {
        die("<h3 style='background-color: #202020; color: #59f270; border-radius: 4px; padding: 10px;'>Page not found.</h3>");
    }
}