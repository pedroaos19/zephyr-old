<?php

use Zephyr\Router\Map;
use Zephyr\Utils\Str;

if (!function_exists('route')) 
{
    function route($alias, $params = null)
    {
        $route = Map::instance()->findRouteByAlias($alias);
        if (is_null($route)) die(trigger_error("A route with the alias \"" . $alias . "\" doesn't exist."));

        return is_null($params) ? $route->path : $route->applyParams($params);
    }
}

if (!function_exists('route_get')) 
{
    function route_get($uri, $mapping, $options = null)
    {
        Map::instance()->get($uri, $mapping, $options);
    }
}

if (!function_exists('route_post')) 
{
    function route_post($uri, $mapping, $options = null)
    {
        Map::instance()->post($uri, $mapping, $options);
    }
}

if (!function_exists('route_group')) 
{
    function route_group($baseUri, array $children, $options = null)
    {
        Map::instance()->group($baseUri, $children, $options);
    }
}

if (!function_exists('route_spa')) 
{
    function route_spa($mapping, $options = null)
    {
        Map::instance()->spa($mapping, $options);
    }
}

if (!function_exists('route_active')) 
{
    function route_active($route, $exact = false)
    {
        $url = str_replace(base_url(), "", current_url());

        $url = rtrim($url, "/");
        $route = rtrim($route, "/");

        return $exact ? $url === $route : Str::contains($url, $route);
    }
}

if (!function_exists('current_route')) 
{
    function current_route()
    {
        $url = str_replace(base_url(), "", current_url());
        $url = rtrim($url, "/");

        return $url;
    }
}