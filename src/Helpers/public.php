<?php

if (!function_exists('asset')) 
{
    function asset($file)
    {
        return base_url() . '/public/' . $file;
    }
}

if (!function_exists('css')) 
{
    function css($file)
    {
        print "<link href=\"" . asset('css/' . $file) . "\" rel=\"stylesheet\"/>";
    }
}

if (!function_exists('js')) 
{
    function js($file)
    {
        print "<script src=\"" . asset('js/' . $file) . "\"></script>";
    }
}