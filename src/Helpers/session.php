<?php

use Zephyr\Http\Session;
use Zephyr\Http\Security\Csrf;

if (!function_exists('flash')) 
{
    function flash($key, $message = null)
    {
        if (isset($message)) 
        {
            Session::capture()->flash($key, $message);
        } 
        else 
        {
            return Session::capture()->flash($key);
        }
    }
}

if (!function_exists('session_set')) 
{
    function session_set($key, $message)
    {
        if (is_array($message)) 
        {
            $message = json_encode($message);
        }

        Session::capture()->set($key, $message);
    }
}

if (!function_exists('session_get')) 
{
    function session_get($key)
    {
        $message = Session::capture()->get($key);
        $finalMessage = json_decode($message, true);

        Session::capture()->delete($key);

        return json_last_error() == JSON_ERROR_NONE ? $finalMessage : $message;
    }
}

if (!function_exists('errors')) 
{
    function errors($message = null)
    {
        if (isset($message)) 
        {
            flash("form_errors", $message);
        } 
        else 
        {
            return flash("form_errors");
        }
    }
}

if (!function_exists('csrf_token')) 
{
    function csrf_token()
    {
        echo Csrf::token();
    }
}

if (!function_exists('csrf_token_raw')) 
{
    function csrf_token_raw()
    {
        echo Csrf::rawToken();
    }
}