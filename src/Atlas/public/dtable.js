window.addEventListener("load", function() {
    var tables = document.querySelectorAll(".dtable");
    tables.forEach(function(table) {
        dTable(table);
    });

    function dTable(table) {
        this.reference = table;
        this.currentPage = 1;

        this.getNumberOfRowsPerPage = function() {
            return this.reference.getAttribute("data-rpp") !== null
                ? parseInt(this.reference.getAttribute("data-rpp"))
                : 10;
        };

        this.clearTableBody = function() {
            this.reference.querySelector("tbody").innerHTML = "";
        };

        this.getColumns = function() {
            var columns = [];

            var theads = this.reference.querySelectorAll("thead>tr>th");
            theads.forEach(function(thead) {
                columns.push(thead.innerText.toLowerCase());
            });

            return columns;
        };

        this.getData = function() {
            var data = [];

            var rows = this.reference.querySelectorAll("tbody>tr");

            this.rowAttributes = {};
            rows.forEach(function(row, index) {
                attr = {};
                for (var i = 0; i < row.attributes.length; i++) {
                    var attrib = row.attributes[i];
                    attribName = attrib.name;
                    attribValue = attrib.value;

                    attr[attribName] = attribValue;
                }

                this.rowAttributes[index] = attr;

                var dataRow = [];
                var cells = row.querySelectorAll("td");

                cells.forEach(function(cell) {
                    dataRow.push(cell.cloneNode(true));
                });

                data.push(dataRow);
            });
            
            return data;
        };

        this.buildButton = function(text, isActive) {
            var item = buildElement("div", { class: "pagination__item" });
            var button = buildElement("button", {
                text: text,
                class: isActive ? "pagination__active" : ""
            });

            item.appendChild(button);
            return item;
        };

        this.buildPrevButton = function() {
            var button = this.buildButton("Prev");
            if (this.currentPage === 1) {
                button
                    .querySelector("button")
                    .setAttribute("class", "pagination__disabled");
            }

            _ = this;
            button
                .querySelector("button")
                .addEventListener("click", function() {
                    if (_.currentPage - 1 > 0) {
                        _.currentPage--;
                        _.buildPagination();
                        _.buildBody();
                    }
                });

            return button;
        };

        this.buildNextButton = function() {
            var button = this.buildButton("Next");
            if (this.currentPage === this.totalPages) {
                button
                    .querySelector("button")
                    .setAttribute("class", "pagination__disabled");
            }

            _ = this;
            button
                .querySelector("button")
                .addEventListener("click", function() {
                    if (_.currentPage + 1 <= _.totalPages) {
                        _.currentPage++;
                        _.buildPagination();
                        _.buildBody();
                    }
                });

            return button;
        };

        this.buildSpan = function(text) {
            var item = buildElement("div", { class: "pagination__item" });
            var span = buildElement("span", { text: text });

            item.appendChild(span);
            return item;
        };

        this.getPageRange = function() {
            var delta = 1;
            var left = this.currentPage - delta;
            var right = this.currentPage + delta;
            var range = [];
            var rangeWithDots = [];
            var lastInserted;

            for (var i = 1; i <= this.totalPages; i++) {
                if (
                    i == 1 ||
                    i == this.totalPages ||
                    (i >= left && i <= right)
                ) {
                    range.push(i);
                }
            }

            range.forEach(function(el) {
                if (lastInserted) {
                    if (el - lastInserted === 2) {
                        rangeWithDots.push(lastInserted + 1);
                    } else if (el - lastInserted !== 1) {
                        rangeWithDots.push("...");
                    }
                }
                rangeWithDots.push(el);
                lastInserted = el;
            });

            return rangeWithDots;
        };

        this.getPageButtons = function() {
            var buttons = [];

            var range = this.getPageRange();
            range.forEach(function(text) {
                if (text === "...") {
                    buttons.push(this.buildSpan("..."));
                    return;
                }

                var button = this.buildButton(
                    text,
                    parseInt(text) === this.currentPage
                );
                var _ = this;
                button.addEventListener("click", function(e) {
                    var pageClicked = parseInt(e.target.innerText);
                    _.currentPage = pageClicked;
                    _.buildPagination();
                    _.buildBody();
                });

                buttons.push(button);
            });

            return buttons;
        };

        this.buildPagination = function() {
            var oldPagination = this.reference.parentNode.querySelector(
                "#pagination"
            );
            if (oldPagination) {
                this.reference.parentNode.removeChild(oldPagination);
            }

            var container = buildElement("div", {
                class: "align__r",
                id: "pagination"
            });
            var pagination = buildElement("div", {
                class: "dtable__pagination"
            });

            pagination.appendChild(this.buildPrevButton());
            var buttons = this.getPageButtons();
            buttons.forEach(function(button) {
                pagination.appendChild(button);
            });
            pagination.appendChild(this.buildNextButton());

            container.appendChild(pagination);
            this.reference.after(container);
        };

        this.buildCellRow = function(dataRow, index) {
            var row = buildElement("tr", this.rowAttributes[index]);

            dataRow.forEach(function(dataCell) {
                var cell = dataCell;
                row.appendChild(cell);
            });

            return row;
        };

        this.getRows = function() {
            var rows = [];
            for (
                var i = 10 * (this.currentPage - 1);
                i < this.rowsPerPage * this.currentPage;
                i++
            ) {
                if (this.data.length > i) {
                    rows.push(this.buildCellRow(this.data[i], i));
                }
            }

            return rows;
        };

        this.buildBody = function() {
            this.clearTableBody();

            var rows = this.getRows();
            rows.forEach(function(row) {
                this.reference.querySelector("tbody").appendChild(row);
            });

            if (this.withCheckBoxes) {
                this.addCheckboxColumn();
            }

            if (this.withActions) {
                this.addActions();
            }
        };

        this.resetSort = function() {
            var upArrows = document.querySelectorAll(".arrow-up__active");
            if (upArrows && upArrows.length > 0) {
                upArrows.forEach(function(upArrow) {
                    upArrow.classList.remove("arrow-up__active");
                });
            }

            var downArrows = document.querySelectorAll(".arrow-down__active");
            if (downArrows && downArrows.length > 0) {
                downArrows.forEach(function(downArrow) {
                    downArrow.classList.remove("arrow-down__active");
                });
            }
        };

        this.getSortArrowUp = function() {
            var arrowUp = buildElement("div", { class: "arrow-up" });
            arrowUp.addEventListener("click", function(e) {
                _.resetSort();

                var columnSelected = e.target.parentNode.parentNode
                    .querySelector(".th__text")
                    .innerText.toLowerCase();

                var index = _.columns.indexOf(columnSelected);
                var order = "asc";
                _.data.sort(function(a, b) {
                    var first = a[index],
                        second = b[index];

                    if (!isNaN(first)) first = +first;
                    if (!isNaN(second)) second = +second;

                    if (first === second) {
                        return 0;
                    } else {
                        if (order === "asc") {
                            return first < second ? -1 : 1;
                        } else if (order === "desc") {
                            return first < second ? 1 : -1;
                        }
                    }
                });

                e.target.classList.add("arrow-up__active");
                _.buildBody();
            });

            return arrowUp;
        };

        this.getSortArrowDown = function() {
            var arrowDown = buildElement("div", { class: "arrow-down" });

            var _ = this;
            arrowDown.addEventListener("click", function(e) {
                _.resetSort();

                var columnSelected = e.target.parentNode.parentNode
                    .querySelector(".th__text")
                    .innerText.toLowerCase();

                var index = _.columns.indexOf(columnSelected);
                var order = "desc";
                _.data.sort(function(a, b) {
                    var first = a[index],
                        second = b[index];

                    if (!isNaN(first)) first = +first;
                    if (!isNaN(second)) second = +second;

                    if (first === second) {
                        return 0;
                    } else {
                        if (order === "asc") {
                            return first < second ? -1 : 1;
                        } else if (order === "desc") {
                            return first < second ? 1 : -1;
                        }
                    }
                });

                e.target.classList.add("arrow-down__active");
                _.buildBody();
            });

            return arrowDown;
        };

        this.addSortable = function() {
            var headers = this.reference.querySelectorAll("th");

            headers.forEach(function(header) {
                if (header.innerHTML === "" || header.children.length !== 0) {
                    return;
                }

                var headerText = header.innerHTML;
                header.innerHTML = "";

                var thCnt = buildElement("div", { class: "th__cnt" });
                var thText = buildElement("span", {
                    class: "th__text",
                    text: headerText
                });

                var sortCnt = buildElement("div", { class: "sort__cnt" });
                sortCnt.appendChild(this.getSortArrowUp());
                sortCnt.appendChild(this.getSortArrowDown());

                thCnt.appendChild(thText);
                thCnt.appendChild(sortCnt);

                header.appendChild(thCnt);
            });
        };

        this.getMasterCheckbox = function() {
            var masterCell = buildElement("td");
            var masterCheckbox = buildElement("input", {
                type: "checkbox",
                class: "master__cb"
            });
            masterCheckbox.addEventListener("click", function(e) {
                var cbs = document.querySelectorAll(".row__cb");

                if (e.target.checked) {
                    cbs.forEach(function(cb) {
                        cb.checked = true;
                    });
                } else {
                    cbs.forEach(function(cb) {
                        cb.checked = false;
                    });
                }
            });

            masterCell.appendChild(masterCheckbox);
            return masterCell;
        };

        this.addCheckboxColumn = function() {
            var headerRow = table.querySelector("thead>tr");

            var masterCheckbox = this.getMasterCheckbox();
            if (!this.reference.querySelector(".master__cb")) {
                headerRow.insertBefore(masterCheckbox, headerRow.firstChild);
            }

            var bodyRows = table.querySelectorAll("tbody>tr");
            bodyRows.forEach(function(row) {
                var cell = buildElement("td");
                var checkbox = buildElement("input", {
                    type: "checkbox",
                    class: "row__cb"
                });

                var _ = this;
                checkbox.addEventListener("change", function(e) {
                    _.checked.push(1);
                });

                cell.appendChild(checkbox);
                row.insertBefore(cell, row.firstChild);
            });
        };

        this.addActions = function() {
            var headerRow = table.querySelector("thead>tr");

            if (!this.reference.querySelector(".header__actions")) {
                headerRow.appendChild(
                    buildElement("th", { class: "header__actions" })
                );
            }

            var bodyRows = table.querySelectorAll("tbody>tr");
            bodyRows.forEach(function(row) {
                var cell = buildElement("td");
                var menu = buildElement("div", { class: "threedots" });

                var dialog = buildElement("div", { class: "dialog__3dots" });

                var editLink = row.getAttribute("data-actions-edit");
                var edit = buildElement("a", { text: "Edit", href: editLink});

                var deleteLink = row.getAttribute("data-actions-delete");
                var del = buildElement("a", { text: "Delete" });
                del.addEventListener("click", function(e) {
                    console.log("modal");
                });

                dialog.appendChild(edit);
                dialog.appendChild(del);
                cell.appendChild(dialog);

                cell.appendChild(menu);
                row.appendChild(cell);
            });
        };

        this.addFilter = function() {
            var formElement = buildElement("div", {
                class: "form-element dtable__search"
            });
            var input = buildElement("input", {
                type: "text",
                class: "form-input",
                placeholder: "Search..."
            });

            this.backupData = this.data;
            var _ = this;
            input.addEventListener("keyup", function(e) {
                var filter = e.target.value.toUpperCase();
                
                var filteredData = [];
                _.backupData.forEach(function(dataRow) {
                    var wasAdded = false;

                    dataRow.forEach(function(dataCell) {
                        if(wasAdded) return;

                        if (dataCell.innerText.toUpperCase().indexOf(filter) > -1) {
                            filteredData.push(dataRow);
                            wasAdded = true;
                            return;
                        }
                    });
                });

                if (filter !== "") {
                    _.data = filteredData;

                    _.rowsPerPage = _.getNumberOfRowsPerPage();
                    _.totalPages = Math.ceil(_.data.length / _.rowsPerPage);
                    _.buildPagination();
                    _.buildBody();
                } else {
                    _.data = _.backupData;

                    _.rowsPerPage = _.getNumberOfRowsPerPage();
                    _.totalPages = Math.ceil(_.data.length / _.rowsPerPage);
                    _.buildPagination();
                    _.buildBody();
                }
            });

            formElement.appendChild(input);
            this.reference.before(formElement);
        };

        this.columns = this.getColumns();
        this.data = this.getData();
        this.rowsPerPage = this.getNumberOfRowsPerPage();
        this.totalPages = Math.ceil(this.data.length / this.rowsPerPage);

        if (this.reference.getAttribute("data-filter") !== null) {
            this.addFilter();
        }

        if (this.reference.getAttribute("data-sortable") !== null) {
            this.addSortable();
        }

        if (this.reference.getAttribute("data-checkboxes") !== null) {
            this.checked = [];
            this.withCheckBoxes = true;
        }

        if (this.reference.getAttribute("data-actions") !== null) {
            this.withActions = true;
        }

        this.buildPagination();
        this.buildBody();

        return this;
    }
}, false);
