window.addEventListener("load", function() {
    var files = document.querySelectorAll('.form-input__file');
    files.forEach(function(file) {
        var button = buildElement("button", { class: "form-input__file-btn", text: "Choose File"});
        file.appendChild(button);

        var input = file.querySelector(".form-input__file-input");
        input.addEventListener('change', function(e) {
            var fileName = e.target.value.replace("C:\\fakepath\\", "");
            e.target.parentNode.querySelector(".form-input__file-desc").innerText = fileName;
        });

        file.addEventListener('click', function(e) {
            if (e.target.classList.contains("form-input__file-btn"))
                e.preventDefault();

            e.target.parentNode.querySelector('.form-input__file-input').click();
        });
    });

    var images = document.querySelectorAll(".form-input__image");
    images.forEach(function(image) {
        var button = buildElement("button", { class: "form-input__image-btn", text: "Choose Image" });
        image.appendChild(button);

        var preview = buildElement("div", { class: "form-input__image-preview" });
        var previewSpan = buildElement("span", { text: "Preview" });
        var previewImg = buildElement("img", { src: "none" });
        preview.appendChild(previewSpan);
        preview.appendChild(previewImg);
        image.appendChild(preview);

        // INPUT
        var input = image.querySelector(".form-input__image-input");
        input.addEventListener("change", function(e) {
            var imageName = e.target.value.replace("C:\\fakepath\\", "");
            e.target.parentNode.querySelector(".form-input__image-desc").innerText = imageName;

            var reader = new FileReader();
            reader.onload = function(e2) {
                previewImg.setAttribute("src", e2.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            previewSpan.classList.add("preview__active");
        });

        // PREVIEW
        preview.addEventListener('mouseover', function(e) {
            if(e.target === this && preview.querySelector('.image__preview-popup') === null) {
                var x = document.documentElement.clientWidth - e.target.offsetLeft - 16;
                var y = document.documentElement.clientHeight - e.target.offsetTop;

                if (input.value !== "") {
                    var popup = buildElement("div", {
                        class: "image__preview-popup",
                        style: "bottom: " + y + "px; right: " + x + "px;"
                    });

                    var img = buildElement("img", {
                        src: previewImg.getAttribute("src")
                    });
                    popup.appendChild(img);
                    preview.appendChild(popup);
                }
            }
        });

        preview.addEventListener("mouseleave", function(e) {
            var popup = preview.querySelector(".image__preview-popup");
            if (popup !== null) {
                popup.parentNode.removeChild(popup);
            }
        });
        
        // TRIGGER
        image.addEventListener("click", function(e) {
            if (e.target.classList.contains("form-input__image-btn"))
                e.preventDefault();

            e.target.parentNode
                .querySelector(".form-input__image-input")
                .click();
        });
    });
}, false);