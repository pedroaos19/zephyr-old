window.addEventListener("load", function() {
    var previews = document.querySelectorAll(".record__preview");

    previews.forEach(function(preview) {
        preview.addEventListener("mouseover", function(e) {
            if (preview.querySelector(".image__preview-popup") === null) {
                var rect = e.target.getBoundingClientRect();

                var x = document.documentElement.clientWidth - rect.left - 32;
                var y = document.documentElement.clientHeight - rect.top + 8;

                var popup = buildElement("div", {
                    class: "image__preview-popup",
                    style: "bottom: " + y + "px; right: " + x + "px;"
                });

                var img = buildElement("img", {
                    src: e.target.parentNode.querySelector("img").getAttribute("src")
                });

                popup.appendChild(img);
                preview.appendChild(popup);
            }
        });

        preview.addEventListener("mouseleave", function(e) {
            var popup = preview.querySelector(".image__preview-popup");
            if (popup !== null) {
                popup.parentNode.removeChild(popup);
            }
        });
    });
    
}, false);

document.addEventListener("click", function(e) {
    var activeThreeDots = document.querySelectorAll(".dialog__3dots-active");
    activeThreeDots.forEach(function(dialog) {
        dialog.classList.remove("dialog__3dots-active");
    });

    if(e.target.classList.contains("threedots")) {
        var dialog = e.target.parentNode.querySelector(".dialog__3dots");

        var rect = e.target.getBoundingClientRect();
        var x = document.documentElement.clientWidth - rect.left;
        var y = document.documentElement.clientHeight - rect.top;

        dialog.setAttribute(
            "style",
            "bottom: " + y + "px; right: " + x + "px;"
        );
        dialog.classList.add("dialog__3dots-active");
    }
});

var wrapper = document.querySelector(".content__wrapper");
wrapper.addEventListener("scroll", function() {
    var activeThreeDots = document.querySelectorAll(".dialog__3dots-active");
    activeThreeDots.forEach(function(dialog) {
        dialog.classList.remove("dialog__3dots-active");
    });
});