window.buildElement = function(el, options) {
    var element = document.createElement(el);
    
    if(options) {
        for(var prop in options) {
            if (Object.prototype.hasOwnProperty.call(options, prop)) {
                element.setAttribute(prop, options[prop]);
            }
        }
    }

    if(options && options.class) {
        element.setAttribute("class", options.class);
    }

    if (options && options.id) {
        element.setAttribute("id", options.id);
    }

    if (options && options.href) {
        element.setAttribute("href", options.href);
    }

    if(options && options.text) {
        element.innerText = options.text.toString();
    }

    if (options && options.html) {
        element.innerHTML = options.html;
    }

    if (options && options.style) {
        element.setAttribute("style", options.style);
    }

    if (options && options.type) {
        element.setAttribute("type", options.type);
    }


    if (options && options.src) {
        element.setAttribute("src", options.src);
    }

    if (options && options.placeholder) {
        element.setAttribute("placeholder", options.placeholder);
    }

    return element;
}

window.addEventListener("load", function() {
    var addColumnButtons = document.querySelectorAll(".new-cols__add");
    addColumnButtons.forEach(function(btn) {
        var blankCol = btn.parentNode.parentNode.querySelector("li");

        btn.addEventListener("click", function(e) {
            var colsCount = btn.parentNode.parentNode.querySelector(".new-cols__list").children.length;
            
            var newCol = blankCol.cloneNode(true);
            newCol.removeChild(newCol.querySelector(".align__r"));
            
            if (colsCount >= 1) {
                var after = newCol.querySelector('#after')
                after.setAttribute("disabled", "disabled");
                after.innerHTML = "";
            }

            var delBtnCnt = buildElement("div", {class: "align__r"});
            var delBtn = buildElement("div", {class: "btn", type: "button", "text": "Delete"});

            delBtn.addEventListener("click", function(e) {
                var li = e.target.parentNode.parentNode;
                li.parentNode.removeChild(li);
            })

            delBtnCnt.appendChild(delBtn);
            newCol.appendChild(delBtnCnt);

            btn.parentNode.parentNode.querySelector(".new-cols__list").appendChild(newCol);
        });
    });
}, false);

window.addEventListener("load", function() {
    var relation = document.querySelector("select#relation");
    var second = document.querySelector("select#second");

    if(relation) {
        relation.addEventListener("change", function (e) {
            var relType = parseInt(e.target.value);

            switch (relType) {
                case 1:
                    second.innerHTML = "";
                    var belongsTo = tables.belongsTo;
                    belongsTo.forEach(function (btTable) {
                        var option = buildElement("option", { value: btTable, text: btTable });
                        second.appendChild(option);
                    });
                    break;
                case 2:
                    second.innerHTML = "";
                    var hasMany = tables.hasMany;
                    hasMany.forEach(function (hmTable) {
                        var option = buildElement("option", { value: hmTable, text: hmTable });
                        second.appendChild(option);
                    });
                    break;
                case 3:
                    second.innerHTML = "";
                    var belongsToMany = tables.belongsToMany;
                    belongsToMany.forEach(function (btmTable) {
                        var option = buildElement("option", { value: btmTable, text: btmTable });
                        second.appendChild(option);
                    });
                    break;
            }

            second.dispatchEvent(new Event('change'));
        });

        relation.dispatchEvent(new Event('change'));
    }

    if(second) {
        var displayColumnInput = document.querySelector("select#display_column");

        second.addEventListener("change", function(e) {
            displayColumnInput.innerHTML = "";
            
            var relation = document.querySelector("select#relation").value;
            var table = e.target.value;
            if(parseInt(relation) === 2) {
                table = document.querySelector("select#first").value;
            } else if(parseInt(relation) === 3) {
                
            }

            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/atlas/table/relation/' + table + "/columns");
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var columns = JSON.parse(xhr.response);
                    columns.forEach(function(column) {
                        var option = buildElement("option", {value: column, text: column});
                        displayColumnInput.appendChild(option);
                    })
                } else {
                    console.log(xhr);
                }
            };
            xhr.send();
        });

        second.dispatchEvent(new Event('change'));
    }
});
