@if (errors())
<div class="alert alert-danger" style="padding-left: 32px;">
    <ul>
        @foreach (errors() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif