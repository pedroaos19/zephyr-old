<header>
    <nav class="navbar">
        <a href="/atlas" class="navbar__brand">
            <h3 class="navbar__brand-text">Atlas</h3>
        </a>
        <ul class="navbar__list">
            <form class="hidden" method="POST" action="/atlas/signout" id="logout-form">{{csrf_token()}}</form>
            <li class="navbar__list-item">
                <button class="logout-btn" onclick="document.querySelector('#logout-form').submit();">
                    <span>Logout</span>
                </button>
            </li>
        </ul>
    </nav>
</header>