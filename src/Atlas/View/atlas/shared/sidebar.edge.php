<div class="sidebar">
    <ul class="sidebar__list">
        <li class="sidebar__list-item @if(route_active('/atlas/table')) active @endif">
            <a href="/atlas/table">
                <span>Tables</span>
            </a>
        </li>
        <li class="sidebar__list-item @if(route_active('/atlas/route')) active @endif">
            <a href="/atlas/route">
                <span>Routes</span>
            </a>
        </li>
        <li class="sidebar__list-item @if(route_active('/atlas/page')) active @endif">
            <a href="/atlas/page">
                <span>Pages</span>
            </a>
        </li>
        <li class="sidebar__list-item @if(route_active('/atlas/member')) active @endif">
            <a href="/atlas/member">
                <span>Members</span>
            </a>
        </li>
        <li class="sidebar__list-item @if(route_active('/atlas/settings')) active @endif">
            <a href="/atlas/settings">
                <span>Settings</span>
            </a>
        </li>
    </ul>
</div>