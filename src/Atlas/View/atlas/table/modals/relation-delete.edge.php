<div class="modal fade" tabindex="-1" role="dialog" id="modal-relation-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-atlas">
            <div class="modal-header">
                <h5 class="modal-title">Delete Relation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this relation?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger rel-confirm-delete-modal" type="button" class="btn btn-danger">Delete</button>
                <button class="btn btn-secondary rel-cancel-delete-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form class="rel-delete-form" method="POST" action="/atlas/table/relation/%first%/%second%/%column%/destroy">{{csrf_token()}}</form>
            </div>
        </div>
    </div>
</div>