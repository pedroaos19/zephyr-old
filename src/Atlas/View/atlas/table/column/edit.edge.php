@extends('atlas.layouts.master')

@section('content')
<div class="content__title-cnt">
    <span class="content__title">Tables</span>
</div>
<div class="content">
    <h4 class="content__subtitle">Edit column: <strong>{{$column->name}}</strong></h4>
    @include('atlas.shared.errors')
    <form method="POST" action="/atlas/table/{{$table->name}}/column/{{$column->name}}/update" id="create-column-form" enctype="multipart/form-data">
        {{csrf_token()}}
        <div class="form-element">
            <label for="name">Column Name</label>
            <input class="form-input" type="text" id="name" name="name" placeholder="Column Name" value="{{$column->name}}" required />
        </div>
        <div class="form-element">
            <label for="type">Column Type</label>
            <select class="form-input" id="type" name="type" required>
                <option value="bit" @if($column->type === "BIT") selected @endif>Bit</option>
                <option value="integer" @if($column->type === "INT") selected @endif>Int</option>
                <option value="tinyint" @if($column->type === "TINYINT") selected @endif>Tinyint</option>
                <option value="smallint" @if($column->type === "SMALLINT") selected @endif>Smallint</option>
                <option value="mediumint" @if($column->type === "MEDIUMINT") selected @endif>Mediumint</option>
                <option value="bigint" @if($column->type === "BIGINT") selected @endif>Bigint</option>
                <option value="float" @if($column->type === "FLOAT(10,2)") selected @endif>Float</option>
                <option value="double" @if($column->type === "DOUBLE(16,4)") selected @endif>Double</option>
                <option value="date" @if($column->type === "DATE") selected @endif>Date</option>
                <option value="datetime" @if($column->type === "DATETIME") selected @endif>DateTime</option>
                <option value="timestamp" @if($column->type === "TIMESTAMP") selected @endif>Timestamp</option>
                <option value="time" @if($column->type === "TIME") selected @endif>Time</option>
                <option value="char" @if($column->type === "CHAR(1)") selected @endif>Char</option>
                <option value="varchar" @if($column->type === "VARCHAR(250)") selected @endif>Varchar</option>
                <option value="file" @if($column->type === "VARCHAR(510)") selected @endif>File</option>
                <option value="image" @if($column->type === "VARCHAR(512)") selected @endif>Image</option>
                <option value="text" @if($column->type === "TEXT") selected @endif>Text</option>
                <option value="tinytext" @if($column->type === "TINYTEXT") selected @endif>Tinytext</option>
                <option value="mediumtext" @if($column->type === "MEDIUMTEXT") selected @endif>Mediumtext</option>
                <option value="longtext" @if($column->type === "LONGTEXT") selected @endif>Longtext</option>
            </select>
        </div>
        <input type="hidden" id="default_placeholder" value="{{$column->default}}" data-type="test">
        <div class="form-element">
            <label for="default">Default</label>
            <input class="form-input" type="text" id="default" name="default" placeholder="Default" />
        </div>
        <div class="form-element">
            <label for="nullable">Is Nullable?</label>
            <select class="form-input" id="nullable" name="nullable">
                <option value="1_on" @if($column->nullable) selected @endif>Yes</option>
                <option value="0_off" @if(!$column->nullable) selected @endif>No</option>
            </select>
        </div>
        <div class="form-element">
            <label for="unique">Is Unique?</label>
            <select class="form-input" id="unique" name="unique">
                <option value="1_on" @if($column->unique) selected @endif>Yes</option>
                <option value="0_off" @if(!$column->unique) selected @endif>No</option>
            </select>
        </div>
        <div class="form-element">
            <label for="after">After</label>
            <select class="form-input" id="after" name="after">
                @foreach($table->schema->columns() as $col)
                @if($col->name !== $column->name)
                <option value="{{$col->name}}" @if($column->after === $col->name) selected @endif>{{$col->name}}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="form__submit">
            <button type="submit" class="form__submit-button">Submit</button>
        </div>
    </form>
</div>
@endsection