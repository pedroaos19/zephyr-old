@extends('atlas.layouts.master')

@section('content')
<div class="content__title-cnt">
    <span class="content__title">Add columns to "{{$table->name}}"</span>
</div>
<div class="content">
    <h4 class="content__subtitle">New columns</h4>
    @include('atlas.shared.errors')
    <form method="POST" action="/atlas/table/{{$table->name}}/column/store" id="create-column-form" enctype="multipart/form-data">
        {{csrf_token()}}
        <div class="align__l">
            <button type="button" class="btn new-cols__add">Add</button>
        </div>
        <ul class="new-cols__list">
            <li>
                <div class="form-element">
                    <label for="name">Name</label>
                    <input class="form-input" type="text" name="name[]" id="name" placeholder="Name">
                </div>
                <div class="form-element">
                    <label for="type">Type</label>
                    <select class="form-input" name="type[]" id="type">
                        <option value="bit">Bit</option>
                        <option value="int">Int</option>
                        <option value="tinyint">Tinyint</option>
                        <option value="smallint">Smallint</option>
                        <option value="mediumint">Mediumint</option>
                        <option value="bigint">Bigint</option>
                        <option value="float">Float</option>
                        <option value="double">Double</option>
                        <option value="date">Date</option>
                        <option value="datetime">DateTime</option>
                        <option value="timestamp">Timestamp</option>
                        <option value="time">Time</option>
                        <option value="char">Char</option>
                        <option value="varchar" selected>Varchar</option>
                        <option value="file">File</option>
                        <option value="image">Image</option>
                        <option value="text">Text</option>
                        <option value="tinytext">Tinytext</option>
                        <option value="mediumtext">Mediumtext</option>
                        <option value="longtext">Longtext</option>
                    </select>
                </div>
                <div class="form-element">
                    <label for="default">Default</label>
                    <input class="form-input" type="text" name="default[]" id="default" placeholder="Default">
                </div>
                <div class="form-element">
                    <label for="nullable">Nullable</label>
                    <select class="form-input" name="nullable[]" id="nullable">
                        <option value="1_on">Yes</option>
                        <option value="0_off" selected>No</option>
                    </select>
                </div>
                <div class="form-element">
                    <label for="unique">Unique</label>
                    <select class="form-input" name="unique[]" id="unique">
                        <option value="1_on">Yes</option>
                        <option value="0_off" selected>No</option>
                    </select>
                </div>
                <div class="form-element">
                    <label for="after">After</label>
                    <select class="form-input" name="after[]" id="after">
                        @foreach($table->schema->columns() as $column)
                        <option value="{{$column->name}}" selected>{{$column->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="align__r">
                    <button type="button" class="btn" disabled>Delete</button>
                </div>
            </li>
        </ul>
        <div class="form__submit">
            <button type="submit" class="form__submit-button">Submit</button>
        </div>
    </form>
</div>
@endsection