@extends('atlas.layouts.master')

@section('content')
<div class="content__title-cnt">
    <span class="content__title">Tables</span>
</div>
<div class="content">
    <h4 class="content__subtitle">Insert a new record</h4>
    @include('atlas.shared.errors')
    <form method="POST" action="/atlas/table/{{$table->name}}/record/store" id="create-record-form" enctype="multipart/form-data">
        {{csrf_token()}}
        @foreach($table->schema->columns(false) as $column)
        {!! \Zephyr\Atlas\View\Generator\InputGenerator::generate($column) !!}
        @endforeach
        <div class="form-element">
            <label for="aa">DateTime</label>
            <div class="">
                <input type="text" name="" id="">
                <button>Date</button>
            </div>
        </div>
        <div class="form__submit">
            <button type="submit" class="form__submit-button">Submit</button>
        </div>
    </form>
</div>
@endsection