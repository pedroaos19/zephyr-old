@extends('atlas.layouts.master')

@section('content')
<div class="content__title-cnt">
    <span class="content__title">Tables</span>
</div>
<div class="content-cnt" data-simplebar>
    <div class="content-card">
        <h4 class="content-card__title">Edit record</h4>
        @include('atlas.shared.errors')
        <form method="POST" action="/atlas/table/{{$table->name}}/record/{{$record->id}}/update" id="create-record-form" enctype="multipart/form-data">
            {{csrf_token()}}
            @foreach($table->schema->columns(false) as $column)
            <?php $colName = $column->name ?>
            {!! \Zephyr\Atlas\View\Generator\InputGenerator::generate($column, $record->$colName) !!}
            @endforeach
            <div style="display: flex; justify-content: flex-end;">
                <button type="submit" class="btn btn-atlas">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection