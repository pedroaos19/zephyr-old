@extends('atlas.layouts.master')

@section('content')
<div class="content__title-cnt">
    <span class="content__title">Tables</span>
</div>
<div class="content">
    <h4 class="content__subtitle">Create a new table</h4>
    @include('atlas.shared.errors')
    <form method="POST" action="/atlas/table/store">
        {{csrf_token()}}
        <div class="form-element">
            <label for="name">Table Name</label>
            <input class="form-input" type="text" id="name" name="name" placeholder="Table Name" required />
        </div>
        <div class="form__submit">
            <button type="submit" class="form__submit-button">Submit</button>
        </div>
    </form>
</div>
@endsection