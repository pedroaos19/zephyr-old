<div class="content__nav spread">
    <div class="content__nav-left">
        <a href="/atlas/table/{{$table->name}}">
            Records
        </a>
        <a href="/atlas/table/{{$table->name}}/column">
            Columns
        </a>
    </div>
    <div class="content__nav-right">
        <a href="/atlas/table/{{$table->name}}/record/create" class="@if(count($table->schema->columns()) < 2) disabled @endif">New Record</a>
        <a href="/atlas/table/{{$table->name}}/destroy">Delete Table</a>
    </div>
</div>