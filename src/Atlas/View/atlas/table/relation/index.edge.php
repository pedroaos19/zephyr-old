@extends('atlas.layouts.master')

@section('content')
<!-- @include('atlas/table/modals/relation-delete') -->
<div class="content__title">
    <span>Table Relations</span>
</div>
<div class="content">
    <div class="content-card">
        <!-- @if(flash('success'))
        <div class="alert alert-success" style="padding-left: 32px;">
            <ul>
                <li>{{ flash('success') }}</li>
            </ul>
        </div>
        @endif -->
    </div>
    @foreach($tables as $table)
    <div class="rel__cnt">
        <div class="rel__table">
            <p>{{$table->name}}</p>
            <div class="rel__table-actions">
                <a href="/atlas/table/relation/{{$table->name}}/create" class="btn">New Relation</a>
            </div>
        </div>
        <div class="rel__list">
            @if($table->belongsToTables())
            <div class="rel__list-item">
                <div class="rel__type">
                    <span class="value__btn">Belongs To</span>
                </div>
                <ul class="rel__table-list">
                    @foreach($table->belongsToTables() as $otherTable)
                    <li>
                        <span class="rel__list-table-name" style="margin-right: 4px;"><strong>{{$otherTable["REFERENCED_TABLE_NAME"]}}</strong></span>
                        <small class="mr-3">(<i>via column</i> "<strong>{{$otherTable["COLUMN_NAME"]}}</strong>")</small>
                        <button class="btn btn-sm btn-danger rel-modal-delete-trigger" data-modal-state="closed" data-toggle="modal" data-target="#modal-relation-delete" data-table-first="{{$table->name}}" data-table-second="{{$otherTable['REFERENCED_TABLE_NAME']}}" data-column="{{$otherTable['COLUMN_NAME']}}">
                            <i class="fa fa-times"></i>
                        </button>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if($table->hasManyTables())
            <div class="rel__list-item">
                <div class="rel__type">
                    <span class="value__btn">Has Many</span>
                </div>
                <ul class="rel__list">
                    @foreach($table->hasManyTables() as $otherTable)
                    <li>
                        <span class="rel__list-table-name" style="margin-right: 4px;"><strong>{{$otherTable["table"]}}</strong></span>
                        <small class="mr-3">(<i>from <strong>{{$otherTable["table"]}}</strong> column</i> "<strong>{{$otherTable["fk"]}}</strong>")</small>
                        <button class="btn btn-sm btn-danger rel-modal-delete-trigger" data-modal-state="closed" data-toggle="modal" data-target="#modal-relation-delete" data-table-first="{{$otherTable['table']}}" data-table-second="{{$table->name}}" data-column="{{$otherTable['fk']}}">
                            <i class="fa fa-times"></i>
                        </button>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
                        @if($table->belongsToManyTables())
                        <div class="relations__rel-btm">
                            <div class="rel-btm__indicator">
                                <span class="btn-sm btn-atlas">Belongs To Many</span>
                            </div>
                            <ul class="rel__list">
                                @foreach($table->belongsToManyTables() as $otherTable)
                                <li>
                                    <span class="rel__list-table-name" style="margin-right: 4px;"><strong>{{$otherTable["table"]}}</strong></span>
                                    <small class="mr-3">(<i>pivoted with "<strong>{{$otherTable["pivot"]}}</strong>")</i></small>
                                    <button class="btn btn-sm btn-danger rel-modal-delete-trigger" data-modal-state="closed" data-toggle="modal" data-target="#modal-relation-delete" data-table-first="{{$otherTable['table']}}" data-table-second="{{$table->name}}" data-column="NULL">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
    </div>
    @endforeach
</div>
@endsection