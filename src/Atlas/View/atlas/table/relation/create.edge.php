@extends('atlas.layouts.master')

@section('content')
<div class="content__title">
    <span>Tables</span>
</div>
<div class="content">
    <h4 class="content__subtitle">Add a relationship to {{$table->name}}</h4>
    @include('atlas.shared.errors')
    <form method="POST" action="/atlas/table/relation/{{$table->name}}/store" id="rel-create-form">
        {{csrf_token()}}
        <script>
            tables = {
                "belongsTo": {!!$belongsTo!!},
                "hasMany": {!!$hasMany!!},
                "belongsToMany": {!!$belongsToMany!!}
            }
        </script>
        <div class="form-element">
            <label for="furst">First Table</label>
                <select id="first" class="form-input" disabled required>
                    <option>{{$table->name}}</option>
                </select>
        </div>
        <div class="form-element">
            <label for="relation">Relation Type</label>
            <select id="relation" name="relation" class="form-input" required>
                <option value="1">Belongs To</option>
                <option value="2">Has Many</option>
                <option value="3">Belongs To Many</option>
            </select>
        </div>
        <div class="form-element">
            <label for="second">Second Table</label>
            <select id="second" name="second" class="form-input" required></select>
        </div>
        <div id="normal">
            <div class="form-element">
                <label for="column">Column Name (Optional)</label>
                <input class="form-input" type="text" id="column" name="column" placeholder="Optional" />
            </div>
            <div class="form-element ">
                <label for="display_as">Display As (Optional)</label>
                <input class="form-input" type="text" id="display_as" name="display_as" placeholder="Optional" />
            </div>
            <div class="form-element">
                <label for="display_column">Display Column</label>
                <select id="display_column" name="display_column" class="form-input"></select>
            </div>
        </div>
        <div class="form__submit">
            <button type="submit" class="form__submit-button">Submit</button>
        </div>
    </form>
</div>
@endsection