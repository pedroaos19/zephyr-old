@extends('atlas.layouts.master')

@section('content')
<!-- @include('atlas/table/modals/record-delete') -->
<div class="content__title">
    <span>{{$table->model}}</span>
</div>
<div class="content">
    @include('atlas/table/shared/nav')
    @if(!empty($records))
    <table class="dtable" data-sortable data-actions data-checkboxes data-filter>
        <thead>
            <tr>
                @foreach($table->schema->names(true) as $column)
                @if($table->schema->column($column)->foreignKey)
                <th>{{atlas_foreign_key($table, $relations, $column)}}</th>
                @else
                <th>{{$column}}</th>
                @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($records as $record)
            <tr>
                @foreach($table->schema->names(true) as $column)
                <td>
                    @if(!is_null($record->$column) && $table->schema->column($column)->foreignKey)
                    <a 
                        class="btn" 
                        href="{{'/atlas/table/' . $table->schema->column($column)->foreignKey['table'] . '/record/' . $record->$column . '/show'}}"
                    >
                        {{atlas_foreign_value($table, $relations, $column, $record)}}
                    </a>
                    @elseif(!is_null($record) && $table->schema->column($column)->type == "VARCHAR(512)")
                    <div class="record__preview">
                        <img class="record__preview-img" src="{{$record->$column}}">
                        <span class="record__preview-btn">{{$record->$column}}</span>
                    </div>
                    @elseif(!is_null($record->$column))
                    {{$record->$column}}
                    @else
                    <span class="value__btn-disabled">Null</span>
                    @endif
                </td>
                @endforeach
                <!-- <td>
                    <a href="/atlas/table/{{$table->name}}/record/{{$record->id}}/show" class="btn btn-sm btn-atlas"><i class="fa fa-fw fa-eye"></i></a>
                    <a href="/atlas/table/{{$table->name}}/record/{{$record->id}}/edit" class="btn btn-sm btn-atlas"><i class="fa fa-fw fa-edit"></i></a>
                    <button data-target-id="{{$record->id}}" data-modal-state="closed" data-toggle="modal" data-target="#modal-record-delete" class="btn btn-sm btn-atlas modal-delete-trigger"><i class="fa fa-fw fa-times"></i></button>
                </td> -->
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <h2 class="empty__text">
        @if(count($table->schema->columns()) < 2) Add more columns to insert records. @else No records found. @endif </h2> @endif </div> @endsection