@extends('atlas.layouts.master')

@section('content')
@include('atlas/table/modals/column-delete')
<div class="content__title">
    <span>{{$table->model}}</span>
</div>
<div class="content">
    @include('atlas/table/shared/nav')

    @if(flash('success'))
    <div class="alert alert-success" style="padding-left: 32px;">
        <ul>
            <li>{{ flash('success') }}</li>
        </ul>
    </div>
    @endif

    <div class="align__r">
        <a href="/atlas/table/{{$table->name}}/column/create" class="btn">New Column</a>
    </div>

    <table class="dtable" data-actions>
        <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Default</th>
                <th>Nullable</th>
                <th>Unique</th>
                <th>Key</th>
            </tr>
        </thead>
        <tbody>
            @foreach($table->schema->columns() as $column)
            <tr 
                data-actions-edit="/atlas/table/{{$table->name}}/column/{{$column->name}}/edit"
                data-actions-delete="/atlas/table/{{$table->name}}/column/{{$column->name}}/destroy"
            >
                <td>{{$column->name}}</td>
                <td>{{$column->humanizedType()}}</td>
                <td>
                    @if($column->default)
                    <span class="value__btn">{{$column->default}}</s>
                        @else
                        <span class="value__btn-disabled">No Default</span>
                        @endif
                </td>
                <td>
                    @if($column->nullable)
                    <span class="value__btn">Nullable</s>
                        @else
                        <span class="value__btn-disabled">Not Null</span>
                        @endif
                </td>
                <td>
                    @if($column->unique)
                    <span class="value__btn">Unique</s>
                        @else
                        <span class="value__btn-disabled">Not Unique</span>
                        @endif
                </td>
                <td>
                    @if($column->primaryKey)
                    <span class="value__btn">Primary</s>
                        @elseif($column->foreignKey)
                        <span class="value__btn">Foreign</s>
                            @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection