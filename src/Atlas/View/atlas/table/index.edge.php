@extends('atlas.layouts.master')

@section('content')
<div class="content__title">
    <span>Tables</span>
</div>
<div class="content">
    <div class="content__nav">
        <a href="/atlas/table/create">New Table</a>
        <a href="/atlas/table/relation">Relations</a>
    </div>
    @if(flash('success'))
    <div class="alert alert-success" style="padding-left: 32px;">
        <ul>
            <li>{{ flash('success') }}</li>
        </ul>
    </div>
    @endif
    @if(!empty($tables))
    <div class="table__list">
        @foreach($tables as $table)
        <div class="table__card">
            <p>{{ucfirst($table->name)}}</p>
            <a href="/atlas/table/{{$table->name}}">View More</a>
        </div>
        @endforeach
    </div>
    @else
    <h2 class="empty__text">No tables found.</h2>
    @endif
</div>
@endsection