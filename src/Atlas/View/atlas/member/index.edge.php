@extends('atlas.layouts.master')

@section('content')
@include('atlas/table/modals/record-delete')
<div class="content__title-cnt">
    <span class="content__title">Member</span>
</div>
<div class="content-cnt" data-simplebar>
    <div class="content-card">
        <div class="tab-nav">
            <div class="tab-nav__left">
            </div>
            <div class="tab-nav__right">
                <a href="/atlas/member/create" class="btn btn-sm btn-atlas @if(count($table->schema->columns()) < 2) disabled @endif">New Member</a>
            </div>
        </div>
        @if(!empty($members))
        <table class="table table-striped table-bordered nowrap dt-table">
            <thead>
                <tr>
                    @foreach($table->schema->names(true) as $column)
                    @if($column !== 'password')
                    <th>{{$column}}</th>
                    @endif
                    @endforeach
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                <tr>
                    @foreach($table->schema->names(true) as $column)
                    @if($column !== 'password')
                    <td>
                        @if(!is_null($member->$column) && $table->schema->column($column)->foreignKey)
                        <a class="btn btn-sm btn-atlas" href="{{'/atlas/table/' . $table->schema->column($column)->foreignKey['table'] . '/record/' . $record->$column . '/show'}}">{{$record->$column}}</a>
                        @elseif(!is_null($member->$column))
                        {{$member->$column}}
                        @else
                        <span class="btn-sm btn-atlas-disabled">Null</span>
                        @endif
                    </td>
                    @endif
                    @endforeach
                    <td>
                        <a href="/atlas/member/{{$member->id}}/show" class="btn btn-sm btn-atlas"><i class="fa fa-fw fa-eye"></i></a>
                        <a href="/atlas/member/{{$member->id}}/edit" class="btn btn-sm btn-atlas"><i class="fa fa-fw fa-edit"></i></a>
                        <button data-target-id="{{$member->id}}" data-modal-state="closed" data-toggle="modal" data-target="#modal-member-delete" class="btn btn-sm btn-atlas modal-delete-trigger"><i class="fa fa-fw fa-times"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="well-atlas">
            @if(count($table->schema->columns()) < 2) Add more columns to insert records. @else No records found. @endif </div> @endif </div> </div> @endsection