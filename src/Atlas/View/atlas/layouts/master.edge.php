<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Atlas Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:200,300,400,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="/src/Atlas/public/main.css" />
    <link rel="stylesheet" href="/src/Atlas/public/navbar.css" />
    <link rel="stylesheet" href="/src/Atlas/public/sidebar.css" />
    <link rel="stylesheet" href="/src/Atlas/public/table.css" />
    <link rel="stylesheet" href="/src/Atlas/public/route.css" />
</head>

<body>
    @include('atlas.shared.navbar')
    <main class="wrapper">
        @include('atlas.shared.sidebar')
        <div class="content__wrapper">
            @yield('content')
        </div>
    </main>
    <script src="/src/Atlas/public/main.js"></script>
    <script src="/src/Atlas/public/dtable.js"></script>
    <script src="/src/Atlas/public/fileinput.js"></script>
    <script src="/src/Atlas/public/defered.js"></script>
</body>

</html>