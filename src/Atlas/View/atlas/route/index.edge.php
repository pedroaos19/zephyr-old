@extends('atlas.layouts.master')

@section('content')
<div class="content__title">
    <span>Route List</span>
</div>
<div class="content">
    @foreach($routes as $route)
    <div class="route__item">
        <span class="route__method 
            @if($route->method === 'GET') route__method-get 
            @elseif($route->method === 'POST') route__method-post 
            @endif">{{$route->method}}</span>
        <span class="route__path">
            {{$route->path}}
            @if(isset($route->alias))
            <span class="path__alias">{{$route->alias}}</span>
            @endif
        </span>
        <span class="route__action">
            <span class="action__controller">{{$route->controller}}</span><span class="action__action">{{$route->action}}</span>
        </span>
    </div>
    @endforeach
</div>
@endsection