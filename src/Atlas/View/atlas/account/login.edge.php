<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <title>Atlas - Login</title>
    <link rel="stylesheet" href="/src/Atlas/public/login.css" />
</head>

<body>
    <div class="form-card">
        <div class="form-title">
            <h3>Atlas</h3>
        </div>
        <form id="form-signin" method="POST" action="/atlas/signin">
            {{ csrf_token() }}
            <div class="form-element">
                <input class="form-input" type="email" name="email" id="email" placeholder="Email" />
            </div>
            <div class="form-element">
                <input class="form-input" type="password" name="password" id="password" placeholder="Password" />
            </div>
            <button class="form-submit" type="submit">Login</button>
        </form>
    </div>
</body>

</html>