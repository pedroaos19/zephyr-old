<?php
namespace Zephyr\Atlas\View\Generator;

use Zephyr\Database\Column;
use Zephyr\Database\Table;

class InputGenerator
{
    const FORM_GROUP = "
        <div class=\"form-element\">
            %s
            %s
        </div>
    ";

    const LABEL = "
        <label for=\"%s\">%s</label>
    ";

    const COL_BIT = "
        <select class=\"form-input\" id=\"%s\" name=\"%s\">
            <option value=\"0_off\" %s>0</option>
            <option value=\"1_on\" %s>1</option>
        </select>
    ";

    const COL_FOREIGN = "
        <select class=\"form-input\" id=\"%s\" name=\"%s\">
            %s
        </select>
    ";

    const COL_FOREIGN_OPT = "
        <option value=\"%s\" %s>%s</option>
    ";

    const COL_INT = "
        <input type=\"number\" step=\"1\" class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\" %s/>
    ";

    const COL_FLOAT = "
        <input type=\"number\" step=\"0.0001\" class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\" %s/>
    ";

    const COL_DATE = "
        <input type=\"date\" class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\" %s/>
    ";

    const COL_CHAR = "
        <input type=\"text\" maxlength=\"1\" class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\" %s/>
    ";

    const COL_VARCHAR = "
        <input type=\"text\" class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\" %s/>
    ";

    const COL_TEXT = "
        <textarea class=\"form-input\" name=\"%s\" id=\"%s\" placeholder=\"%s\">%s</textarea>
    ";

    const COL_DATETIME = "
        <div class=\"input-group date atlas-datetimepicker\" id=\"atlas-dtp_%s\" data-target-input=\"nearest\">
            <input type=\"text\" class=\"form-input datetimepicker-input\" data-target=\"#atlas-dtp_%s\" name=\"%s\" id=\"%s\" %s/>
            <div class=\"input-group-append\" data-target=\"#atlas-dtp_%s\" data-toggle=\"datetimepicker\">
                <div class=\"input-group-text\">
                    <i class=\"fa fa-calendar\"></i>
                </div>
            </div>    
        </div>
    ";

    const COL_FILE = "
    <div class=\"form-element\">
        <label for=\"%s\">%s</label>
        <div class=\"form-input__file\">
            <input type=\"file\" class=\"form-input__file-input\" id=\"%s\" name=\"%s\">
            <span class=\"form-input__file-desc\" for=\"%s\">%s</span>
        </div>
    </div>
    ";

    const COL_IMAGE = "
    <div class=\"form-element\">
        <label for=\"%s\">%s</label>
        <div class=\"form-input__image\">
            <input type=\"file\" class=\"form-input__image-input\" id=\"%s\" name=\"%s\">
            <span class=\"form-input__image-desc\" for=\"%s\">%s</span>
        </div>
    </div>
    ";

    const VALUE = "
        value=\"%s\"
    ";

    public static function generate(Column $column, $value = null)
    {
        switch ($column->type)
        {
            case "VARCHAR(510)":
                return tprintf(
                    InputGenerator::COL_FILE,
                    $column->name,
                    humanize($column->name),
                    $column->name,
                    $column->name,
                    $column->name,
                    isset($value) ? $value : "Choose a file"
                );
            case "VARCHAR(512)":
                return tprintf(
                    InputGenerator::COL_IMAGE,
                    $column->name,
                    humanize($column->name),
                    $column->name,
                    $column->name,
                    $column->name,
                    isset($value) ? $value : "Choose an image...",
                    $column->name,
                    isset($value) ? asset($value) : asset('images/no-image.jpg')
                );
        }

        switch($column->humanizedType())
        {
            case "Bit":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(
                        InputGenerator::COL_BIT, 
                        $column->name, 
                        $column->name, 
                        isset($value) && $value === false ? "selected" : "",
                        isset($value) && $value === true ? "selected" : ""
                    )
                );
            case "Int":
            case "Tinyint":
            case "Smallint":
            case "Mediumint":
            case "Bigint":
                if($column->foreignKey)
                {
                    $tableName = $column->foreignKey["table"];
                    $model = Table::table($tableName)->namespace;

                    $all = $model::all();
                    $optString = "";
                    foreach($all as $record)
                    {
                        $optString .= tprintf(
                            InputGenerator::COL_FOREIGN_OPT,
                            $record->id,
                            isset($value) && $value == $record->id ? "selected" : "",
                            $record->toString()
                        );
                    }

                    return tprintf(
                        InputGenerator::FORM_GROUP,
                        tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                        tprintf(
                            InputGenerator::COL_FOREIGN,
                            $column->name,
                            $column->name,
                            $optString
                        )
                    );
                }

                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_INT, $column->name, $column->name, humanize($column->name), isset($value) ? tprintf(InputGenerator::VALUE, $value) : "")
                );
            case "Float":
            case "Double":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_FLOAT, $column->name, $column->name, humanize($column->name), isset($value) ? tprintf(InputGenerator::VALUE, $value) : "")
                );
            case "Date":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_DATE, $column->name, $column->name, humanize($column->name), isset($value) ? tprintf(InputGenerator::VALUE, $value) : "")
                );
            case "Datetime":
            case "Timestamp":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(
                        InputGenerator::COL_DATETIME, 
                        $column->name, 
                        $column->name, 
                        $column->name, 
                        $column->name,
                        isset($value) ? tprintf(InputGenerator::VALUE, $value) : "",
                        $column->name
                    )
                );
            case "Time":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(
                        InputGenerator::COL_DATETIME,
                        $column->name,
                        $column->name,
                        $column->name,
                        $column->name,
                        isset($value) ? tprintf(InputGenerator::VALUE, $value) : "",
                        $column->name
                    )
                );
            case "Char":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_CHAR, $column->name, $column->name, humanize($column->name), isset($value) ? tprintf(InputGenerator::VALUE, $value) : "")
                );
            case "Varchar":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_VARCHAR, $column->name, $column->name, humanize($column->name), isset($value) ? tprintf(InputGenerator::VALUE, $value) : "")
                );
            case "Text":
            case "Tinytext":
            case "Mediumtext":
            case "Longtext":
                return tprintf(
                    InputGenerator::FORM_GROUP,
                    tprintf(InputGenerator::LABEL, $column->name, humanize($column->name)),
                    tprintf(InputGenerator::COL_TEXT, $column->name, $column->name, humanize($column->name), isset($value) ? $value : "")
                );
        }
    }
}