<?php
namespace Zephyr\Atlas\Middleware;

use Zephyr\Http\Request\Request;
use Closure;

class Authorized
{
    public function run(Request $request, Closure $next)
    {
        if (!auth()) return redirect('/');
        if (is_null(member())) return redirect('/');
        if (!member()->has('atlas_admin')) return redirect('/');

        return $next($request);
    }
}
