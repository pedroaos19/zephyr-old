<?php

namespace Zephyr\Atlas\Controller\Route;

use Zephyr\Controller\Controller;
use Zephyr\Router\Map;

class RouteController extends Controller
{
    public function index()
    {
        $routes = Map::instance()->routes();
        return view('atlas/route/index', compact('routes'));
    }
}
