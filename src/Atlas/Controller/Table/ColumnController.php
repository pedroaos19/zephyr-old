<?php
namespace Zephyr\Atlas\Controller\Table;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;
use Zephyr\Database\Table;
use Zephyr\Database\Schema;
use Zephyr\Http\Session;
use Zephyr\Atlas\Utils\AtlasTouch;
use Zephyr\Database\SqlType;
use Zephyr\Http\Request\Uploader;

class ColumnController extends Controller
{
    const COL_NAME = 0;
    const COL_TYPE = 1;
    const COL_DEFAULT = 2;
    const COL_NULLABLE = 3;
    const COL_UNIQUE = 4;
    const COL_AFTER = 5;

    public function index()
    { }

    public function create($name)
    {
        $table = Table::table($name); 
        return view('atlas/table/column/create', compact('table'));
    }

    public function store(Request $request, $name)
    { 
        $data = $request->all();
        $numberOfColumns = count($data["name"]);

        $schema = new Schema;
        for($i = 0; $i < $numberOfColumns; $i++) 
        {
            $column = array_column($data, $i);

            $type = $column[static::COL_TYPE];
            $default = !is_null($request->files) && in_array("default", array_keys($request->files)) ? 
                Uploader::save("default", $name) : 
                $column[static::COL_DEFAULT];
            
            $after = isset($column[static::COL_AFTER]) ? $column[static::COL_AFTER] : $data["name"][$i - 1];

            $schema->$type($column[static::COL_NAME])
                ->default($default)
                ->nullable($column[static::COL_NULLABLE])
                ->unique($column[static::COL_UNIQUE])
                ->after($after);
        }
        
        Table::table($name)->addColumns($schema);

        flash("success", "Table altered successfully");
        return redirect('/atlas/table/' . $name . '/column');
    }

    public function edit($name, $colname)
    {
        $table = Table::table($name);
        $column = $table->schema->column($colname);

        return view('atlas/table/column/edit', compact('table', 'column'));
    }

    public function update(Request $request, $name, $colname)
    {
        $data = $request->all();

        $columnName = $data["name"];
        $type = $data["type"];
        $nullable = $data["nullable"];
        $unique = $data["unique"];
        $after = $data["after"];

        $default = null;
        if (in_array("default", array_keys($request->files))) 
        {
            $oldColDefault = Table::table($name)->schema->column($colname)->default;

            if(is_null($request->files["default"]) && $oldColDefault !== null)
            {   
                $default = $oldColDefault;
            }
            else
            {
                $default = Uploader::save("default", $name);
            }
        }
        else
        {
            $default = $data["default"];
        }

        $schema = new Schema;
        $schema->$type($columnName)
            ->default($default)
            ->nullable($nullable)
            ->unique($unique)
            ->after($after);
        
        if($columnName !== $colname) 
        {
            Table::table($name)->renameColumn($colname, $columnName);
        }

        Table::table($name)->alterColumns($schema);

        flash("success", "Column modified successfully");
        return redirect('/atlas/table/' . $name . '/column');
    }

    public function destroy(Request $request, $name, $colname)
    { 
        Table::table($name)->dropColumn($colname);

        flash("success", "Column removed successfully");
        return redirect('/atlas/table/' . $name . '/column');
    }
}
