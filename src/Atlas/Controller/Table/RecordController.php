<?php
namespace Zephyr\Atlas\Controller\Table;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;
use Zephyr\Database\Table;
use Zephyr\Database\Schema;
use Zephyr\Http\Session;
use Zephyr\Atlas\Utils\AtlasTouch;
use Zephyr\Database\SqlType;
use Zephyr\Http\Request\Uploader;

class RecordController extends Controller
{
    public function create($name)
    {
        $table = Table::table($name);
        return view('atlas/table/record/create', compact('table'));
    }

    public function store(Request $request, $name)
    {
        // dd($request);
        $table = Table::table($name);
        $model = $table->namespace;
        $columns = $table->schema->columns(false);

        $newRecord = new $model;
        foreach($columns as $column)
        {
            $colName = $column->name;
            if (in_array($colName, array_keys($request->files))) 
            {
                $newRecord->$colName = Uploader::save($colName, $name);
                continue;
            }

            $newRecord->$colName = $request->post($colName);
        }
        
        $newRecord->save();

        flash("success", "Record inserted successfully");
        return redirect('/atlas/table/' . $name );
    }

    public function edit($name, $id)
    {
        $table = Table::table($name);
        $model = $table->namespace;

        $record = $model::get($id);

        return view('atlas/table/record/edit', compact('table', 'record'));
    }

    public function update(Request $request, $name, $id)
    {
        $table = Table::table($name);
        $model = $table->namespace;
        $columns = $table->schema->columns(false);
        
        $record = $model::get($id);
        foreach ($columns as $column) 
        {
            $colName = $column->name;
            if (in_array($colName, array_keys($request->files))) 
            {
                if(is_null($request->files[$colName])) continue;

                $record->$colName = Uploader::save($colName, $name);
                continue;
            }

            $record->$colName = $request->post($colName);
        }

        $record->save();

        flash("success", "Record updated successfully");
        return redirect('/atlas/table/' . $name);
    }

    public function destroy(Request $request, $name, $id)
    {
        $model = Table::table($name)->namespace;
        $record = $model::get($id);

        $record->delete();

        flash("success", "Record deleted successfully");
        return redirect('/atlas/table/' . $name);
    }
}
