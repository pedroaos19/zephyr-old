<?php
namespace Zephyr\Atlas\Controller\Table;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;
use Zephyr\Database\Table;
use Zephyr\Database\Schema;
use Zephyr\Http\Session;
use Zephyr\Atlas\Utils\AtlasTouch;
use Zephyr\Atlas\Model\AtlasRelation;

class TableController extends Controller
{
    public function index()
    {
        $tables = Table::all(false, ['migrations', 'members', 'roles', 'atlas_relations']);
        return view('atlas/table/index', compact('tables'));
    }

    public function create()
    {
        return view('atlas/table/create');
    }

    public function store(Request $request)
    {
        if(!$request->has('name')) {
            errors("Name is required.");
            return redirect('/atlas/table/create');
        }

        if(Table::exists($request->post('name'))) 
        {
            errors("That table already exists.");
            return redirect('/atlas/table/create');
        }

        $schema = new Schema();
        $schema->primary('id');
        
        Table::create(tablerize($request->post('name')), $schema);
        AtlasTouch::createModel(tablerize($request->post('name')));
        flash("success", "Table created successfully");

        return redirect('/atlas/table');
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }

    public function record($name)
    {
        $table = Table::table($name);

        $class = $table->namespace;
        $records = $class::all();

        $relations = AtlasRelation::where(['table_name'], ['='], [$name]);
        return view('atlas/table/record', compact('table', 'records', 'relations'));
    }

    public function column($name)
    {
        $table = Table::table($name);

        return view('atlas/table/column', compact('table'));
    }
}
