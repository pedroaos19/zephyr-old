<?php
namespace Zephyr\Atlas\Controller\Table;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;
use Zephyr\Database\Table;
use Zephyr\Database\Schema;
use Zephyr\Http\Session;
use Zephyr\Atlas\Utils\AtlasTouch;
use Zephyr\Database\SqlType;
use Zephyr\Model\Pivot;
use Zephyr\Database\Connection;
use Zephyr\Database\PivotTable;
use Zephyr\Atlas\Model\AtlasRelation;

class RelationController extends Controller
{
    public function index()
    { 
        $tables = Table::all(false, ['migrations', 'members', 'roles', 'atlas_relations']);

        return view('atlas/table/relation/index', compact('tables'));
    }

    public function create($name)
    {
        $table = Table::table($name);

        $hasManyExcept = array_column($table->hasManyTables(), 'table');
        $belongsToExcept = array_column($table->belongsToTables(), 'REFERENCED_TABLE_NAME');
        $belongsToManyExcept = array_column($table->belongsToManyTables(), 'table');

        $hasMany = Table::all(
            false, 
            array_merge(['migrations', 'members', 'roles', 'atlas_relations', $name], $hasManyExcept, $belongsToExcept, $belongsToManyExcept)
        );
        $hasMany = array_column($hasMany, 'name');
        $hasMany = json_encode(array_values($hasMany));
        
        $belongsTo = Table::all(
            false, 
            array_merge(['migrations', 'members', 'roles', 'atlas_relations', $name], $hasManyExcept, $belongsToExcept, $belongsToManyExcept)
        );
        $belongsTo = array_column($belongsTo, 'name');
        $belongsTo = json_encode(array_values($belongsTo));

        $belongsToMany = Table::all(
            false, 
            array_merge(['migrations', 'members', 'roles', 'atlas_relations', $name], $hasManyExcept, $belongsToExcept)
        );
        $belongsToMany = array_column($belongsToMany, 'name');
        $belongsToMany = json_encode(array_values($belongsToMany));

        return view('atlas/table/relation/create', compact('table', 'hasMany', 'belongsTo', 'belongsToMany'));
    }

    public function store(Request $request, $name)
    {
        if(!in_array($request->post('relation'), [1, 2, 3]))
        {
            errors('Invalid relation choice.');
            return redirect('/atlas/table/relation/' . $name . '/create');
        }

        switch($request->post('relation'))
        {
            case 1:
                return $this->belongsTo(
                    $name, 
                    $request->post('second'), 
                    $request->post('column'), 
                    $request->post('display_as'),
                    $request->post('display_column')
                );
            case 2:
                return $this->hasMany(
                    $name, 
                    $request->post('second'),
                    $request->post('column'),
                    $request->post('display_as'),
                    $request->post('display_column')
                );
            case 3:
                return $this->belongsToMany($name, $request->post('second'));
        }
    }

    private function belongsTo($first, $second, $column = null, $displayAs = null, $displayColumn = null)
    {
        $col = isset($column) ? $column : singularize($second) . "_id";
        
        if(Table::table($first)->hasColumn($col))
        {
            errors("Column already exists in table " . $first);
            return redirect('/atlas/table/relation/' . $first . '/create');
        }
        
        $schema = new Schema;        
        $schema->foreign($col, $second)->nullable();

        Table::table($first)->addColumns($schema, true);

        AtlasTouch::addBelongsToModel($first, $second, $col);
        AtlasTouch::addHasManyModel($second, $first, $col);

        $atlasRelation = new AtlasRelation;
        $atlasRelation->table_name = $first;
        $atlasRelation->column_name = $col;
        $atlasRelation->relation_type = "belongs_to";
        $atlasRelation->definition = json_encode([
            "referenced" => $second,
            "display_as" => !is_null($displayAs) ? $displayAs : $col,
            "display_column" => $displayColumn
        ]);
        $atlasRelation->save();

        flash("success", "Relation added successfully");
        return redirect('/atlas/table/relation');
    }

    private function hasMany($first, $second, $column = null, $displayAs = null, $displayColumn = null)
    {
        $col = isset($column) ? $column : singularize($first) . "_id";

        $schema = new Schema;
        $schema->foreign($col, $first)->nullable();

        Table::table($second)->addColumns($schema, true);

        AtlasTouch::addHasManyModel($first, $second, $col);
        AtlasTouch::addBelongsToModel($second, $first, $col);

        $atlasRelation = new AtlasRelation;
        $atlasRelation->table_name = $second;
        $atlasRelation->column_name = $col;
        $atlasRelation->relation_type = "belongs_to";
        $atlasRelation->definition = json_encode([
            "referenced" => $first,
            "display_as" => !is_null($displayAs) ? $displayAs : $col,
            "display_column" => $displayColumn
        ]);
        $atlasRelation->save();

        flash("success", "Relation added successfully");
        return redirect('/atlas/table/relation');
    }

    private function belongsToMany($first, $second)
    {
        $schema = new Schema;
        $schema->primary('id');

        PivotTable::pivot($first, $second, $schema);

        AtlasTouch::addBelongsToManyModel($first, $second);
        AtlasTouch::addBelongsToManyModel($second, $first);

        flash("success", "Relation added successfully");
        return redirect('/atlas/table/relation');
    }

    public function columns($table)
    {
        $columns = array_column(Table::table($table)->schema->columns(), "name");
        return json($columns);
    }

    public function destroy(Request $request, $name, $other, $column)
    {
        if($column === "NULL")
        {
            $tables = [singularize($name), singularize($other)];
            sort($tables);
            $pivotTable = join("_", $tables);

            PivotTable::drop($pivotTable);

            AtlasTouch::removeBelongsToManyModel($name, $other);
            AtlasTouch::removeBelongsToManyModel($other, $name);

            flash("success", "Relation removed successfully");
            return redirect('/atlas/table/relation');
        }

        if (!Table::table($name)->hasColumn($column) && !Table::table($other)->hasColumn($column)) 
        {
            errors("Column doesn't exist");
            return redirect('/atlas/table/relation');
        }

        Table::table($name)->dropColumn($column);

        AtlasTouch::removeBelongsToModel($name, $other, $column);
        AtlasTouch::removeHasManyModel($other, $name, $column);

        flash("success", "Relation removed successfully");
        return redirect('/atlas/table/relation');
    }
}
