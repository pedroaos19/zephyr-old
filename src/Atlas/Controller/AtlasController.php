<?php
namespace Zephyr\Atlas\Controller;

use Zephyr\Controller\Controller;

class AtlasController extends Controller
{
    public function index()
    {
        return view('atlas/index');
    }
}
