<?php
namespace Zephyr\Atlas\Controller\Account;

use Zephyr\Controller\Controller;
use Zephyr\Http\Request\Request;

class AccountController extends Controller
{
    public function login()
    {
        return view('atlas/account/login');
    }

    public function signin(Request $request)
    {
        membership()->login($request->post('email'), $request->post('password'));
        return redirect('/atlas');
    }

    public function signout(Request $request)
    {
        membership()->logout();
        return redirect('/');
    }
}
