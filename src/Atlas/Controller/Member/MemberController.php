<?php
namespace Zephyr\Atlas\Controller\Member;

use App\Model\Member;
use Zephyr\Database\Table;

class MemberController
{ 
    public function index()
    {
        $members = Member::all();
        $table = Table::table('members');
        return view('atlas/member/index', compact('members', 'table'));
    }
}
