<?php
namespace Zephyr\Atlas\Utils;

use Symfony\Component\Process\PhpExecutableFinder;
use Zephyr\Database\Table;
use Zephyr\Filesystem\Filesystem;
use Zephyr\Utils\Str;

class AtlasTouch
{
    const BELONGS_TO = '
        public function %s()
        {
        '."\t".'return $this->belongsTo(\'%s\', \'%s\');
        }
    ';

    const HAS_MANY = '
        public function %s()
        {
        '."\t".'return $this->hasMany(\'%s\', \'%s\');
        }
    ';

    const BELONGS_TO_MANY = '
        public function %s()
        {
        '."\t".'return $this->belongsToMany(\'%s\');
        }
    ';

    public static function createModel($table)
    {
        $php = (new PhpExecutableFinder)->find(false);

        passthru($php . ' boreas make:model ' . classerize($table));
    }

    public static function destroyModel($table)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);
        
        $file = Filesystem::file($reflector->getFileName());
        $file->delete();
    }

    public static function addBelongsToModel($table, $other, $column)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if (Filesystem::exists($reflector->getFileName())) 
        {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();
            $lastOcurrence = strrpos($fileString, "}");

            $otherNamespace = Table::table($other)->namespace;

            if (Str::contains($file->contents(), $other) && Str::contains($file->contents(), $otherNamespace))
                return false;

            $newFileString = substr_replace(
                $fileString,
                "\t" . tprintf(
                    AtlasTouch::BELONGS_TO,
                    singularize($other),
                    $otherNamespace,
                    $column
                ) . PHP_EOL,
                $lastOcurrence,
                0
            );

            $file->putContents($newFileString);
            return true;
        }
    }

    public static function addHasManyModel($table, $other, $column)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if(Filesystem::exists($reflector->getFileName()))
        {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();
            $lastOcurrence = strrpos($fileString, "}");

            $otherNamespace = Table::table($other)->namespace;

            if (Str::contains($file->contents(), $other) && Str::contains($file->contents(), $otherNamespace))
                return false;

            $newFileString = substr_replace(
                $fileString, 
                "\t" . tprintf(
                    AtlasTouch::HAS_MANY,
                    $other,
                    $otherNamespace,
                    $column
                ) . PHP_EOL,
                $lastOcurrence, 
                0
            );

            $file->putContents($newFileString);
            return true;
        }
    }

    public static function addBelongsToManyModel($table, $other)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if (Filesystem::exists($reflector->getFileName())) 
        {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();
            $lastOcurrence = strrpos($fileString, "}");

            $otherNamespace = Table::table($other)->namespace;

            if (Str::contains($file->contents(), $other) && Str::contains($file->contents(), $otherNamespace))
                return false;

            $newFileString = substr_replace(
                $fileString,
                "\t" . tprintf(
                    AtlasTouch::BELONGS_TO_MANY,
                    $other,
                    $otherNamespace
                ) . PHP_EOL,
                $lastOcurrence,
                0
            );

            $file->putContents($newFileString);
            return true;
        }
    }

    public static function removeBelongsToModel($table, $other, $column)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if (Filesystem::exists($reflector->getFileName())) {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();

            $otherNamespace = Table::table($other)->namespace;
            $toRemove = tprintf(
                AtlasTouch::BELONGS_TO,
                $other,
                $otherNamespace,
                $column
            );

            $newFileString = str_replace($toRemove, "", $fileString);

            $file->putContents($newFileString);
            return true;
        }
    }

    public static function removeHasManyModel($table, $other, $column)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if (Filesystem::exists($reflector->getFileName())) {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();

            $otherNamespace = Table::table($other)->namespace;
            $toRemove = tprintf(
                AtlasTouch::HAS_MANY,
                $other,
                $otherNamespace,
                $column
            );

            $newFileString = str_replace($toRemove, "", $fileString);

            $file->putContents($newFileString);
            return true;
        }
    }

    public static function removeBelongsToManyModel($table, $other)
    {
        $namespace = Table::table($table)->namespace;
        $reflector = new \ReflectionClass($namespace);

        if (Filesystem::exists($reflector->getFileName())) {
            $file = Filesystem::file($reflector->getFileName());

            $fileString = $file->contents();

            $otherNamespace = Table::table($other)->namespace;
            $toRemove = tprintf(
                AtlasTouch::BELONGS_TO_MANY,
                $other,
                $otherNamespace
            );

            $newFileString = str_replace($toRemove, "", $fileString);

            $file->putContents($newFileString);
            return true;
        }
    }
}