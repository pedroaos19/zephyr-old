<?php

namespace Zephyr\Atlas;

class AtlasRouter
{
    public static function routes()
    {
        route_get("/atlas", "AtlasController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller'
        ]);
        
        static::account();
        static::tables();
        static::relations();
        static::columns();
        static::records();
        static::member_relations();
        static::member_columns();
        static::member_roles();
        static::member_records();
        static::routes_list();
    }

    protected static function account()
    {
        route_get("/atlas/login", "AccountController.login", [
            'middleware' => [],
            'namespace' => 'Zephyr\Atlas\Controller\Account'
        ]);

        route_post("/atlas/signin", "AccountController.signin", [
            'middleware' => [],
            'namespace' => 'Zephyr\Atlas\Controller\Account'
        ]);

        route_post("/atlas/signout", "AccountController.signout", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Account'
        ]);
    }

    protected static function tables()
    {
        route_get("/atlas/table", "TableController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/create", "TableController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/store", "TableController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/edit", "TableController.edit", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/update", "TableController.update", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/destroy", "TableController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);
    }

    protected static function columns()
    {
        route_get("/atlas/table/:name/column", "TableController.column", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/:name/column/create", "ColumnController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/column/store", "ColumnController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/:name/column/:colname/edit", "ColumnController.edit", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/column/:colname/update", "ColumnController.update", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/column/:colname/destroy", "ColumnController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);
    }

    protected static function records()
    {
        route_get("/atlas/table/:name", "TableController.record", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/:name/record/create", "RecordController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/record/store", "RecordController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/:name/record/:id/edit", "RecordController.edit", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/record/:id/update", "RecordController.update", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/:name/record/:id/destroy", "RecordController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);
    }

    protected static function relations()
    {
        route_get("/atlas/table/relation", "RelationController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/relation/:name/create", "RelationController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/relation/:name/store", "RelationController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_post("/atlas/table/relation/:name/:other/:column/destroy", "RelationController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);

        route_get("/atlas/table/relation/:name/columns", "RelationController.columns", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Table'
        ]);
    }

    protected static function member_records()
    {
        route_get("/atlas/member", "MemberController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_get("/atlas/member/create", "MemberController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/store", "MemberController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_get("/atlas/member/:id/edit", "MemberController.edit", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/:id/update", "MemberController.update", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/:id/destroy", "MemberController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);
    }

    protected static function member_roles()
    {
        route_post("/atlas/member/:id/role/store", "MemberController.roleStore", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/:id/destroy", "MemberController.roleDestroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);
    }

    protected static function member_columns()
    {
        route_get("/atlas/member/column", "ColumnController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_get("/atlas/member/column/create", "ColumnController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/column/store", "ColumnController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_get("/atlas/member/column/:colname/edit", "ColumnController.edit", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/column/:colname/update", "ColumnController.update", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/column/:colname/destroy", "ColumnController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);
    }

    protected static function member_relations()
    {
        route_get("/atlas/member/relation", "RelationController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_get("/atlas/member/relation/create", "RelationController.create", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/relation/store", "RelationController.store", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);

        route_post("/atlas/member/relation/:other/:column/destroy", "RelationController.destroy", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Member'
        ]);
    }

    protected static function routes_list()
    {
        route_get("/atlas/route", "RouteController.index", [
            'middleware' => [\Zephyr\Atlas\Middleware\Authorized::class],
            'namespace' => 'Zephyr\Atlas\Controller\Route'
        ]);
    }
}